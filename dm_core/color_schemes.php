<?php
/**
 * This file contains all the global variables for the color scheme, with all the variations
 */
$GLOBALS["dm-color-1"] = '';
$GLOBALS["dm-color-1-light"] = '';
$GLOBALS["dm-color-1-lighter"] = '';
$GLOBALS["dm-color-1-dark"] = '';
$GLOBALS["dm-color-1-darker"] = '';
$GLOBALS["dm-color-2"] = '';
$GLOBALS["dm-color-2-light"] = '';
$GLOBALS["dm-color-2-lighter"] = '';
$GLOBALS["dm-color-2-dark"] = '';
$GLOBALS["dm-color-2-darker"] = '';
$GLOBALS["dm-color-3"] = '';
$GLOBALS["dm-color-3-light"] = '';
$GLOBALS["dm-color-3-lighter"] = '';
$GLOBALS["dm-color-3-dark"] = '';
$GLOBALS["dm-color-3-darker"] = '';
$GLOBALS["dm-color-4"] = '';
$GLOBALS["dm-color-4-light"] = '';
$GLOBALS["dm-color-4-lighter"] = '';
$GLOBALS["dm-color-4-dark"] = '';
$GLOBALS["dm-color-4-darker"] = '';

$GLOBALS["dm-color-gray-light"] = '#999999';
$GLOBALS["dm-color-gray-dark"] = '#555555';
$GLOBALS["dm-color-white"] = '#ffffff';
$GLOBALS["dm-color-black"] = '#000000';