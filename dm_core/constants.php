<?php
/**
 * This is where theme-level constants are defined
 *
 * @package dm_core
 * @since 1.0.0
 */

/**
 * Constant that holds the theme's name and is used whenever we need to store options or theme-specific information
 */
define('THEME_NAME', 'thema');

/**
 * This is where all of the theme options are saved.
 * The only exception is the option used for storing whether the demo content has been imported.
 * That option looks like theme_name_imported_content (i.e. thema_imported_content)
 */
define('DM_OPTIONS', 'thema_options');


define('DM_IMPORTED_CONTENT', 'thema_imported_content');
