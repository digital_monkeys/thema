<?php

/**
 * Used to create necessary files/folders, otherwise some functions will crash
 */
function dm_create_required_files()
{
    // Make an array of all the folders that need to be created
    $arrRequiredFolders = array(
        get_template_directory() . '/exported',

        get_template_directory() . '/graphics/svg/default',

        get_template_directory() . '/graphics/svg/dm-color-1',
        get_template_directory() . '/graphics/svg/dm-color-1-light',
        get_template_directory() . '/graphics/svg/dm-color-1-lighter',
        get_template_directory() . '/graphics/svg/dm-color-1-dark',
        get_template_directory() . '/graphics/svg/dm-color-1-darker',
        get_template_directory() . '/graphics/svg/dm-color-2',
        get_template_directory() . '/graphics/svg/dm-color-2-light',
        get_template_directory() . '/graphics/svg/dm-color-2-lighter',
        get_template_directory() . '/graphics/svg/dm-color-2-dark',
        get_template_directory() . '/graphics/svg/dm-color-2-darker',
        get_template_directory() . '/graphics/svg/dm-color-3',
        get_template_directory() . '/graphics/svg/dm-color-3-light',
        get_template_directory() . '/graphics/svg/dm-color-3-lighter',
        get_template_directory() . '/graphics/svg/dm-color-3-dark',
        get_template_directory() . '/graphics/svg/dm-color-3-darker',
        get_template_directory() . '/graphics/svg/dm-color-4',
        get_template_directory() . '/graphics/svg/dm-color-4-light',
        get_template_directory() . '/graphics/svg/dm-color-4-lighter',
        get_template_directory() . '/graphics/svg/dm-color-4-dark',
        get_template_directory() . '/graphics/svg/dm-color-4-darker',
        get_template_directory() . '/graphics/svg/dm-color-gray-light',
        get_template_directory() . '/graphics/svg/dm-color-gray-dark',
        get_template_directory() . '/graphics/svg/dm-color-white',
        get_template_directory() . '/graphics/svg/dm-color-black',
    );

    // Then iterate through the array, creating each folder at its specified path
    foreach ($arrRequiredFolders as $crtDir)
    {
        if (!file_exists($crtDir)) {
            mkdir($crtDir);
        }
    }
}

dm_create_required_files();