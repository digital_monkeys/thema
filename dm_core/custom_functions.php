<?php
/**
 * This function is a wrapper over the built-in get_option() function, with the difference it returns a specific setting from an array of options.
 * If the option array doesn't exist, it creates it.
 * Also, it does a null-check for you.
 *
 * @param $optionType
 * @param string $optionName
 * @return string
 */
function dm_get_option($optionName)
{
    // First we try to read the option from the database
    $arrOptionContents = get_option(DM_OPTIONS);

    // If the option doesn't exist, we create it
    if(!isset($arrOptionContents) || $arrOptionContents === false)
    {
        add_option(DM_OPTIONS);
    }

    // Then we try to access the specific setting from the array that represents the option.
    // If the array of options itself is missing, or the specified setting is not found, it will return an empty string.
    // It will not return null, so as not to cause any problems with the returned result not being set.
    $strToReturn = '';
    if(isset($arrOptionContents) && isset($arrOptionContents[$optionName]))
    {
        $strToReturn = $arrOptionContents[$optionName];
        return $strToReturn;
    }
    else
    {
        return '';
    }
}

/**
 * This is just a wrapper over dm_get_option(), with the difference that it echoes the result instead of returning it.
 * @param $optionType
 * @param $optionName
 */
function dm_option($optionName)
{
    echo dm_get_option($optionName);
}


function dm_get_color_index($strToProcess)
{
    $strToReturn = '1';
    switch($strToProcess)
    {
        case 'Theme color 1':
            $strToReturn = '-1';
            break;
        case 'Theme color 1 light':
            $strToReturn = '-1-light';
            break;
        case 'Theme color 1 lighter':
            $strToReturn = '-1-lighter';
            break;
        case 'Theme color 1 dark':
            $strToReturn = '-1-dark';
            break;
        case 'Theme color 1 darker':
            $strToReturn = '-1-darker';
            break;


        case 'Theme color 2':
            $strToReturn = '-2';
            break;
        case 'Theme color 2 light':
            $strToReturn = '-2-light';
            break;
        case 'Theme color 2 lighter':
            $strToReturn = '-2-lighter';
            break;
        case 'Theme color 2 dark':
            $strToReturn = '-2-dark';
            break;
        case 'Theme color 2 darker':
            $strToReturn = '-2-darker';
            break;


        case 'Theme color 3':
            $strToReturn = '-3';
            break;
        case 'Theme color 3 light':
            $strToReturn = '-3-light';
            break;
        case 'Theme color 3 lighter':
            $strToReturn = '-3-lighter';
            break;
        case 'Theme color 3 dark':
            $strToReturn = '-3-dark';
            break;
        case 'Theme color 3 darker':
            $strToReturn = '-3-darker';
            break;


        case 'Theme color 4':
            $strToReturn = '-4';
            break;
        case 'Theme color 4 light':
            $strToReturn = '-4-light';
            break;
        case 'Theme color 4 lighter':
            $strToReturn = '-4-lighter';
            break;
        case 'Theme color 4 dark':
            $strToReturn = '-4-dark';
            break;
        case 'Theme color 4 darker':
            $strToReturn = '-4-darker';
            break;



        case 'Gray Light':
            $strToReturn = '-gray-light';
            break;
        case 'Gray Dark':
            $strToReturn = '-gray-dark';
            break;
        case 'White':
            $strToReturn = '-white';
            break;
        case 'Black':
            $strToReturn = '-black';
            break;
    }
    return $strToReturn;
}
function dm_get_font_index($strToProcess)
{
    $strToReturn = '-1';
    switch($strToProcess)
    {
        case 'Theme font 1':
            $strToReturn = '-1';
            break;
        case 'Theme font 2':
            $strToReturn = '-2';
            break;
        case 'Theme font 3':
            $strToReturn = '-3';
            break;
    }
    return $strToReturn;
}
function dm_get_theme_color($colorIndex)
{
    $strToReturn = '';
    switch((string)strtolower($colorIndex))
    {
        case '-1':
            $strToReturn = $GLOBALS["dm-color-1"];
            break;
        case '-1-light':
            $strToReturn = $GLOBALS["dm-color-1-light"];
            break;
        case '-1-lighter':
            $strToReturn = $GLOBALS["dm-color-1-lighter"];
            break;
        case '-1-dark':
            $strToReturn = $GLOBALS["dm-color-1-dark"];
            break;
        case '-1-darker':
            $strToReturn = $GLOBALS["dm-color-1-darker"];
            break;

        case '-2':
            $strToReturn = $GLOBALS["dm-color-2"];
            break;
        case '-2-light':
            $strToReturn = $GLOBALS["dm-color-2-light"];
            break;
        case '-2-lighter':
            $strToReturn = $GLOBALS["dm-color-2-lighter"];
            break;
        case '-2-dark':
            $strToReturn = $GLOBALS["dm-color-2-dark"];
            break;
        case '-2-darker':
            $strToReturn = $GLOBALS["dm-color-2-darker"];
            break;
        case '-3':
            $strToReturn = $GLOBALS["dm-color-3"];
            break;
        case '-3-light':
            $strToReturn = $GLOBALS["dm-color-3-light"];
            break;
        case '-3-lighter':
            $strToReturn = $GLOBALS["dm-color-3-lighter"];
            break;
        case '-3-dark':
            $strToReturn = $GLOBALS["dm-color-3-dark"];
            break;
        case '-3-darker':
            $strToReturn = $GLOBALS["dm-color-3-darker"];
            break;
        case '-4':
            $strToReturn = $GLOBALS["dm-color-4"];
            break;
        case '-4-light':
            $strToReturn = $GLOBALS["dm-color-4-light"];
            break;
        case '-4-lighter':
            $strToReturn = $GLOBALS["dm-color-4-lighter"];
            break;
        case '-4-dark':
            $strToReturn = $GLOBALS["dm-color-4-dark"];
            break;
        case '-4-darker':
            $strToReturn = $GLOBALS["dm-color-4-darker"];
            break;
        case '-gray-light':
            $strToReturn = $GLOBALS["dm-color-gray-light"];
            break;
        case '-gray-dark':
            $strToReturn = $GLOBALS["dm-color-gray-dark"];
            break;
        case 'white':
            $strToReturn = $GLOBALS["dm-color-white"];
            break;
        case 'black':
            $strToReturn = $GLOBALS["dm-color-black"];
            break;
        default:
            $strToReturn = $GLOBALS["dm-color-1"];
            break;
    }
    return $strToReturn;
}
function dm_get_color_options($associative = false)
{
    $arrOptions = array(
        'Theme color 1',
        'Theme color 1 light',
        'Theme color 1 lighter',
        'Theme color 1 dark',
        'Theme color 1 darker',

        'Theme color 2',
        'Theme color 2 light',
        'Theme color 2 lighter',
        'Theme color 2 dark',
        'Theme color 2 darker',

        'Theme color 3',
        'Theme color 3 light',
        'Theme color 3 lighter',
        'Theme color 3 dark',
        'Theme color 3 darker',

        'Theme color 4',
        'Theme color 4 light',
        'Theme color 4 lighter',
        'Theme color 4 dark',
        'Theme color 4 darker',

        'Gray Light',
        'Gray Dark',
        'White',
        'Black',
    );

    $arrToReturn = array();
    if($associative)
    {
      foreach($arrOptions as $option)
      {
          $arrToReturn[$option] = $option;
      }
    }
    else
    {
        $arrToReturn = $arrOptions;
    }

    return $arrToReturn;
}
function dm_get_font_options($associative = false)
{
    $arrOptions = array(
        'Theme Font 1',
        'Theme Font 2',
        'Theme Font 3',
    );

    $arrToReturn = array();
    if($associative) {
        foreach($arrOptions as $option)
        {
            $arrToReturn[$option] = $option;
        }
    }
    else
    {
        $arrToReturn = $arrOptions;
    }
}
function dm_get_transition_options()
{
    /*
    $arrOptions = array(
        'none',
        'bounceIn',
        'bounceInDown',
        'bounceInLeft',
        'bounceInRight',
        'bounceInUp',
        'fadeIn',
        'fadeInDown',
        'fadeInDownBig',
        'fadeInLeft',
        'fadeInLeftBig',
        'fadeInRight',
        'fadeInRightBig',
        'fadeInUp',
        'fadeInUpBig',
        'flip',
        'flipInX',
        'flipInY',
        'lightSpeedIn',
        'rotateIn',
        'rotateInDownLeft',
        'rotateInDownRight',
        'rotateInUpLeft',
        'rotateInUpRight',
        'slideInUp',
        'slideInDown',
        'slideInLeft',
        'slideInRight',
        'rollIn',
        'zoomIn',
        'zoomInDown',
        'zoomInLeft',
        'zoomInRight',
        'zoomInUp',
        'jello',
        'bounce',
        'flash',
        'pulse',
        'rubberBand',
        'shake',
        'swing',
        'tada',
        'wobble',
    );
    */

    $arrOptions = array(
        'none',
        'fade-in',
        'slide-up',
        'slide-down',
        'slide-left',
        'slide-right',
        'scale-in',
        'scale-slide-up',
        'scale-slide-down',
        'scale-slide-left',
        'scale-slide-right',
        'rotate-in',
        'roll-up',
        'roll-down',
        'roll-left',
        'roll-right',
    );

    return $arrOptions;
}
function dm_get_transition_duration_options()
{
    $arrOptions = array(
        '0.3',
        '0.4',
        '0.5',
        '0.6',
        '0.7',
        '0.8',
        '0.9',
        '1',
        '1.25',
        '1.5',
        '1.75',
        '2',
        '2.25',
        '2.5',
        '2.75',
        '3',
        '3.25',
        '3.5',
        '3.75',
        '4',
        '4.25',
        '4.5',
        '4.75',
        '5',
        '5.25',
        '5.5',
        '5.75',
        '6',
        '6.25',
        '6.5',
        '6.75',
        '7',
        '7.25',
        '7.5',
        '7.75',
        '8',
        '8.25',
        '8.5',
        '8.75',
        '9',
        '9.25',
        '9.5',
        '9.75',
        '10',
    );

    return $arrOptions;
}
function dm_get_transition_delay_options()
{
    $arrOptions = array(
        '0',
        '0.25',
        '0.5',
        '0.75',
        '1',
        '1.25',
        '1.5',
        '1.75',
        '2',
        '2.25',
        '2.5',
        '2.75',
        '3',
        '3.25',
        '3.5',
        '3.75',
        '4',
        '4.25',
        '4.5',
        '4.75',
        '5',
        '5.25',
        '5.5',
        '5.75',
        '6',
        '6.25',
        '6.5',
        '6.75',
        '7',
        '7.25',
        '7.5',
        '7.75',
        '8',
        '8.25',
        '8.5',
        '8.75',
        '9',
        '9.25',
        '9.5',
        '9.75',
        '10',
    );

    return $arrOptions;
}
function dm_get_animate_on_options()
{
    $arrOptions = array(
        'scroll',
        'click',
        'hover',
        'load',
    );

    return $arrOptions;
}
function dm_get_transition_repeat_options()
{
    $arrOptions = array(
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '10',
        'infinite',
    );

    return $arrOptions;
}
function dm_get_vc_full_params($arrSpecificParams)
{
    require get_template_directory().'/shortcodes/includes/vc_shortcode_animation_part.php';
    require get_template_directory().'/shortcodes/includes/vc_shortcode_css_part.php';
    $arrToReturn = array();

    foreach($arrSpecificParams as $crtElem)
    {
        $arrToReturn[] = $crtElem;
    }

    foreach($dm_vc_transition_params as $crtElem)
    {
        $arrToReturn[] = $crtElem;
    }

    foreach($dm_vc_css_params as $crtElem)
    {
        $arrToReturn[] = $crtElem;
    }

    return $arrToReturn;
}
function dm_get_transition_easing_options()
{
    /*
    $arrOptions = array(
        'linear',
        'ease',
        'easeIn',
        'easeInOut',
        'easeInSine',
        'easeOutSine',
        'easeInOutSine',
        'easeInQuad',
        'easeOutQuad',
        'easeInOutQuad',
        'easeInCubic',
        'easeOutCubic',
        'easeInOutCubic',
        'easeInQuart',
        'easeOutQuart',
        'easeInOutQuart',
        'easeInQuint',
        'easeOutQuint',
        'easeInOutQuint',
        'easeInExpo',
        'easeOutExpo',
        'easeInOutExpo',
        'easeInCirc',
        'easeOutCirc',
        'easeInOutCirc',
        'easeInBack',
        'easeOutBack',
        'easeInOutBack',
    );
    */

    $arrOptions = array(
        'Power0',
        'Power1',
        'Power2',
        'Power3',
        'Power4',
        'Back',
        'Elastic',
        'Bounce',
        'Rough',
        'SlowMo',
        'Stepped',
        'Circ',
        'Expo',
        'Sine',
    );

    $arrToReturn = array();
    foreach($arrOptions as $option)
    {
        if($option == 'SlowMo') {
            $arrToReturn[] = $option;
        }
        elseif($option == 'Stepped') {
            $arrToReturn[] = $option;
        }else{
            $arrToReturn[] = $option . '.easeIn';
            $arrToReturn[] = $option . '.easeOut';
            $arrToReturn[] = $option . '.easeInOut';
        }
    }

    return $arrToReturn;
}
function dm_get_easing($strEasingName)
{
    $strToReturn = 'ease';
    switch($strEasingName)
    {
        case 'linear':
            $strToReturn = 'linear';
            break;
        case 'ease':
            $strToReturn = 'ease';
            break;

        case 'easeIn':
            $strToReturn = 'ease-in';
            break;
        case 'easeOut':
            $strToReturn = 'ease-out';
            break;
        case 'easeInOut':
            $strToReturn = 'ease-in-out';
            break;

        case 'easeInQuad':
            $strToReturn = 'cubic-bezier(0.550, 0.085, 0.680, 0.530)';
            break;
        case 'easeInCubic':
            $strToReturn = 'cubic-bezier(0.550, 0.055, 0.675, 0.190)';
            break;
        case 'easeInQuart':
            $strToReturn = 'cubic-bezier(0.895, 0.030, 0.685, 0.220)';
            break;
        case 'easeInQuint':
            $strToReturn = 'cubic-bezier(0.755, 0.050, 0.855, 0.060)';
            break;
        case 'easeInSine':
            $strToReturn = 'cubic-bezier(0.470, 0.000, 0.745, 0.715)';
            break;
        case 'easeInExpo':
            $strToReturn = 'cubic-bezier(0.950, 0.050, 0.795, 0.035)';
            break;
        case 'easeInCirc':
            $strToReturn = 'cubic-bezier(0.600, 0.040, 0.980, 0.335)';
            break;
        case 'easeInBack':
            $strToReturn = 'cubic-bezier(0.600, -0.280, 0.735, 0.045)';
            break;





        case 'easeOutQuad':
            $strToReturn = 'cubic-bezier(0.250, 0.460, 0.450, 0.940)';
            break;
        case 'easeOutCubic':
            $strToReturn = 'cubic-bezier(0.215, 0.610, 0.355, 1.000)';
            break;
        case 'easeOutQuart':
            $strToReturn = 'cubic-bezier(0.165, 0.840, 0.440, 1.000)';
            break;
        case 'easeOutQuint':
            $strToReturn = 'cubic-bezier(0.230, 1.000, 0.320, 1.000)';
            break;
        case 'easeOutSine':
            $strToReturn = 'cubic-bezier(0.390, 0.575, 0.565, 1.000)';
            break;
        case 'easeOutExpo':
            $strToReturn = 'cubic-bezier(0.190, 1.000, 0.220, 1.000)';
            break;
        case 'easeOutCirc':
            $strToReturn = 'cubic-bezier(0.075, 0.820, 0.165, 1.000)';
            break;
        case 'easeOutBack':
            $strToReturn = 'cubic-bezier(0.175, 0.885, 0.320, 1.275)';
            break;



        case 'easeInOutQuad':
            $strToReturn = 'cubic-bezier(0.455, 0.030, 0.515, 0.955)';
            break;
        case 'easeInOutCubic':
            $strToReturn = 'cubic-bezier(0.645, 0.045, 0.355, 1.000)';
            break;
        case 'easeInOutQuart':
            $strToReturn = 'cubic-bezier(0.770, 0.000, 0.175, 1.000)';
            break;
        case 'easeInOutQuint':
            $strToReturn = 'cubic-bezier(0.860, 0.000, 0.070, 1.000)';
            break;
        case 'easeInOutSine':
            $strToReturn = 'cubic-bezier(0.445, 0.050, 0.550, 0.950)';
            break;
        case 'easeInOutExpo':
            $strToReturn = 'cubic-bezier(1.000, 0.000, 0.000, 1.000)';
            break;
        case 'easeInOutCirc':
            $strToReturn = 'cubic-bezier(0.785, 0.135, 0.150, 0.860)';
            break;
        case 'easeInOutBack':
            $strToReturn = 'cubic-bezier(0.680, -0.550, 0.265, 1.550)';
            break;

    }
    return $strToReturn;
}
function dm_get_svg_url($color, $fileName)
{
    $imgUrl = get_template_directory_uri()."/graphics/svg/color$color/$fileName.svg";
    return $imgUrl;
}
function dm_post_exists($postName)
{
    $posts = get_posts();
    foreach ($posts as $post)
    {
        if ($post->post_name == $postName) {
            return true;
        }
    }
    return false;
}
function dm_get_header_type()
{
    $typeToReturn = dm_get_option('header-type');
    if(isset($typeToReturn) && strlen((string)$typeToReturn) > 0)
    {
        return $typeToReturn;
    }
    else
    {
        return '1';
    }
}
function dm_get_meta($postID, $fieldName)
{
    $arrData = get_post_meta($postID, $fieldName);
    if(isset($arrData) && isset($arrData[0]))
    {
        return $arrData[0];
    }
    else
    {
        return '';
    }
}
function dm_get_font_list()
{
    $arrFonts =
        array(
            'none',
            'ABeeZee',
            'Abel',
            'Abril Fatface',
            'Aclonica',
            'Acme',
            'Actor',
            'Adamina',
            'Advent Pro',
            'Aguafina Script',
            'Akronim',
            'Aladin',
            'Aldrich',
            'Alef',
            'Alegreya',
            'Alegreya SC',
            'Alegreya Sans',
            'Alegreya Sans SC',
            'Alex Brush',
            'Alfa Slab One',
            'Alice',
            'Alike',
            'Alike Angular',
            'Allan',
            'Allerta',
            'Allerta Stencil',
            'Allura',
            'Almendra',
            'Almendra Display',
            'Almendra SC',
            'Amarante',
            'Amaranth',
            'Amatic SC',
            'Amethysta',
            'Amiri',
            'Amita',
            'Anaheim',
            'Andada',
            'Andika',
            'Angkor',
            'Annie Use Your Telescope',
            'Anonymous Pro',
            'Antic',
            'Antic Didone',
            'Antic Slab',
            'Anton',
            'Arapey',
            'Arbutus',
            'Arbutus Slab',
            'Architects Daughter',
            'Archivo Black',
            'Archivo Narrow',
            'Arimo',
            'Arizonia',
            'Armata',
            'Artifika',
            'Arvo',
            'Arya',
            'Asap',
            'Asar',
            'Asset',
            'Astloch',
            'Asul',
            'Atomic Age',
            'Aubrey',
            'Audiowide',
            'Autour One',
            'Average',
            'Average Sans',
            'Averia Gruesa Libre',
            'Averia Libre',
            'Averia Sans Libre',
            'Averia Serif Libre',
            'Bad Script',
            'Balthazar',
            'Bangers',
            'Basic',
            'Battambang',
            'Baumans',
            'Bayon',
            'Belgrano',
            'Belleza',
            'BenchNine',
            'Bentham',
            'Berkshire Swash',
            'Bevan',
            'Bigelow Rules',
            'Bigshot One',
            'Bilbo',
            'Bilbo Swash Caps',
            'Biryani',
            'Bitter',
            'Black Ops One',
            'Bokor',
            'Bonbon',
            'Boogaloo',
            'Bowlby One',
            'Bowlby One SC',
            'Brawler',
            'Bree Serif',
            'Bubblegum Sans',
            'Bubbler One',
            'Buda',
            'Buenard',
            'Butcherman',
            'Butterfly Kids',
            'Cabin',
            'Cabin Condensed',
            'Cabin Sketch',
            'Caesar Dressing',
            'Cagliostro',
            'Calligraffitti',
            'Cambay',
            'Cambo',
            'Candal',
            'Cantarell',
            'Cantata One',
            'Cantora One',
            'Capriola',
            'Cardo',
            'Carme',
            'Carrois Gothic',
            'Carrois Gothic SC',
            'Carter One',
            'Catamaran',
            'Caudex',
            'Caveat',
            'Caveat Brush',
            'Cedarville Cursive',
            'Ceviche One',
            'Changa One',
            'Chango',
            'Chau Philomene One',
            'Chela One',
            'Chelsea Market',
            'Chenla',
            'Cherry Cream Soda',
            'Cherry Swash',
            'Chewy',
            'Chicle',
            'Chivo',
            'Chonburi',
            'Cinzel',
            'Cinzel Decorative',
            'Clicker Script',
            'Coda',
            'Coda Caption',
            'Codystar',
            'Combo',
            'Comfortaa',
            'Coming Soon',
            'Concert One',
            'Condiment',
            'Content',
            'Contrail One',
            'Convergence',
            'Cookie',
            'Copse',
            'Corben',
            'Courgette',
            'Cousine',
            'Coustard',
            'Covered By Your Grace',
            'Crafty Girls',
            'Creepster',
            'Crete Round',
            'Crimson Text',
            'Croissant One',
            'Crushed',
            'Cuprum',
            'Cutive',
            'Cutive Mono',
            'Damion',
            'Dancing Script',
            'Dangrek',
            'Dawning of a New Day',
            'Days One',
            'Dekko',
            'Delius',
            'Delius Swash Caps',
            'Delius Unicase',
            'Della Respira',
            'Denk One',
            'Devonshire',
            'Dhurjati',
            'Didact Gothic',
            'Diplomata',
            'Diplomata SC',
            'Domine',
            'Donegal One',
            'Doppio One',
            'Dorsa',
            'Dosis',
            'Dr Sugiyama',
            'Droid Sans',
            'Droid Sans Mono',
            'Droid Serif',
            'Duru Sans',
            'Dynalight',
            'EB Garamond',
            'Eagle Lake',
            'Eater',
            'Economica',
            'Eczar',
            'Ek Mukta',
            'Electrolize',
            'Elsie',
            'Elsie Swash Caps',
            'Emblema One',
            'Emilys Candy',
            'Engagement',
            'Englebert',
            'Enriqueta',
            'Erica One',
            'Esteban',
            'Euphoria Script',
            'Ewert',
            'Exo',
            'Exo 2',
            'Expletus Sans',
            'Fanwood Text',
            'Fascinate',
            'Fascinate Inline',
            'Faster One',
            'Fasthand',
            'Fauna One',
            'Federant',
            'Federo',
            'Felipa',
            'Fenix',
            'Finger Paint',
            'Fira Mono',
            'Fira Sans',
            'Fjalla One',
            'Fjord One',
            'Flamenco',
            'Flavors',
            'Fondamento',
            'Fontdiner Swanky',
            'Forum',
            'Francois One',
            'Freckle Face',
            'Fredericka the Great',
            'Fredoka One',
            'Freehand',
            'Fresca',
            'Frijole',
            'Fruktur',
            'Fugaz One',
            'GFS Didot',
            'GFS Neohellenic',
            'Gabriela',
            'Gafata',
            'Galdeano',
            'Galindo',
            'Gentium Basic',
            'Gentium Book Basic',
            'Geo',
            'Geostar',
            'Geostar Fill',
            'Germania One',
            'Gidugu',
            'Gilda Display',
            'Give You Glory',
            'Glass Antiqua',
            'Glegoo',
            'Gloria Hallelujah',
            'Goblin One',
            'Gochi Hand',
            'Gorditas',
            'Goudy Bookletter 1911',
            'Graduate',
            'Grand Hotel',
            'Gravitas One',
            'Great Vibes',
            'Griffy',
            'Gruppo',
            'Gudea',
            'Gurajada',
            'Habibi',
            'Halant',
            'Hammersmith One',
            'Hanalei',
            'Hanalei Fill',
            'Handlee',
            'Hanuman',
            'Happy Monkey',
            'Headland One',
            'Henny Penny',
            'Herr Von Muellerhoff',
            'Hind',
            'Hind Siliguri',
            'Hind Vadodara',
            'Holtwood One SC',
            'Homemade Apple',
            'Homenaje',
            'IM Fell DW Pica',
            'IM Fell DW Pica SC',
            'IM Fell Double Pica',
            'IM Fell Double Pica SC',
            'IM Fell English',
            'IM Fell English SC',
            'IM Fell French Canon',
            'IM Fell French Canon SC',
            'IM Fell Great Primer',
            'IM Fell Great Primer SC',
            'Iceberg',
            'Iceland',
            'Imprima',
            'Inconsolata',
            'Inder',
            'Indie Flower',
            'Inika',
            'Inknut Antiqua',
            'Irish Grover',
            'Istok Web',
            'Italiana',
            'Italianno',
            'Itim',
            'Jacques Francois',
            'Jacques Francois Shadow',
            'Jaldi',
            'Jim Nightshade',
            'Jockey One',
            'Jolly Lodger',
            'Josefin Sans',
            'Josefin Slab',
            'Joti One',
            'Judson',
            'Julee',
            'Julius Sans One',
            'Junge',
            'Jura',
            'Just Another Hand',
            'Just Me Again Down Here',
            'Kadwa',
            'Kalam',
            'Kameron',
            'Kantumruy',
            'Karla',
            'Karma',
            'Kaushan Script',
            'Kavoon',
            'Kdam Thmor',
            'Keania One',
            'Kelly Slab',
            'Kenia',
            'Khand',
            'Khmer',
            'Khula',
            'Kite One',
            'Knewave',
            'Kotta One',
            'Koulen',
            'Kranky',
            'Kreon',
            'Kristi',
            'Krona One',
            'Kurale',
            'La Belle Aurore',
            'Laila',
            'Lakki Reddy',
            'Lancelot',
            'Lateef',
            'Lato',
            'League Script',
            'Leckerli One',
            'Ledger',
            'Lekton',
            'Lemon',
            'Libre Baskerville',
            'Life Savers',
            'Lilita One',
            'Lily Script One',
            'Limelight',
            'Linden Hill',
            'Lobster',
            'Lobster Two',
            'Londrina Outline',
            'Londrina Shadow',
            'Londrina Sketch',
            'Londrina Solid',
            'Lora',
            'Love Ya Like A Sister',
            'Loved by the King',
            'Lovers Quarrel',
            'Luckiest Guy',
            'Lusitana',
            'Lustria',
            'Macondo',
            'Macondo Swash Caps',
            'Magra',
            'Maiden Orange',
            'Mako',
            'Mallanna',
            'Mandali',
            'Marcellus',
            'Marcellus SC',
            'Marck Script',
            'Margarine',
            'Marko One',
            'Marmelad',
            'Martel',
            'Martel Sans',
            'Marvel',
            'Mate',
            'Mate SC',
            'Maven Pro',
            'McLaren',
            'Meddon',
            'MedievalSharp',
            'Medula One',
            'Megrim',
            'Meie Script',
            'Merienda',
            'Merienda One',
            'Merriweather',
            'Merriweather Sans',
            'Metal',
            'Metal Mania',
            'Metamorphous',
            'Metrophobic',
            'Michroma',
            'Milonga',
            'Miltonian',
            'Miltonian Tattoo',
            'Miniver',
            'Miss Fajardose',
            'Modak',
            'Modern Antiqua',
            'Molengo',
            'Molle',
            'Monda',
            'Monofett',
            'Monoton',
            'Monsieur La Doulaise',
            'Montaga',
            'Montez',
            'Montserrat',
            'Montserrat Alternates',
            'Montserrat Subrayada',
            'Moul',
            'Moulpali',
            'Mountains of Christmas',
            'Mouse Memoirs',
            'Mr Bedfort',
            'Mr Dafoe',
            'Mr De Haviland',
            'Mrs Saint Delafield',
            'Mrs Sheppards',
            'Muli',
            'Mystery Quest',
            'NTR',
            'Neucha',
            'Neuton',
            'New Rocker',
            'News Cycle',
            'Niconne',
            'Nixie One',
            'Nobile',
            'Nokora',
            'Norican',
            'Nosifer',
            'Nothing You Could Do',
            'Noticia Text',
            'Noto Sans',
            'Noto Serif',
            'Nova Cut',
            'Nova Flat',
            'Nova Mono',
            'Nova Oval',
            'Nova Round',
            'Nova Script',
            'Nova Slim',
            'Nova Square',
            'Numans',
            'Nunito',
            'Odor Mean Chey',
            'Offside',
            'Old Standard TT',
            'Oldenburg',
            'Oleo Script',
            'Oleo Script Swash Caps',
            'Open Sans',
            'Open Sans Condensed',
            'Oranienbaum',
            'Orbitron',
            'Oregano',
            'Orienta',
            'Original Surfer',
            'Oswald',
            'Over the Rainbow',
            'Overlock',
            'Overlock SC',
            'Ovo',
            'Oxygen',
            'Oxygen Mono',
            'PT Mono',
            'PT Sans',
            'PT Sans Caption',
            'PT Sans Narrow',
            'PT Serif',
            'PT Serif Caption',
            'Pacifico',
            'Palanquin',
            'Palanquin Dark',
            'Paprika',
            'Parisienne',
            'Passero One',
            'Passion One',
            'Pathway Gothic One',
            'Patrick Hand',
            'Patrick Hand SC',
            'Patua One',
            'Paytone One',
            'Peddana',
            'Peralta',
            'Permanent Marker',
            'Petit Formal Script',
            'Petrona',
            'Philosopher',
            'Piedra',
            'Pinyon Script',
            'Pirata One',
            'Plaster',
            'Play',
            'Playball',
            'Playfair Display',
            'Playfair Display SC',
            'Podkova',
            'Poiret One',
            'Poller One',
            'Poly',
            'Pompiere',
            'Pontano Sans',
            'Poppins',
            'Port Lligat Sans',
            'Port Lligat Slab',
            'Pragati Narrow',
            'Prata',
            'Preahvihear',
            'Press Start 2P',
            'Princess Sofia',
            'Prociono',
            'Prosto One',
            'Puritan',
            'Purple Purse',
            'Quando',
            'Quantico',
            'Quattrocento',
            'Quattrocento Sans',
            'Questrial',
            'Quicksand',
            'Quintessential',
            'Qwigley',
            'Racing Sans One',
            'Radley',
            'Rajdhani',
            'Raleway',
            'Raleway Dots',
            'Ramabhadra',
            'Ramaraja',
            'Rambla',
            'Rammetto One',
            'Ranchers',
            'Rancho',
            'Ranga',
            'Rationale',
            'Ravi Prakash',
            'Redressed',
            'Reenie Beanie',
            'Revalia',
            'Rhodium Libre',
            'Ribeye',
            'Ribeye Marrow',
            'Righteous',
            'Risque',
            'Roboto',
            'Roboto Condensed',
            'Roboto Mono',
            'Roboto Slab',
            'Rochester',
            'Rock Salt',
            'Rokkitt',
            'Romanesco',
            'Ropa Sans',
            'Rosario',
            'Rosarivo',
            'Rouge Script',
            'Rozha One',
            'Rubik',
            'Rubik Mono One',
            'Rubik One',
            'Ruda',
            'Rufina',
            'Ruge Boogie',
            'Ruluko',
            'Rum Raisin',
            'Ruslan Display',
            'Russo One',
            'Ruthie',
            'Rye',
            'Sacramento',
            'Sahitya',
            'Sail',
            'Salsa',
            'Sanchez',
            'Sancreek',
            'Sansita One',
            'Sarala',
            'Sarina',
            'Sarpanch',
            'Satisfy',
            'Scada',
            'Scheherazade',
            'Schoolbell',
            'Seaweed Script',
            'Sevillana',
            'Seymour One',
            'Shadows Into Light',
            'Shadows Into Light Two',
            'Shanti',
            'Share',
            'Share Tech',
            'Share Tech Mono',
            'Shojumaru',
            'Short Stack',
            'Siemreap',
            'Sigmar One',
            'Signika',
            'Signika Negative',
            'Simonetta',
            'Sintony',
            'Sirin Stencil',
            'Six Caps',
            'Skranji',
            'Slabo 13px',
            'Slabo 27px',
            'Slackey',
            'Smokum',
            'Smythe',
            'Sniglet',
            'Snippet',
            'Snowburst One',
            'Sofadi One',
            'Sofia',
            'Sonsie One',
            'Sorts Mill Goudy',
            'Source Code Pro',
            'Source Sans Pro',
            'Source Serif Pro',
            'Special Elite',
            'Spicy Rice',
            'Spinnaker',
            'Spirax',
            'Squada One',
            'Sree Krushnadevaraya',
            'Stalemate',
            'Stalinist One',
            'Stardos Stencil',
            'Stint Ultra Condensed',
            'Stint Ultra Expanded',
            'Stoke',
            'Strait',
            'Sue Ellen Francisco',
            'Sumana',
            'Sunshiney',
            'Supermercado One',
            'Sura',
            'Suranna',
            'Suravaram',
            'Suwannaphum',
            'Swanky and Moo Moo',
            'Syncopate',
            'Tangerine',
            'Taprom',
            'Tauri',
            'Teko',
            'Telex',
            'Tenali Ramakrishna',
            'Tenor Sans',
            'Text Me One',
            'The Girl Next Door',
            'Tienne',
            'Tillana',
            'Timmana',
            'Tinos',
            'Titan One',
            'Titillium Web',
            'Trade Winds',
            'Trocchi',
            'Trochut',
            'Trykker',
            'Tulpen One',
            'Ubuntu',
            'Ubuntu Condensed',
            'Ubuntu Mono',
            'Ultra',
            'Uncial Antiqua',
            'Underdog',
            'Unica One',
            'UnifrakturCook',
            'UnifrakturMaguntia',
            'Unkempt',
            'Unlock',
            'Unna',
            'VT323',
            'Vampiro One',
            'Varela',
            'Varela Round',
            'Vast Shadow',
            'Vesper Libre',
            'Vibur',
            'Vidaloka',
            'Viga',
            'Voces',
            'Volkhov',
            'Vollkorn',
            'Voltaire',
            'Waiting for the Sunrise',
            'Wallpoet',
            'Walter Turncoat',
            'Warnes',
            'Wellfleet',
            'Wendy One',
            'Wire One',
            'Work Sans',
            'Yanone Kaffeesatz',
            'Yantramanav',
            'Yellowtail',
            'Yeseva One',
            'Yesteryear',
            'Zeyada',
        );


    $arrToReturn = array();
    foreach($arrFonts as $initialName)
    {
        //$usableName = str_replace(' ', '+', $initialName);

        //$objFont = new stdClass();
        //$objFont->displayName = $initialName;
        //$objFont->usableName = $initialName;

        $arrToReturn[$initialName] = $initialName;
    }

    return $arrToReturn;


    return $arrFonts;
}
function dm_get_post_types()
{
    $arrToReturn = [];
    $arrPostTypes = get_post_types();
    foreach($arrPostTypes as $postType)
    {
        $arrToReturn[] = $postType;
    }
    $arrToReturn[] = 'jetpack-portfolio';
    $arrToReturn[] = 'jetpack-testimonial';

    return $arrToReturn;
    //return array('post', 'page', 'dm_portfolio');
}
function dm_get_partial_list()
{
    $arrToReturn = array('default blog template');

    $arrPartials = scandir(get_template_directory().'/partials/');
    foreach($arrPartials as $fileName)
    {
        $arrFileName = explode('.', $fileName);

        $displayName = $arrFileName[0];

        if(strlen($fileName) > 2) {
            $arrToReturn[] = $displayName;
        }
    }

    return $arrToReturn;
}
function dm_get_contact_forms()
{
    $arrToReturn = array('choose a form');
    $args = array(
        "post_type" => "wpcf7_contact_form"
    );
    $arrContactForms = get_posts($args);
    foreach($arrContactForms as $item)
    {
        $id = $item->ID;
        $title = $item->post_title;
        $result = $id. '-' .$title;
        $arrToReturn[] = $result;

    }

    return $arrToReturn;

}
/**
 * Returns a custom logo, linked to home.
 *
 * @since 4.5.0
 *
 * @param int $blog_id Optional. ID of the blog in question. Default is the ID of the current blog.
 * @return string Custom logo markup.
 */
function dm_get_custom_logo( $blog_id = 0 ) {
    $html = '';

    if ( is_multisite() && (int) $blog_id !== get_current_blog_id() ) {
        switch_to_blog( $blog_id );
    }

    $custom_logo_id = get_theme_mod( 'custom_logo' );



    // We have a logo. Logo is go.
    if ( $custom_logo_id ) {
        $html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url">%2$s</a>',
            esc_url( home_url( '/' ) ),
            '<img src="'
            .
            wp_get_attachment_image_url( $custom_logo_id, 'full', false, array(
                'class'    => 'custom-logo',
                'itemprop' => 'logo',
            ) )
            .
            '" class="custom-logo" alt="logo" itemprop="logo">'
        );
    }

    // If no logo is set but we're in the Customizer, leave a placeholder (needed for the live preview).
    elseif ( is_customize_preview() ) {
        $html = sprintf( '<a href="%1$s" class="custom-logo-link" style="display:none;"><img class="custom-logo"/></a>',
            esc_url( home_url( '/' ) )
        );
    }

    if ( is_multisite() && ms_is_switched() ) {
        restore_current_blog();
    }

    /**
     * Filter the custom logo output.
     *
     * @since 4.5.0
     *
     * @param string $html Custom logo HTML output.
     */
    return apply_filters( 'get_custom_logo', $html );
}
function dm_custom_logo()
{
    echo dm_get_custom_logo();
}
function dm_get_mobile_breakpoint_options() {
    $arrToReturn = array(
        'default' => 'default',
        'max' => 'max'
    );

    for($i = 0; $i <= 34; $i++) {
        $nrToAdd = 300 + $i * 50;
        $arrToReturn[$nrToAdd] = $nrToAdd;
    }

    return $arrToReturn;
}
function dm_get_mobile_menu_size_options() {
    $arrToReturn = array(
        'default' => 'default',
        'full-screen' => 'full-screen',
        '10%' => '10%',
        '20%' => '20%',
        '30%' => '30%',
        '40%' => '40%',
        '50%' => '50%',
    );

    for($i = 0; $i <= 11; $i++) {
        $nrToAdd = 150 + $i * 50;
        $arrToReturn[$nrToAdd.'px'] = $nrToAdd.'px';
    }

    return $arrToReturn;
}
