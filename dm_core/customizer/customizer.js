(function( $ ) {
    "use strict";

    var DM_OPTIONS = 'thema_options';

    function updateColor1(to) { updateColor(to, '1'); }
    function updateColor2(to) { updateColor(to, '2'); }
    function updateColor3(to) { updateColor(to, '3'); }
    function updateColor4(to) { updateColor(to, '4'); }

    function updateColor( to, colorIndex ) {

        var color = to;
        var colorLight = tinycolor(to).lighten(25).toString();
        var colorLighter = tinycolor(to).lighten(35).toString();
        var colorDark = tinycolor(to).darken(20).toString();
        var colorDarker = tinycolor(to).darken(35).toString();

        $( '.font-color' + colorIndex ).css( 'color', color );
        $( '.font-color' + colorIndex + 'Light' ).css( 'color',  colorLight);
        $( '.font-color' + colorIndex + 'Lighter').css( 'color', colorLighter );
        $( '.font-color' + colorIndex + 'Dark').css( 'color', colorDark );
        $( '.font-color' + colorIndex + 'Darker').css( 'color', colorDarker );

        $( '.bg-color' + colorIndex ).css( 'background-color', color );
        $( '.bg-color' + colorIndex + 'Light' ).css( 'background-color', colorLight );
        $( '.bg-color' + colorIndex + 'Lighter' ).css( 'background-color', colorLighter );
        $( '.bg-color' + colorIndex + 'Dark' ).css( 'background-color', colorDark );
        $( '.bg-color' + colorIndex + 'Darker' ).css( 'background-color', colorDarker );

        $( '.border-left-color' + colorIndex ).css( 'border-left-color', color );
        $( '.border-left-color' + colorIndex + 'Light' ).css( 'border-left-color', colorLight );
        $( '.border-left-color' + colorIndex + 'Lighter' ).css( 'border-left-color', colorLighter );
        $( '.border-left-color' + colorIndex + 'Dark' ).css( 'border-left-color', colorDark );
        $( '.border-left-color' + colorIndex + 'Darker' ).css( 'border-left-color', colorDarker );


        var arrSVGs = document.getElementsByTagName('object');

        for(var i= 0; i < arrSVGs.length; i++)
        {
            var element = arrSVGs[i];
            var path = element.getAttribute('data');
            var mustUpdate = false;
            var crtColor = '';
            if(path.indexOf('svg/color1') != 0) {
                crtColor = color;
                mustUpdate = true;
            } else if(path.indexOf('svg/color1Light') != 0) {
                crtColor = colorLight;
                mustUpdate = true;
            }else if(path.indexOf('svg/color1Lighter') != 0) {
                crtColor = colorLighter;
                mustUpdate = true;
            }else if(path.indexOf('svg/color1Dark') != 0) {
                crtColor = colorDark;
                mustUpdate = true;
            }else if(path.indexOf('svg/color1Darker') != 0) {
                crtColor = colorDarker;
                mustUpdate = true;
            }

            if(mustUpdate) {
                // Get the SVG document inside the Object tag
                var svgDoc = element.contentDocument;
                // Get one of the SVG items by ID;
                var svgItem = svgDoc.getElementById("XMLID_1_");
                // Set the colour to something else
                svgItem.setAttribute("fill", crtColor);
            }
        }
    }



    wp.customize( DM_OPTIONS + '[color-1]', function( value ) {
        value.bind( updateColor1 );
    });

    wp.customize( DM_OPTIONS + '[color-2]', function( value ) {
        value.bind( updateColor2 );
    });

    wp.customize( DM_OPTIONS + '[color-3]', function( value ) {
        value.bind( updateColor3 );
    });

    wp.customize( DM_OPTIONS + '[color-4]', function( value ) {
        value.bind( updateColor4 );
    });


    wp.customize( DM_OPTIONS + '[h1-font-size]', function( value ) {
        value.bind( updateFontSizeH1 );
    });

    wp.customize( DM_OPTIONS + '[h2-font-size]', function( value ) {
        value.bind( updateFontSizeH2 );
    });

    wp.customize( DM_OPTIONS + '[h3-font-size]', function( value ) {
        value.bind( updateFontSizeH3 );
    });

    wp.customize( DM_OPTIONS + '[h4-font-size]', function( value ) {
        value.bind( updateFontSizeH4 );
    });

    wp.customize( DM_OPTIONS + '[h5-font-size]', function( value ) {
        value.bind( updateFontSizeH5 );
    });

    wp.customize( DM_OPTIONS + '[h6-font-size]', function( value ) {
        value.bind( updateFontSizeH6 );
    });

    wp.customize( DM_OPTIONS + '[link-font-size]', function( value ) {
        value.bind( updateFontSizeLink );
    });

    wp.customize( DM_OPTIONS + '[paragraph-font-size]', function( value ) {
        value.bind( updateFontSizeParagraph );
    });

    wp.customize( DM_OPTIONS + '[header-link-size]', function( value ) {
        value.bind( function(to){
            $('header .menu>.menu-item a').css('font-size', to + 'px');
        } );
    });

    function updateFontSizeH1(to) { updateFontSize('h1', to); }
    function updateFontSizeH2(to) { updateFontSize('h2', to); }
    function updateFontSizeH3(to) { updateFontSize('h3', to); }
    function updateFontSizeH4(to) { updateFontSize('h4', to); }
    function updateFontSizeH5(to) { updateFontSize('h5', to); }
    function updateFontSizeH6(to) { updateFontSize('h6', to); }
    function updateFontSizeLink(to) { updateFontSize('a', to); }
    function updateFontSizeParagraph(to) { alert('updateFontSizeParagraph'); updateFontSize('p', to); }

    function updateFontSize(tag, to)
    {
        $(tag).css('font-size', (to + 'px'));
    }


    wp.customize( DM_OPTIONS + '[header-mobile-breakpoint]', function( value ) {
        value.bind( function(to) {
            $('header').attr('data-mobile-breakpoint', to);
            window.updateMobileHeader();
        } );
    });

    wp.customize( DM_OPTIONS + '[header-mobile-direction]', function( value ) {
        value.bind( function(to) {
            $('header').attr('data-mobile-direction', to);
            window.updateMobileHeader();
        } );
    });

    wp.customize( DM_OPTIONS + '[header-mobile-type]', function( value ) {
        value.bind( function(to) {
            $('header').attr('data-mobile-type', to);
            window.updateMobileHeader();
        } );
    });

    wp.customize( DM_OPTIONS + '[header-mobile-size]', function( value ) {
        value.bind( function(to) {
            $('header').attr('data-mobile-size', to);
            window.updateMobileHeader();
        } );
    });

    /*
    wp.customize( DM_OPTIONS + '[header_link_size]', function( value ) {
        alert('header link size A');
        value.bind( function(to){
            alert('header link size');
            $('header .menu>.menu-item>a').css('font-size', to + 'px');
        } );
    });
    */



})( jQuery );
