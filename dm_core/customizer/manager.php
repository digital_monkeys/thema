<?php
function dm_register_theme_customizer( $wp_customize ) {

    // PANELS BEGIN

    $wp_customize->add_panel( '1001', array(
        'priority'       => 10,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => 'Display',
        'description'    => 'This is where all your display options live',
    ) );
    $wp_customize->add_panel( '1002', array(
        'priority'       => 10,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => 'Header',
        'description'    => 'This is where all your header options live',
    ) );

    // PANELS END


    // SECTIONS BEGIN

    // Display
    $wp_customize->add_section( 'dm_colors', array('title' => 'Colors', 'priority'  => 10, 'panel' => '1001'));
    $wp_customize->add_section( 'dm_font_sizes', array('title' => 'Font sizes', 'priority'  => 11, 'panel' => '1001'));
    $wp_customize->add_section( 'dm_fonts', array('title' => 'Fonts', 'priority'  => 12, 'panel' => '1001'));
    $wp_customize->add_section( 'dm_other', array('title' => 'Other', 'priority'  => 14, 'panel' => '1001'));

    // Header
    $wp_customize->add_section( 'dm_header_general', array('title' => 'General Options', 'priority'  => 13, 'panel' => '1002'));
    $wp_customize->add_section( 'dm_header_submenus', array('title' => 'Submenus', 'priority'  => 14, 'panel' => '1002'));
    $wp_customize->add_section( 'dm_header_mobile', array('title' => 'Mobile Menu', 'priority'  => 15, 'panel' => '1002'));
    $wp_customize->add_section( 'dm_header_info_bar', array('title' => 'Top Info Bar', 'priority'  => 16, 'panel' => '1002'));

    // SECTIONS END

    // SETTINGS BEGIN

    $wp_customize->add_setting(DM_OPTIONS.'[color-1]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[color-2]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[color-3]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[color-4]', array('type' => 'option', 'transport' => 'postMessage'));

    $wp_customize->add_setting(DM_OPTIONS.'[h1-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[h2-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[h3-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[h4-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[h5-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[h6-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[paragraph-font-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[link-font-size]', array('type' => 'option', 'transport' => 'postMessage'));

    $wp_customize->add_setting(DM_OPTIONS.'[font-1]', array('type' => 'option', 'transport' => 'refresh'));
    $wp_customize->add_setting(DM_OPTIONS.'[font-2]', array('type' => 'option', 'transport' => 'refresh'));
    $wp_customize->add_setting(DM_OPTIONS.'[font-3]', array('type' => 'option', 'transport' => 'refresh'));


    $wp_customize->add_setting(
        DM_OPTIONS.'[header-type]',
        array(
            'default'     => '1',
            'transport'   => 'postMessage',
            'type'        => 'option',
        )
    );

    $wp_customize->add_setting(DM_OPTIONS.'[header-link-color]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-background-color]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-link-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-link-font]', array('type' => 'option', 'transport' => 'postMessage'));

    $wp_customize->add_setting(DM_OPTIONS.'[header-submenu-link-color]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-submenu-link-size]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-submenu-link-font]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-submenu-background-color]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-submenu-background-color-highlight]', array('type' => 'option', 'transport' => 'postMessage'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-submenu-link-color-highlight]', array('type' => 'option', 'transport' => 'postMessage'));

    $wp_customize->add_setting(DM_OPTIONS.'[header-is-sticky]', array('type' => 'option', 'transport' => 'postMessage', 'default' => 'yes'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-mobile-breakpoint]', array('type' => 'option', 'transport' => 'postMessage', 'default' => 'default'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-mobile-direction]', array('type' => 'option', 'transport' => 'postMessage', 'default' => 'left'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-mobile-type]', array('type' => 'option', 'transport' => 'postMessage', 'default' => 'push'));
    $wp_customize->add_setting(DM_OPTIONS.'[header-mobile-size]', array('type' => 'option', 'transport' => 'postMessage', 'default' => 'default'));

    $wp_customize->add_setting(DM_OPTIONS.'[header-bar-display-working-hours]', array('type' => 'option', 'transport' => 'postMessage', 'default' => ''));
    $wp_customize->add_setting(DM_OPTIONS.'[header-bar-display-contact-info]', array('type' => 'option', 'transport' => 'postMessage', 'default' => ''));
    $wp_customize->add_setting(DM_OPTIONS.'[header-bar-display-social-icons]', array('type' => 'option', 'transport' => 'postMessage', 'default' => ''));
    //$wp_customize->add_setting(DM_OPTIONS.'[header-sticky-background-color]', array('type' => 'option', 'transport' => 'postMessage'));
    //$wp_customize->add_setting(DM_OPTIONS.'[header-sticky-color-scheme]', array('type' => 'option', 'transport' => 'postMessage'));

    // SETTINGS END

    // CONTROLS BEGIN

    // Colors
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color-1',
            array(
                'label'      => __('Color 1 ', 'dm'),
                'section'    => 'dm_colors',
                'settings'   => DM_OPTIONS.'[color-1]',
            ) )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color-2',
            array(
                'label'      => __('Color 2 ', 'dm'),
                'section'    => 'dm_colors',
                'settings'   => DM_OPTIONS.'[color-2]',
            ) )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color-3',
            array(
                'label'      => __('Color 3 ', 'dm'),
                'section'    => 'dm_colors',
                'settings'   => DM_OPTIONS.'[color-3]',
            ) )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'color-4',
            array(
                'label'      => __('Color 4 ', 'dm'),
                'section'    => 'dm_colors',
                'settings'   => DM_OPTIONS.'[color-4]',
            ) )
    );

    // Font sizes
    $arrSizes = array();
    for($i = 10; $i <= 100; $i++)  {
        $arrSizes[$i] = $i;
    }

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'h1-font-size',
            array(
                'label'          => __('H1 font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[h1-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'h2-font-size',
            array(
                'label'          => __('H2 font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[h2-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'h3-font-size',
            array(
                'label'          => __('H3 font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[h3-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'h4-font-size',
            array(
                'label'          => __('H4 font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[h4-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'h5-font-size',
            array(
                'label'          => __('H5 font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[h5-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'h6-font-size',
            array(
                'label'          => __('H6 font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[h6-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'link-font-size',
            array(
                'label'          => __('Link font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[link-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'paragraph-font-size',
            array(
                'label'          => __('Paragraph font size (in pixels)', 'dm'),
                'section'        => 'dm_font_sizes',
                'settings'       => DM_OPTIONS.'[paragraph-font-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );

    // Fonts
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'font-1',
            array(
                'label'          => __('Font 1 (list from google fonts)', 'dm'),
                'section'        => 'dm_fonts',
                'settings'       => DM_OPTIONS.'[font-1]',
                'type'           => 'select',
                'choices'        => dm_get_font_list()
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'font-2',
            array(
                'label'          => __('Font 2 (list from google fonts)', 'dm'),
                'section'        => 'dm_fonts',
                'settings'       => DM_OPTIONS.'[font-2]',
                'type'           => 'select',
                'choices'        => dm_get_font_list()
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'font-3',
            array(
                'label'          => __('Font 3 (list from google fonts)', 'dm'),
                'section'        => 'dm_fonts',
                'settings'       => DM_OPTIONS.'[font-3]',
                'type'           => 'select',
                'choices'        => dm_get_font_list()
            )
        )
    );

    // Header GENERAL
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_is_sticky',
            array(
                'label'          => __('Sticky Header?', 'dm'),
                'section'        => 'dm_header_general',
                'settings'       => DM_OPTIONS.'[header-is-sticky]',
                'type'           => 'checkbox',
                'std'            => '1'
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_type',
            array(
                'label'          => __('Header Type', 'dm'),
                'section'        => 'dm_header_general',
                'settings'       => DM_OPTIONS.'[header-type]',
                'type'           => 'select',
                'choices'        => array(
                    '1'     => '1',
                    '2'     => '2',
                    '3'     => '3',
                )
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_link_color',
            array(
                'label'          => __('Link Color', 'dm'),
                'section'        => 'dm_header_general',
                'settings'       => DM_OPTIONS.'[header-link-color]',
                'type'           => 'select',
                'choices'        => dm_get_color_options(true)
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header-background-color',
            array(
                'label'          => __('Background Color', 'dm'),
                'section'        => 'dm_header_general',
                'settings'       => DM_OPTIONS.'[header-background-color]',
                'type'           => 'select',
                'choices'        => dm_get_color_options(true)
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_link_size',
            array(
                'label'          => __('Link size (in pixels)', 'dm'),
                'section'        => 'dm_header_general',
                'settings'       => DM_OPTIONS.'[header-link-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_link_font',
            array(
                'label'          => __( 'Link font', 'dm' ),
                'section'        => 'dm_header_general',
                'settings'       => DM_OPTIONS.'[header-link-size]',
                'type'           => 'select',
                'choices'        => dm_get_font_options(true)
            )
        )
    );

    // Header SUBMENUS
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_submenu_link_color',
            array(
                'label'          => __( 'Link Color', 'dm' ),
                'section'        => 'dm_header_submenus',
                'settings'       => DM_OPTIONS.'[header-submenu-link-color]',
                'type'           => 'select',
                'choices'        => dm_get_color_options(true)
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_submenu_link_color_highlight',
            array(
                'label'          => __( 'Link Color Highlight', 'dm' ),
                'section'        => 'dm_header_submenus',
                'settings'       => DM_OPTIONS.'[header-submenu-link-color-highlight]',
                'type'           => 'select',
                'choices'        => dm_get_color_options(true)
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_submenu_link_size',
            array(
                'label'          => __( 'Link size (in pixels)', 'dm' ),
                'section'        => 'dm_header_submenus',
                'settings'       => DM_OPTIONS.'[header-submenu-link-size]',
                'type'           => 'select',
                'choices'        => $arrSizes
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_submenu_link_font',
            array(
                'label'          => __( 'Link font', 'dm' ),
                'section'        => 'dm_header_submenus',
                'settings'       => DM_OPTIONS.'[header-submenu-link-size]',
                'type'           => 'select',
                'choices'        => dm_get_font_options(true)
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_submenu_background_color',
            array(
                'label'          => __( 'Background Color', 'dm' ),
                'section'        => 'dm_header_submenus',
                'settings'       => DM_OPTIONS.'[header-submenu-background-color]',
                'type'           => 'select',
                'choices'        => dm_get_color_options(true)
            )
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_submenu_background_color_highlight',
            array(
                'label'          => __( 'Background Color Highlight', 'dm' ),
                'section'        => 'dm_header_submenus',
                'settings'       => DM_OPTIONS.'[header-submenu-background-color-highlight]',
                'type'           => 'select',
                'choices'        => dm_get_color_options(true)
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_mobile_breakpoint',
            array(
                'label'          => __('Mobile menu breakpoint (in pixels)', 'dm'),
                'section'        => 'dm_header_mobile',
                'settings'       => DM_OPTIONS.'[header-mobile-breakpoint]',
                'type'           => 'select',
                'choices'        => dm_get_mobile_breakpoint_options()
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_mobile_direction',
            array(
                'label'          => __( 'Menu direction', 'dm' ),
                'section'        => 'dm_header_mobile',
                'settings'       => DM_OPTIONS.'[header-mobile-direction]',
                'type'           => 'select',
                'choices'        => array(
                    'left' => 'left',
                    'right' => 'right',
                    //'top' => 'top',
                )
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_mobile_type',
            array(
                'label'          => __( 'Menu type', 'dm' ),
                'section'        => 'dm_header_mobile',
                'settings'       => DM_OPTIONS.'[header-mobile-type]',
                'type'           => 'select',
                'choices'        => array(
                    'push' => 'push',
                    'squeeze' => 'squeeze',
                    'overlay' => 'overlay'
                )
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_mobile_size',
            array(
                'label'          => __( 'Menu size', 'dm' ),
                'section'        => 'dm_header_mobile',
                'settings'       => DM_OPTIONS.'[header-mobile-size]',
                'type'           => 'select',
                'choices'        => dm_get_mobile_menu_size_options()
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_bar_display_working_hours',
            array(
                'label'          => __('Display working hours', 'dm'),
                'section'        => 'dm_header_info_bar',
                'settings'       => DM_OPTIONS.'[header-bar-display-working-hours]',
                'type'           => 'checkbox',
                'std'            => '1'
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_bar_display_contact_info',
            array(
                'label'          => __('Display contact info', 'dm'),
                'section'        => 'dm_header_info_bar',
                'settings'       => DM_OPTIONS.'[header-bar-display-contact-info]',
                'type'           => 'checkbox',
                'std'            => '1'
            )
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Control(
            $wp_customize,
            'header_bar_display_social_icons',
            array(
                'label'          => __('Display social icons', 'dm'),
                'section'        => 'dm_header_info_bar',
                'settings'       => DM_OPTIONS.'[header-bar-display-social-icons]',
                'type'           => 'checkbox',
                'std'            => '1'
            )
        )
    );

    // CONTROLS END
}
add_action( 'customize_register', 'dm_register_theme_customizer' );






function dm_register_customizer_partials( WP_Customize_Manager $wp_customize ) {

    // Abort if selective refresh is not available.
    if ( ! isset( $wp_customize->selective_refresh ) ) {
        return;
    }

    $wp_customize->selective_refresh->add_partial( 'header', array(
        'selector' => '.dm_header',
        'settings' => array( DM_OPTIONS.'[header-type]' ),
        'render_callback' => function() {
            $dm_customize_headerType = dm_get_header_type();
            get_template_part('partials/header', "skin$dm_customize_headerType");
        },
    ) );
}
add_action( 'customize_register', 'dm_register_customizer_partials' );







function dm_customizer_live_preview() {

    wp_enqueue_script(
        'dm-theme-customizer',
        get_template_directory_uri() . '/dm_core/customizer/customizer.js',
        array( 'jquery', 'customize-preview' ),
        '0.3.0',
        true
    );

} // end tcx_customizer_live_preview
add_action( 'customize_preview_init', 'dm_customizer_live_preview' );

add_action('init', 'dm_replace_shortcodes');
function dm_replace_shortcodes()
{
    $arrPages = get_pages();
    $strPages = json_encode($arrPages);

}