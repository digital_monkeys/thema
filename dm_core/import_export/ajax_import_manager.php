<?php
add_action('admin_enqueue_scripts', 'dm_enqueue_importer_script');
function dm_enqueue_importer_script()
{
    $imported = get_option(DM_IMPORTED_CONTENT, 'no');
    //if($imported == 'no') {
        wp_register_script('dm_dashboard_script', get_template_directory_uri() . '/dm_core/import_export/dashboard_modifier.js', array('jquery'));
        wp_enqueue_script('dm_dashboard_script');
        wp_localize_script( 'dm_dashboard_script', 'dm', array(
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ));
    //}
}

add_action( 'wp_ajax_nopriv_dm_import_demo_content', 'dm_import_demo_content' );
add_action( 'wp_ajax_dm_import_demo_content', 'dm_import_demo_content' );

function dm_import_demo_content() {
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
        $importer = new DM_Importer();
        $importer->import(get_template_directory().'/demos/demo.zip');
        //sleep(3);
        $result = $importer->success;
        echo $result;
    }
    die();
}



add_action( 'wp_ajax_nopriv_dm_export_demo_content', 'dm_export_demo_content' );
add_action( 'wp_ajax_dm_export_demo_content', 'dm_export_demo_content' );

function dm_export_demo_content() {
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
        $exporter = new DM_Exporter();
        $link = $exporter->get_link();
        echo get_template_directory_uri().$link;
    }
    die();
}



add_action( 'wp_ajax_nopriv_dm_delete_data', 'dm_delete_data' );
add_action( 'wp_ajax_dm_delete_data', 'dm_delete_data' );

function dm_delete_data() {
    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
        $deleter = new DM_Deleter();
        $response = $deleter->deleteData();
        echo $response;
    }
    die();
}