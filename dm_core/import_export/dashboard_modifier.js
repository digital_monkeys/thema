jQuery(document).ready(function($){
   var strContentToAdd = `
		<div class="welcome-panel-column-container dm-welcome-container fade-down-big">
		<h2> Hello there, thanks for using Thema!</h2>
		<p class="about-description"> To get you up and running, you can import our demo content with a single click. </p>
		<div class="button-hero button-import-demo hvr-wobble-horizontal-always-on">
			<div class="loader loader-inline opacity0">
				<div class="loader-inner line-scale">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
			<p class="dm-label"> Import Demo Content </p>
		</div>
		<div class="button-hero button-export-demo hvr-wobble-horizontal-always-on">
			<div class="loader loader-inline opacity0">
				<div class="loader-inner line-scale">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
			<p class="dm-label"> Export Demo Content </p>
		</div>
		<div class="button-hero button-delete-data hvr-wobble-horizontal-always-on">
			<div class="loader loader-inline opacity0">
				<div class="loader-inner line-scale">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
			</div>
			<p class="dm-label"> Delete All Content </p>
		</div>
			  <br>
		<p class="dm-welcome-container"> Or you can check out
			<a class="dm-welcome-container href=" www.google.ro "> our detailed documentation </a> </p></div>
	`;
	setTimeout(addContent, 1000);
	function addContent()
	{
	$('div.welcome-panel-content').prepend(strContentToAdd);
	$('.button-import-demo').click(function() {
		//alert('button clicked');

		var btn = $(this);
		btn.find('p').addClass('fade-out');
		btn.find('.loader').addClass('fade-in');
		btn.removeClass('hvr-pulse-shrink-always-on');
		btn.removeClass('hvr-wobble-horizontal-always-on');

		$.ajax({
			url : dm.ajax_url,
			type : 'post',
			data : {
				action : 'dm_import_demo_content'
			},
			success : function( response ) {
				//alert(response);
				btn.find('p').addClass('fade-in');
				btn.find('p').removeClass('fade-out');
				btn.find('.loader').addClass('fade-out');
				btn.find('.loader').removeClass('fade-in');
				btn.find('p').text(response);
				btn.parent().find('.message-response').addClass('success fade-left');
			}
		});
	});

	$('.button-export-demo').click(function() {
		var btn = $(this);
		btn.find('p').addClass('fade-out');
		btn.find('.loader').addClass('fade-in');
		btn.removeClass('hvr-pulse-shrink-always-on');
		btn.removeClass('hvr-wobble-horizontal-always-on');

		$.ajax({
			url : dm.ajax_url,
			type : 'post',
			data : {
				action : 'dm_export_demo_content'
			},
			success : function( response ) {
				//alert(response);
				btn.find('p').addClass('fade-in');
				btn.find('p').removeClass('fade-out');
				btn.find('.loader').addClass('fade-out');
				btn.find('.loader').removeClass('fade-in');
				btn.find('p').text('You got it!');

				btn.parent().find('.message-response').addClass('success fade-left');
				window.open(response);
			}
		});
	});


		$('.button-delete-data').click(function() {
			var btn = $(this);
			btn.find('p').addClass('fade-out');
			btn.find('.loader').addClass('fade-in');
			btn.removeClass('hvr-pulse-shrink-always-on');
			btn.removeClass('hvr-wobble-horizontal-always-on');

			$.ajax({
				url : dm.ajax_url,
				type : 'post',
				data : {
					action : 'dm_delete_data'
				},
				success : function( response ) {
					//alert(response);
					btn.find('p').addClass('fade-in');
					btn.find('p').removeClass('fade-out');
					btn.find('.loader').addClass('fade-out');
					btn.find('.loader').removeClass('fade-in');
					btn.find('p').text(response);
					btn.parent().find('.message-response').addClass('success fade-left');
				}
			});
		});
   }
   
   
});
