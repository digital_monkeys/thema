<?php

/**
 * Tool used to delete all the data in the WordPress installation. This includes:
 * * posts
 * * pages
 * * menus
 * * widgets
 * * theme options
 *
 * The only method that should be called from the outside is deleteData(), which starts the whole process
 *
 * @since 1.0.0
 */
class DM_Deleter {
    /**
     * @var bool Used to store whether the process went ok or not
     */
    public $success = true;

    /**
     * @var string The message to be displayed at the end, if used
     */
    public $message = '';

    public function __construct()
    {

    }

    /**
     * @return string The message at the end of the process
     */
    public function deleteData()
    {
        // First we delete all of the posts
        $this->delete_posts();

        // Next we delete all of the pages
        $this-> delete_pages();

        // Then we empty the Media Library
        $this->delete_media_library_contents();

        // Now we delete all of the theme options
        $this-> delete_theme_options();

        // Next up: menus
        $this->delete_menus();

        // After that, we remove the widgets from their assigned sidebars
        $this->delete_widgets();
        //return '';
        if($this->success) {
            return 'Data deleted successfully!';
        } else {
            return 'Failed to delete data. Reasons: <br>'.$this->message;
        }
    }

    /**
     *
     * @return string the message
     */
    protected function delete_posts()
    {
        $arrPosts = get_posts();
        foreach($arrPosts as $post)
        {
            if(false === wp_delete_post($post->ID, true))
            {
                return 'Post delete failure!';
            }
        }
    }

    protected function delete_pages()
    {
        $arrPosts = get_pages();
        foreach($arrPosts as $post)
        {
            if(false === wp_delete_post($post->ID, true))
            {
                return 'Post delete failure!';
            }
        }
    }

    protected function delete_media_library_contents()
    {
        $query_images_args = array(
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'post_status'    => 'inherit',
            'posts_per_page' => - 1,
        );

        $query_images = new WP_Query( $query_images_args );
        foreach ( $query_images->posts as $image ) {
            if(false === wp_delete_attachment($image->ID, true))
            {
                return 'Attachment delete failure!';
            }
        }
    }

    protected function delete_theme_options()
    {
        delete_option(DM_OPTIONS);
    }

    protected function delete_menus()
    {
        $arrMenus = wp_get_nav_menus();
        foreach($arrMenus as $menu)
        {
            if(false === wp_delete_nav_menu($menu->name))
            {
                return 'Menu delete failure!';
            }
        }
    }

    protected function delete_widgets()
    {
        //$active_widgets = get_option( 'sidebars_widgets' );
        /*
        global $wp_registered_sidebars;

        foreach($wp_registered_sidebars as $sidebar)
        {
            $active_widgets[$sidebar['id']] = array();
        }
        */
        //update_option( 'sidebars_widgets', $active_widgets );
        update_option( 'sidebars_widgets', array() );


        /*
        $widgets = $GLOBALS['wp_widget_factory']->widgets;
        foreach($widgets as $widget)
        {
            $id = $widget->id_base;
            for($i = 0; $i < 10; $i++)
            {
                $option = get_option($id);
                if(isset($option))
                {
                   if(!delete_option($option))
                   {
                       //$this->success = false;
                       //$this->message .= "<br> Failure on option delete: ".$id;
                   }
                }
            }
        }
        */


        /*
        if ( ! empty ( $active_widgets[ $sidebars['a'] ] )
            or ! empty ( $active_widgets[ $sidebars['b'] ] )
        )
        {   // Okay, no fun anymore. There is already some content.
            return;
        }
        */
    }


    /*
     add_action( 'widgets_init', 't5_default_widget_demo' );

    function t5_default_widget_demo()
    {
        // Register our own widget.
        register_widget( 'T5_Demo_Widget' );

        // Register two sidebars.
        $sidebars = array ( 'a' => 'top-widget', 'b' => 'bottom-widget' );
        foreach ( $sidebars as $sidebar )
        {
            register_sidebar(
                array (
                    'name'          => $sidebar,
                    'id'            => $sidebar,
                    'before_widget' => '',
                    'after_widget'  => ''
                )
            );
        }

        // Okay, now the funny part.

        // We don't want to undo user changes, so we look for changes first.
        $active_widgets = get_option( 'sidebars_widgets' );

        if ( ! empty ( $active_widgets[ $sidebars['a'] ] )
            or ! empty ( $active_widgets[ $sidebars['b'] ] )
        )
        {   // Okay, no fun anymore. There is already some content.
            return;
        }

        // The sidebars are empty, let's put something into them.
        // How about a RSS widget and two instances of our demo widget?

        // Note that widgets are numbered. We need a counter:
        $counter = 1;

        // Add a 'demo' widget to the top sidebar …
        $active_widgets[ $sidebars['a'] ][0] = 't5_demo_widget-' . $counter;
        // … and write some text into it:
        $demo_widget_content[ $counter ] = array ( 'text' => "This works!\n\nAmazing!" );
        #update_option( 'widget_t5_demo_widget', $demo_widget_content );

        $counter++;

        // That was easy. Now a RSS widget. More fields, more fun!
        $active_widgets[ $sidebars['a'] ][] = 'rss-' . $counter;
        // The latest 15 questions from WordPress Stack Exchange.
        $rss_content[ $counter ] = array (
            'title'        => 'WordPress Stack Exchange',
            'url'          => 'http://wordpress.stackexchange.com/feeds',
            'link'         => 'http://wordpress.stackexchange.com/questions',
            'items'        => 15,
            'show_summary' => 0,
            'show_author'  => 1,
            'show_date'    => 1,
        );
        update_option( 'widget_rss', $rss_content );

        $counter++;

        // Okay, now to our second sidebar. We make it short.
        $active_widgets[ $sidebars['b'] ][] = 't5_demo_widget-' . $counter;
        #$demo_widget_content = get_option( 'widget_t5_demo_widget', array() );
        $demo_widget_content[ $counter ] = array ( 'text' => 'The second instance of our amazing demo widget.' );
        update_option( 'widget_t5_demo_widget', $demo_widget_content );

        // Now save the $active_widgets array.
        update_option( 'sidebars_widgets', $active_widgets );
     */
}