<?php

/**
 * Tool used for exporting the current state of your WordPress site
 *
 * This class will make an archive containing the following:
 *
 * * Media Library files
 * * data.txt file, which includes: widgets & widget settings, theme options, pages, posts and menus
 *
 * The exporter will also return a URL to the archive file, in order to download it
 *
 * @since 1.0.0
 * @package dm_core
 * @subpackage import_export
 *
 */
class DM_Exporter
{
    /**
     * @var object|array Used to store all the information to put in the the 'data.txt' file
     */
    public $objData = '';

    /**
     * @var string The path to the archive file to be exported file
     */
    protected $dataFilePath = '';

    /**
     * @var mixed|string The name of the exported file containing the data.
     */
    protected $dataFileName = '';

    /**
     * @var string 
     */
    protected $zipPath = '/exported/site_export.zip';

    /**
     * The constructor sets the file paths and prepares the object that will hold all of the information which will later be stored in the data.txt file
     */
    public function __construct()
    {
        $this->dataFileName = 'data.txt';
        $this->dataFilePath = "/exported/$this->dataFileName";

        $this->objData = new stdClass();
    }

    /**
     * Returns the link to the archive containing all the current site data
     *
     * The function also initializes the objData object
     *
     * @return string The path to the archive containing the export
     * @access public
     * @since 1.0.0
     */
    public function get_link()
    {
        $this->objData = new stdClass();
        $this->run_export();
        return $this->zipPath;
    }

    /**
     * The command to start the export process
     *
     * @access protected
     * @since 1.0.0
     */
    protected function run_export()
    {
        // Run all the necessary steps to add as much information to the export as possible:
        //
        $this->add_widgets_to_export();
        $this->add_theme_options_to_export();
        $this->add_pages_to_export();
        $this->add_posts_to_export();
        $this->add_menus_to_export();
        $this->make_archive();
    }


    /**
     * Used to add the page contents to the export file
     *
     * @access protected
     * @since 1.0.0
     */
    protected function add_pages_to_export()
    {
        $arrItems = get_pages();
        foreach ($arrItems as $crtItem) {
            $crtItem->page_template = get_page_template_slug($crtItem->ID);
        }
        $this->objData->pages = $arrItems;
    }

    /**
     * Used to add the post contents to the export file
     *
     * @access protected
     * @since 1.0.0
     */
    protected function add_posts_to_export()
    {
        $arrItems = get_posts();
        $this->objData->posts = $arrItems;
    }


    /**
     * Adds the menus, along with nested items and menu locations, to the export file
     *
     * @access protected
     * @since 1.0.0
     */
    protected function add_menus_to_export()
    {
        $arrMenusToAdd = array();

        $arrExistingMenus = wp_get_nav_menus();
        foreach($arrExistingMenus as $menu)
        {
            $objMenu = new stdClass();
            $objMenu->name = $menu->name;
            $objMenu->menu_str = json_encode($menu);
            $objMenu->items = wp_get_nav_menu_items($menu->slug);
            foreach($objMenu->items as $menuItem)
            {
                $originalURL = $menuItem->url;
                $parsedURL = parse_url($originalURL, PHP_URL_PATH);
                $menuItem->url = $parsedURL;
            }

            $arrMenusToAdd[] = $objMenu;
        }
        $this->objData->menus = $arrMenusToAdd;
    }


    /**
     * Add widget data, along with information about which sidebars contain which widgets, to the export file
     *
     * @access protected
     * @since 1.0.0
     */
    protected function add_widgets_to_export()
    {
        $objWidgets = new stdClass();
        $objWidgets->widget_data = array();

        $arrSidebarsWidgets = get_option('sidebars_widgets');

        foreach($arrSidebarsWidgets as $sidebar)
        {
            foreach($sidebar as $widgetName)
            {
                $baseID = substr($widgetName, 0, -2);
                $instanceID = substr($widgetName, -1);

                $widgetOption = get_option('widget_'.$baseID);
                $objWidgets->widget_data[$baseID] = $widgetOption;
            }
        }

        $objWidgets->sidebars = $arrSidebarsWidgets;
        $this->objData->widgets = $objWidgets;
    }


    /**
     * Add theme options to the export file
     *
     * Since most of the customization is done through theme options, using the Customizer, we need to add these to the
     * export as well
     *
     * @access protected
     * @since 1.0.0
     */
    protected function add_theme_options_to_export()
    {
        $this->objData->theme_options = get_option(DM_OPTIONS);
    }


    /**
     * Collect all the information from the 'objData' variable, encode it as JSON and put it in the export file
     *
     * @access protected
     *
     * @since 1.0.0
     */
    protected function make_data_file()
    {
        file_put_contents(get_template_directory().$this->dataFilePath, json_encode($this->objData));
    }


    /**
     * Collect all the Media Library items, along with the data file and compress them into an archive, ready to
     * be downloaded
     *
     * @access protected
     * @since 1.0.0
     */
    protected function make_archive()
    {
        $uploadsDir = '../wp-content/uploads';

        $zipPath = get_template_directory().$this->zipPath;

        // If we already have an archive, we delete the old one, just to make sure there is
        // no interference from older exports
        if(file_exists($zipPath))
        {
            unlink($zipPath);
        }


        $zip = new ZipArchive();
        $zip->open($zipPath, ZipArchive::CREATE);

        $query_images_args = array(
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'post_status'    => 'inherit',
            'posts_per_page' => - 1,
        );

        $query_images = new WP_Query( $query_images_args );

        $images = array();
        $this->objData->attachments = array();
        $counter = 0;
        foreach ( $query_images->posts as $image ) {
            $counter++;
            $imageURL = get_attached_file($image->ID);

            $fileName = $counter.'_'.basename($imageURL);

            $zip->addFile($imageURL, $fileName);

            $crtAttachment = new stdClass();
            $crtAttachment->name = $fileName;
            $crtAttachment->url = wp_get_attachment_url($image->ID);
            $crtAttachment->id = $image->ID;

            $this->objData->attachments[] = $crtAttachment;
        }

        $this->make_data_file();

        $zip->addFile(get_template_directory().$this->dataFilePath, $this->dataFileName);
        $zip->close();
    }
}