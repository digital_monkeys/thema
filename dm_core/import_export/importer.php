<?php
/**
 * Tool used to import the archived content and populate a WordPress installation with.
 *
 * It will import the following items:
 * * pages
 * * posts
 * * menus
 * * widgets
 * * theme options
 *
 * In order to start the process, you just have to call the import function and give it the path to the archive
 * containing the data
 *
 * @package dm_core
 * @subpackage import_export
 *
 * @since 1.0.0
 */
class DM_Importer
{
    /**
     * @var bool Used to determine whether the import operation has been successful or not
     *
     * @access public
     * @since 1.0.0
     */
    public $success = true;

    /**
     * @var string The path to the WordPress 'uploads' directory, subdirectory 'imported_content'
     *
     * @access protected
     * @since 1.0.0
     */
    protected $uploadDirPath = '';

    /**
     * @var object|array Used to store all the information from the 'data.txt' file
     * @access protected
     * @since 1.0.0
     */
    protected $objData;

    /**
     * DM_Importer constructor. The constructor just sets the path to the uploads directory, which we'll use on import
     * to extract and open the archive
     *
     * @access public
     */
    public function __construct()
    {
        // We set the upload path to WordPress' 'uploads' directory, in which we look for our 'imported_content' subfolder
        $this->uploadDirPath = wp_upload_dir()['basedir'].'/imported_content/';
    }

    /**
     * The function that sets in motion the import process
     *
     * @param string $filePath The path to the archive file containing all the data (Media Library files and
     * data.txt file)
     *
     * @access public
     *
     * @since 1.0.0
     */
    public function import($filePath)
    {
        // The first step is to extract the archive containing the demo
        // The archive contains two things:
        // 1) a file named 'data.txt', which contains all the information
        // 2) a set of files for the media library
        $this->extract_archive($filePath);

        // After extracting the archive, we need the contents of the 'data.txt' file.
        $strContent = file_get_contents($this->uploadDirPath.'data.txt');
        $this->objData = json_decode($strContent);


        // Run through all the steps necessary to import the data
        $this->import_media_library();
        $this->import_content('page');
        $this->import_content('post');
        $this->import_menus();
        $this->import_theme_options();
        $this->import_widgets();
        $this->replace_image_ids();


        // Set the "success" property to the result of the operation, and if everything went well, update the option
        // in the database
        if($this->success)
        {
            $this->success = "Success!";
            update_option( DM_IMPORTED_CONTENT, 'no');
        }
        else
        {
            $this->success = "Fail!";
        }
    }

    /**
     * Replaces the old image IDs in the posts and pages.
     *
     * At the time of the export, each Media Library item has a specific ID. When the content is imported, for
     * each Media Library item, we have a new ID, but the shortcode attributes and references are still tied to the
     * old ID, so we have to update all of them
     *
     * @access protected
     *
     * @since 1.0.0
     */
    protected function replace_image_ids()
    {
        $this->replace_ids_in_posts(get_posts());
        $this->replace_ids_in_posts(get_pages());
    }

    /**
     * Replace all of the old IDs with new ones in the set of data given as parameter
     *
     * @param array $arrData The collection of posts in which the IDs will be searched and replaced
     *
     * @access protected
     * @since 1.0.0
     */
    protected function replace_ids_in_posts($arrData)
    {
        // First, we iterate through every item in the collection
    	foreach($arrData as $crtItem)
        {
            // Save the information of the current item
            $post_id = $crtItem->ID;
            $post_content = $crtItem->post_content;

            // Go through all of the attachments in the array and replace all of the occurrences of the old IDs with the new IDs
            foreach($this->objData->attachments as $attachment)
            {
            	$attachment_old_id = $attachment->id;
                $attachment_new_id = $attachment->new_id;



                // There are several ways that image IDs are stored in shortcodes and we have to replace each of them

                // "image_id"
                // Replace every occurrence of the old id with the new one for the current attachment
                $old = 'image_id="'.$attachment_old_id.'"';
                $new = 'image_id="'.$attachment_new_id.'"';
                //echo $old.'---'.$new.'    &nbsp&nbsp';
                $post_content = str_replace($old, $new, $post_content);


                // "image"
                // Replace every occurrence of the old id with the new one for the current attachment
                $old = 'image="'.$attachment_old_id.'"';
                $new = 'image="'.$attachment_new_id.'"';
                //echo $old.'---'.$new.'    &nbsp&nbsp';
                $post_content = str_replace($old, $new, $post_content);

                // "id"
                // Replace every occurrence of the old id with the new one for the current attachment
                $old = '?id="'.$attachment_old_id.'"';
                $new = '?id="'.$attachment_new_id.'"';
                //echo $old.'---'.$new.'    &nbsp&nbsp';
                $post_content = str_replace($old, $new, $post_content);
            }

            // Make the array containing the new post's data
            $new_post_data = array(
                'ID'           => $post_id,
                'post_content' => $post_content,
            );

            // Update the post into the database
            wp_update_post( $new_post_data );
            //echo json_encode(get_post($post_id));
        }
	}

    /**
     * Used to "new_url" and "new_id" properties to the specified attachment node
     *
     * Search all of the attachments in the imported data, and when we find the one with the ID that we want, we add
     * two new properties to it, namely "new_url" and "new_id", in order to use them later when we re-establish the
     * connections between shortcodes and Media Library items
     *
     * @param string $filename The name of the attachment file
     * @param string $newID The new ID that has been assigned to the file after re-importing into the Media Library
     * @param string $newURL The new URL that has been assigned to the file after re-importing into the Media Library
     *
     * @access protected
     * @since 1.0.0
     */
    protected function addNewAttachmentIndex($filename, $newID, $newURL)
    {
        foreach($this->objData->attachments as $attachment)
        {
            if($attachment->name == $filename)
            {
                $attachment->new_id = $newID;
                $attachment->new_url = $newURL;
                break;
            }
        }
    }

    /**
     * Since the content to be imported is stored in an archive, the first thing to be done in the importing process is
     * to extract that archive to a specific path, sent to the function via the $archivePath parameter
     *
     * @param string $archivePath
     *
     * @access protected
     * @since 1.0.0
     */
    protected function extract_archive($archivePath)
    {
        $zip = new ZipArchive;
        if ($zip->open($archivePath) === TRUE) {
            $zip->extractTo($this->uploadDirPath);
            $zip->close();
            //echo 'ok   ';
        } else {
            //echo 'failed';
        }
    }

    /**
     * Most of the images contained in the website are stored in the Media Library, so we have to import all of it from
     * the exported archive
     *
     * @access protected
     * @since 1.0.0
     */
    protected function import_media_library()
    {
        // First, we read the contents of the 'imported_content' subfolder from the 'uploads' folder
        $uploadDir = scandir('../wp-content/uploads/imported_content');
        foreach ($uploadDir as $filename)
        {
            // We only want the 'real' files, that's why we check the length of the file.
            // When we scan the directory, we will also have a file named '.' and one named '..', which we want to ignore
            if (strlen($filename) > 2 && strpos($filename, '.') != 0) {
                // $filename should be the path to a file in the upload directory.
                $filename = $this->uploadDirPath.$filename;

                // The ID of the post this attachment is for.
                $parent_post_id = 0;

                // Check the type of file. We'll use this as the 'post_mime_type'.
                $filetype = wp_check_filetype(basename($filename), null);

                // Prepare an array of post data for the attachment.
                $attachment = array(
                    'guid' => $this->uploadDirPath . '/' . basename($filename),
                    'post_mime_type' => $filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );

                // Insert the attachment.
                $attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);

                // Retrieve the URl of the attachment
                $attach_url = wp_get_attachment_url($attach_id);


                // Attach the newly-generated ID and URL to their corresponding node in the data file for later use
                $this->addNewAttachmentIndex(basename($filename), $attach_id, $attach_url);

                // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
                require_once(ABSPATH . 'wp-admin/includes/image.php');

                // Generate the metadata for the attachment, and update the database record.
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);

                // Then just update the featured image for the newly-inserted with the newly-generated data
                set_post_thumbnail($parent_post_id, $attach_id);
            }
        }
    }

    /**
     * Import content based on the content's name
     *
     * @param string $contentName Either 'page' or 'post', used to select the collection of data to be imported
     *
     * @access protected
     * @since 1.0.0
     */
    protected function import_content($contentName)
    {
        // Based on $contentName, we read a different set of values to import (either posts or pages)
        if($contentName == 'post')
        {
            if(isset($this->objData->posts))
            {
                $arrItems = $this->objData->posts;
            }
            else
            {
                return;
            }
        }
        elseif($contentName == 'page')
        {
            if(isset($this->objData->pages))
            {
                $arrItems = $this->objData->pages;
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }


        // Now for every item in the set of elements that we've read, we create an array containing its data,
        // then just create that specific post item (be it post or page)
        foreach($arrItems as $item)
        {
            $newItem                        = [];
            $newItem['post_type']           = $contentName;
            $newItem['post_status']         = $item->post_status;
            $newItem['post_title']          = $item->post_title;
            $newItem['post_name']           = $item->post_name;
            $newItem['post_content']        = $item->post_content;
            $newItem['post_date']           = $item->post_date;
            $newItem['post_date_gmt']       = $item->post_date_gmt;
            $newItem['post_modified']       = $item->post_modified;
            $newItem['post_modified_gmt']   = $item->post_modified_gmt;
            $newItem['comment_status']      = $item->comment_status;
            $newItem['post_author']         = $item->post_author;

            // If the item is a page, we look for a page template
            if($contentName == 'page')
            {
                $newItem['page_template']       = $item->page_template;
            }

            // We only create that post if it doesn't already exist, otherwise we would wind up with duplicates if we
            // run the importer multiple times
            if(!dm_post_exists($newItem['post_name']))
            {
                // If the post insertion went well, WordPress will return the post id, so we can check it to see
                // if everything went well
                $added_id = wp_insert_post($newItem);
                if($added_id == 0)
                {
                    $success = false;
                }
            }
        }
    }


    /**
     * Import menus, including nested menus up to any depth
     *
     * @access protected
     * @since 1.0.0
     */
    protected function import_menus()
    {
        // We get the menu from our data object
        $menus = $this->objData->menus;

        foreach($menus as $menu)
        {
            // We get the menu's name
            $menuName = $menu->name;

            // First, we check if the menu exists
            $menu_exists = wp_get_nav_menu_object( $menuName );

            // If it doesn't exist, let's create it.
            if( !$menu_exists){
                // If the operation has succeeded, we now have the menu ID
                $menuID = wp_create_nav_menu($menuName);

                // In order to get the menu's term id, we have to retrieve the menu object first
                $menuObject = wp_get_nav_menu_object($menuID);

                // The menu's term id will be useful when we want to find it's location
                $menuTermID = $menuObject->term_id;

                // Read the locations
                $locations = get_theme_mod('nav_menu_locations');

                // Assign the menu we want to our location
                $locations['main-menu'] = $menuTermID;

                // Update the theme mod with our new preferences
                set_theme_mod( 'nav_menu_locations', $locations );

                // Now for the menu items:
                // First we create the top-level menu items, in order to get their IDs

                foreach($menu->items as $menuItem) {
                    // If the menu_item_parent is 0, it means that the menu item is top-level
                    if($menuItem->menu_item_parent == 0)
                    {
                        // Set up menu items
                        $argsMenuItem = array(
                            'menu-item-title' => $menuItem->title,
                            'menu-item-url' => home_url($menuItem->url),
                            'menu-item-status' => 'publish'
                        );

                        // Create the menu item and read its id
                        $id = wp_update_nav_menu_item($menuID, 0, $argsMenuItem);

                        // Assign the value of the id we've just read to its corresponding menu item in the array
                        // in order to keep track of it
                        $menuItem->newID = $id;
                    }
                }


                // Now we are interested in the child menu items
                foreach($menu->items as $menuItem) {

                    // If the menu_item_parent is other than 0, it means that the menu item has a parent,
                    // so it's not top-level
                    if($menuItem->menu_item_parent != 0)
                    {
                        $parentID = $this->getNewMenuItemID($menu->items, $menuItem->menu_item_parent);

                        // Set up the data for the current menu item
                        $argsMenuItem = array(
                            'menu-item-title' => $menuItem->title,
                            'menu-item-url' => home_url($menuItem->url),
                            'menu-item-status' => 'publish',
                            'menu-item-parent-id' => $parentID, // Read the new menu id of that element
                        );

                        // Now just create the menu item, based on the data we've assigned to it
                        wp_update_nav_menu_item($menuID, 0, $argsMenuItem);
                    }
                }


            }
        }
    }

    /**
     * Used to establish the correlation between the old ID of a menu item and its new value
     *
     * @param array|object $menuItems The collection of menu items to be searched
     * @param $oldID
     * @return int The new item ID
     *
     * @access protected
     * @since 1.0.0
     */
    protected function getNewMenuItemID($menuItems, $oldID)
    {
        // We assume that the new ID is 0
        $newID = 0;

        foreach($menuItems as $menuItem)
        {
            if($oldID == $menuItem->ID)
            {
                // When we find our item, we read its new id and assign it to our variable
                $newID = $menuItem->newID;
                break;
            }
        }

        // Now just return the newly-found id
        return $newID;
    }

    /**
     * Since the customization is done using the Customizer and it stores options as theme options, when we import the
     * content, we have to take care of the theme options. These are stored as an array in an option. The name of the
     * option follows this pattern: theme_name_options (example: thema_options)
     *
     * @access protected
     * @since 1.0.0
     */
    protected function import_theme_options()
    {
        // We just have to cast the object containing the theme options as an array, then put it into the database
        update_option(DM_OPTIONS, (array)$this->objData->theme_options);
    }


    /**
     * Import the widget settings and the sidebars to which they are assigned
     *
     * @access protected
     * @since 1.0.0
     */
    protected function import_widgets()
    {
        // Read the widgets information into a variable
        $objWidgetData = $this->objData->widgets;

        // Read the widgets into an array
        $arrWidgets = $objWidgetData->widge_data;

        // Read the sidebars into their own array
        $arrSidebars = (array)$objWidgetData->sidebars;

        // Now update the 'sidebars widgets' option with what we have from the imported data
        update_option( 'sidebars_widgets', $arrSidebars );

        // After we've set the sidebar information, we have to set up and configure the widgets
        foreach($arrWidgets as $baseID => $objInstances)
        {
            // We have to cast the object containing the instances to an array, then assign it to its specific option
            update_option( 'widget_'.$baseID, (array)$objInstances);
        }
    }
}