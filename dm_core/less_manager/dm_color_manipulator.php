<?php
class dm_color_manipulator
{
    public function __construct()
    {

    }

    public function darken($color, $delta) {
        $color = $this->HEX2RGB($color);
        $hsl = $this->toHSL($color);
        $hsl[3] = $this->clamp($hsl[3] - $delta, 100);
        $rgb = $this->toRGB($hsl);
        $result = $this->RGBtoHEX($rgb);
        return $result;
    }

    public function lighten($color, $delta) {
        $color = $this->HEX2RGB($color);
        $hsl = $this->toHSL($color);
        $hsl[3] = $this->clamp($hsl[3] + $delta, 100);
        $rgb = $this->toRGB($hsl);
        $result = $this->RGBtoHEX($rgb);
        return $result;
    }

    public function saturate($color, $delta) {
        $hsl = $this->toHSL($color);
        $hsl[2] = $this->clamp($hsl[2] + $delta, 100);
        return $this->toRGB($hsl);
    }

    public function desaturate($color, $delta) {
        $hsl = $this->toHSL($color);
        $hsl[2] = $this->clamp($hsl[2] - $delta, 100);
        return $this->toRGB($hsl);
    }

    public function spin($color, $delta) {
        $hsl = $this->toHSL($color);
        $hsl[1] = $hsl[1] + $delta % 360;
        if ($hsl[1] < 0) $hsl[1] += 360;
        return $this->toRGB($hsl);
    }

    public function fadeout($color, $delta) {
        $color[4] = $this->clamp((isset($color[4]) ? $color[4] : 1) - $delta/100);
        return $color;
    }

    public function fadein($color, $delta) {
        $color[4] = $this->clamp((isset($color[4]) ? $color[4] : 1) + $delta/100);
        return $color;
    }

    public function hue($color) {
        $hsl = $this->toHSL($this->assertColor($color));
        return round($hsl[1]);
    }

    public function saturation($color) {
        $hsl = $this->toHSL($this->assertColor($color));
        return round($hsl[2]);
    }

    public function lightness($color) {
        $hsl = $this->toHSL($this->assertColor($color));
        return round($hsl[3]);
    }

    protected function toHSL($color) {
        if ($color[0] == 'hsl') return $color;

        $r = $color[1] / 255;
        $g = $color[2] / 255;
        $b = $color[3] / 255;

        $min = min($r, $g, $b);
        $max = max($r, $g, $b);

        $L = ($min + $max) / 2;
        if ($min == $max) {
            $S = $H = 0;
        } else {
            if ($L < 0.5)
                $S = ($max - $min)/($max + $min);
            else
                $S = ($max - $min)/(2.0 - $max - $min);

            if ($r == $max) $H = ($g - $b)/($max - $min);
            elseif ($g == $max) $H = 2.0 + ($b - $r)/($max - $min);
            elseif ($b == $max) $H = 4.0 + ($r - $g)/($max - $min);

        }

        $out = array('hsl',
            ($H < 0 ? $H + 6 : $H)*60,
            $S*100,
            $L*100,
        );

        if (count($color) > 4) $out[] = $color[4]; // copy alpha
        return $out;
    }

    /**
     * Converts a hsl array into a color value in rgb.
     * Expects H to be in range of 0 to 360, S and L in 0 to 100
     */
    protected function toRGB($color) {
        if ($color[0] == 'color') return $color;

        $H = $color[1] / 360;
        $S = $color[2] / 100;
        $L = $color[3] / 100;

        if ($S == 0) {
            $r = $g = $b = $L;
        } else {
            $temp2 = $L < 0.5 ?
                $L*(1.0 + $S) :
                $L + $S - $L * $S;

            $temp1 = 2.0 * $L - $temp2;

            $r = $this->toRGB_helper($H + 1/3, $temp1, $temp2);
            $g = $this->toRGB_helper($H, $temp1, $temp2);
            $b = $this->toRGB_helper($H - 1/3, $temp1, $temp2);
        }

        // $out = array('color', round($r*255), round($g*255), round($b*255));
        $out = array('color', $r*255, $g*255, $b*255);
        if (count($color) > 4) $out[] = $color[4]; // copy alpha
        return $out;
    }


    protected function toRGB_helper($comp, $temp1, $temp2) {
        if ($comp < 0) $comp += 1.0;
        elseif ($comp > 1) $comp -= 1.0;

        if (6 * $comp < 1) return $temp1 + ($temp2 - $temp1) * 6 * $comp;
        if (2 * $comp < 1) return $temp2;
        if (3 * $comp < 2) return $temp1 + ($temp2 - $temp1)*((2/3) - $comp) * 6;

        return $temp1;
    }


    protected function RGBtoHEX($arrColor)
    {
        $red = $arrColor[1];
        $green = $arrColor[2];
        $blue = $arrColor[3];

        $hex = "#";
        $hex .= str_pad(dechex($red), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($green), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($blue), 2, "0", STR_PAD_LEFT);

        return $hex; // returns the hex value including the number sign (#)
    }

    function HEX2RGB($hex) {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array('color', $r, $g, $b);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $rgb; // returns an array with the rgb values
    }


    protected function clamp($v, $max = 1, $min = 0) {
        return min($max, max($min, $v));
    }

}