<?php
dm_tint_image_resources();
function dm_tint_image_resources()
{
    $arrColors = array(
        'dm-color-1',
        'dm-color-1-light',
        'dm-color-1-lighter',
        'dm-color-1-dark',
        'dm-color-1-darker',
        'dm-color-2',
        'dm-color-2-light',
        'dm-color-2-lighter',
        'dm-color-2-dark',
        'dm-color-2-darker',
        'dm-color-3',
        'dm-color-3-light',
        'dm-color-3-lighter',
        'dm-color-3-dark',
        'dm-color-3-darker',
        'dm-color-4',
        'dm-color-4-light',
        'dm-color-4-lighter',
        'dm-color-4-dark',
        'dm-color-4-darker',
        'dm-color-gray-light',
        'dm-color-gray-dark',
        'dm-color-white',
        'dm-color-black',
    );

    $strImagesDirPathDefault = get_template_directory().'/graphics/svg/default';
    $strImagesDirPathColor = get_template_directory().'/graphics/svg/';



    $arrImagesDir = scandir($strImagesDirPathDefault);
    foreach ($arrImagesDir as $fileName)
    {

        if(strlen($fileName) > 2 && strpos($fileName, '.') != 0) {
            $filePath = $strImagesDirPathDefault.'/'.$fileName;

            $crtFile = fopen($filePath, "r+") or die("Unable to open file!");
            $crtFileContents = fread($crtFile,filesize($filePath));
            foreach($arrColors as $crtColor)
            {
                $newFilePath = $strImagesDirPathColor.$crtColor.'/'.$fileName;
                file_put_contents($newFilePath, str_replace('#FF0000', $GLOBALS[$crtColor], $crtFileContents));
            }

            fclose($crtFile);
        }
    }
}
