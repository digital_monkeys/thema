<?php
// First we include the compiler
require_once 'lessc.inc.php';

// Then we create an array of file paths to compile into the final CSS file
$dm_arrLessFiles = array(
    get_template_directory_uri() . '/less/variables.less',
    get_template_directory_uri() . '/less/transitions.css',
    get_template_directory_uri() . '/less/general.less',
    get_template_directory_uri() . '/less/parallax.less',
    get_template_directory_uri() . '/less/header.less',
    //get_template_directory_uri() . '/dm_core/less_manager/less/components.less'
);

$dm_less_components_directory = scandir(get_template_directory().'/less/components');
foreach ($dm_less_components_directory as $filename)
{
    if(strlen($filename) > 2) {
        $dm_arrLessFiles[] =  get_template_directory().'/less/components/'.$filename;
    }
}

// We instantiate the LESS compiler
$lessCompiler = new lessc();
$lessCompiler->setPreserveComments(true);
$lessCompiler->setFormatter('compressed');

// We create a variable to hold the unified LESS contents
$strEntireLess = '';

// We loop through all the files listed in the array declared beforehand, adding all their contents to the $entireLess variable
foreach ($dm_arrLessFiles as $file) {
    $strEntireLess .= file_get_contents($file);
}

// Now we parse the LESS code to pure CSS
$outputCSS = $lessCompiler->parse($strEntireLess);

// The final step is to export the CSS code to a .css file
file_put_contents(get_template_directory() . '/style_exported.css', $outputCSS);


