<?php
$strLessVariables = '';

// Here we create an array containing all the variables that have to be outputted in the variables.less file
$dm_strLessVariables =
    '
    [
        {
            "name": "content_width",
            "defaultValue": "1200px"
        },
        {
            "name": "h1-font-size",
            "defaultValue": "40"
        },
        {
            "name": "h2-font-size",
            "defaultValue": "35"
        },
        {
            "name": "h3-font-size",
            "defaultValue": "30"
        },
        {
            "name": "h4-font-size",
            "defaultValue": "25"
        },
        {
            "name": "h5-font-size",
            "defaultValue": "22"
        },
        {
            "name": "h6-font-size",
            "defaultValue": "19"
        },
        {
            "name": "paragraph-font-size",
            "defaultValue": "16"
        },
        {
            "name": "link-font-size",
            "defaultValue": "16"
        },
        {
            "name": "header_link_size",
            "defaultValue": "16"
        }
    ]
    ';

// Then we parse the JSON above
$dm_arrLessVariables = json_decode($dm_strLessVariables);

// For every element in the array above, we create a LESS variable
foreach($dm_arrLessVariables as $objVariable)
{
    $variableName = $objVariable->name;
    $defaultValue = $objVariable->defaultValue;

    if(strlen(dm_get_option($variableName)) > 0) {
        $strLessVariables .= '@'.$variableName.': ' . dm_get_option($variableName) . ";\n";
    } else {
        $strLessVariables .= '@' . $variableName . ':' . $defaultValue . ";\n";
    }
}

$strLessVariables .= '@'.'font-1: "' . dm_get_option('font-1') . "\";\n";
$strLessVariables .= '@'.'font-2: "' . dm_get_option('font-2') . "\";\n";
$strLessVariables .= '@'.'font-3: "' . dm_get_option('font-3') . "\";\n";



// For the color schemes, it's a little different.
// We have the definitions for the color schemes stored somewhere and we have the index of the selected color scheme.
// Then we create the LESS variables that correspond to each color of the color scheme.
// If the color scheme exists, we read its colors. If not, we give each color a default value.


if(strlen(dm_get_option('color-1')) > 0) {
    $GLOBALS["dm-color-1"] = dm_get_option('color-1');
} else {
    $GLOBALS["dm-color-1"] = '#333333';
}

if(strlen(dm_get_option('color-2')) > 0) {
    $GLOBALS["dm-color-2"] = dm_get_option('color-2');
} else {
    $GLOBALS["dm-color-2"] = '#666666';
}

if(strlen(dm_get_option('color-3')) > 0) {
    $GLOBALS["dm-color-3"] = dm_get_option('color-3');
} else {
    $GLOBALS["dm-color-3"] = '#999999';
}

if(strlen(dm_get_option('color-4')) > 0) {
    $GLOBALS["dm-color-4"] = dm_get_option('color-4');
} else {
    $GLOBALS["dm-color-4"] = '#aaaaaa';
}


// First we include the class to manipulate colors
require_once 'dm_color_manipulator.php';
$dm_color_manipulator = new dm_color_manipulator();
//echo ( $dm_color_manipulator->lighten($GLOBALS["dm_color1"], '25%'));


$GLOBALS["dm-color-1-light"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-1"], '25%');
$GLOBALS["dm-color-1-lighter"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-1"], '35%');
$GLOBALS["dm-color-1-dark"] = $dm_color_manipulator->darken($GLOBALS["dm-color-1"], '20%');;
$GLOBALS["dm-color-1-darker"] = $dm_color_manipulator->darken($GLOBALS["dm-color-1"], '35%');

$GLOBALS["dm-color-2-light"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-2"], '25%');
$GLOBALS["dm-color-2-lighter"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-2"], '35%');
$GLOBALS["dm-color-2-dark"] = $dm_color_manipulator->darken($GLOBALS["dm-color-2"], '20%');;
$GLOBALS["dm-color-2-darker"] = $dm_color_manipulator->darken($GLOBALS["dm-color-2"], '35%');

$GLOBALS["dm-color-3-light"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-3"], '25%');
$GLOBALS["dm-color-3-lighter"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-3"], '35%');
$GLOBALS["dm-color-3-dark"] = $dm_color_manipulator->darken($GLOBALS["dm-color-3"], '20%');;
$GLOBALS["dm-color-3-darker"] = $dm_color_manipulator->darken($GLOBALS["dm-color-3"], '35%');

$GLOBALS["dm-color-4-light"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-4"], '25%');
$GLOBALS["dm-color-4-lighter"] = $dm_color_manipulator->lighten($GLOBALS["dm-color-4"], '35%');
$GLOBALS["dm-color-4-dark"] = $dm_color_manipulator->darken($GLOBALS["dm-color-4"], '20%');;
$GLOBALS["dm-color-4-darker"] = $dm_color_manipulator->darken($GLOBALS["dm-color-4"], '35%');



$strLessVariables .= "\n";
$strLessVariables .= "\n";

$strLessVariables .= '@color-1: '         . $GLOBALS["dm-color-1"]         . ";\n";
$strLessVariables .= '@color-1-light: '   . $GLOBALS["dm-color-1-light"]    . ";\n";
$strLessVariables .= '@color-1-lighter: ' . $GLOBALS["dm-color-1-lighter"]  . ";\n";
$strLessVariables .= '@color-1-dark: '    . $GLOBALS["dm-color-1-dark"]     . ";\n";
$strLessVariables .= '@color-1-darker: '  . $GLOBALS["dm-color-1-darker"]   . ";\n";

$strLessVariables .= "\n";

$strLessVariables .= '@color-2: '         . $GLOBALS["dm-color-2"]         . ";\n";
$strLessVariables .= '@color-2-light: '   . $GLOBALS["dm-color-2-light"]    . ";\n";
$strLessVariables .= '@color-2-lighter: ' . $GLOBALS["dm-color-2-lighter"]  . ";\n";
$strLessVariables .= '@color-2-dark: '    . $GLOBALS["dm-color-2-dark"]     . ";\n";
$strLessVariables .= '@color-2-darker: '  . $GLOBALS["dm-color-2-darker"]   . ";\n";

$strLessVariables .= "\n";

$strLessVariables .= '@color-3: '         . $GLOBALS["dm-color-3"]         . ";\n";
$strLessVariables .= '@color-3-light: '   . $GLOBALS["dm-color-3-light"]    . ";\n";
$strLessVariables .= '@color-3-lighter: ' . $GLOBALS["dm-color-3-lighter"]  . ";\n";
$strLessVariables .= '@color-3-dark: '    . $GLOBALS["dm-color-3-dark"]     . ";\n";
$strLessVariables .= '@color-3-darker: '  . $GLOBALS["dm-color-3-darker"]   . ";\n";

$strLessVariables .= "\n";

$strLessVariables .= '@color-4: '         . $GLOBALS["dm-color-4"]         . ";\n";
$strLessVariables .= '@color-4-light: '   . $GLOBALS["dm-color-4-light"]    . ";\n";
$strLessVariables .= '@color-4-lighter: ' . $GLOBALS["dm-color-4-lighter"]  . ";\n";
$strLessVariables .= '@color-4-dark: '    . $GLOBALS["dm-color-4-dark"]     . ";\n";
$strLessVariables .= '@color-4-darker: '  . $GLOBALS["dm-color-4-darker"]   . ";\n";

$strLessVariables .= "\n";

$strLessVariables .= '@color-gray-light: ' . $GLOBALS["dm-color-gray-light"] . ";\n";
$strLessVariables .= '@color-gray-dark: '  . $GLOBALS["dm-color-gray-dark"]  . ";\n";
$strLessVariables .= '@color-white: '      . $GLOBALS["dm-color-white"]     . ";\n";
$strLessVariables .= '@color-black: '      . $GLOBALS["dm-color-black"]     . ";\n";


// Finally, we write the contents of the string that we have created to the variables.less file,
// which is going to be included among the other LESS files
file_put_contents(get_template_directory() . '/less/variables.less', $strLessVariables);