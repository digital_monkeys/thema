<?php
add_action('update_option_'.DM_OPTIONS, 'compile_less');

/**
 * Gather and compile all the LESS files into one final style.css file
 * @since 1.0.0
 * @access private
 */
function compile_less()
{
    // First we require all the necessary files

    // This file generates the "variables.less" file containing all the variables that will be used in LESS
    require_once 'less_variables.php';

    // Grabs all the required LESS and CSS files needed to make one final style.css file
    require_once 'less_compile_manager.php';

    // Needed to create the color variations of all the .svg graphic resources
    require_once 'image_tint.php';

    // Now we just have to output the final files: the style one and the script one
    mergeScripts();
    mergeStyles();
}




