<?php
// Added to extend allowed files types in Media upload
add_filter('upload_mimes', 'dm_custom_upload_mimes');
function dm_custom_upload_mimes ( $existing_mimes=array() ) {

// Add *.EPS files to Media upload
$existing_mimes['zip'] = 'application/postscript';

return $existing_mimes;
}