<?php
/**
 * Merges all the JavaScript files into one file for reducing the number of HTTPRequests. Creates a file named "dm_scripts.js"
 *
 * @since 1.0.0
 */
function mergeScripts()
{
    // Grab all the files and put them in an array
    $arrFiles = array(
        // External dependencies
        '/dm_core/external/libraries/js/bootstrap.min.js',
        '/dm_core/external/libraries/js/TweenMax.min.js',
        '/dm_core/external/libraries/js/EasePack.min.js',
        '/dm_core/external/plugins_to_run/js/jquery.visible.min.js',
        '/dm_core/external/plugins_to_run/js/circle_progress.js',
        '/dm_core/external/plugins_to_run/js/masonry.pkgd.min.js',
        '/dm_core/external/plugins_to_run/js/isotope.pkgd.min.js',
        '/dm_core/external/plugins_to_run/js/tinycolor-min.js',


        // Theme scripts
        '/js/component_core.js',
        '/js/components.js',
        '/js/preloader.js',
        '/js/header_mobile.js',
        '/js/post_grid.js',
        '/js/header.js',
        '/js/slider.js',
        '/js/animate.js',

    );

    // Create a string container for the resulting collection of items
    $strContents = '';

    // Iterate through the array of scripts and add them to the string container
    foreach ($arrFiles as $filePath) {
        $completePath = get_template_directory() . $filePath;
        $strContents .= file_get_contents($completePath);
    }

    // Now just save the contents of the string container to the output file
    file_put_contents(get_template_directory().'/js/dm_scripts.js', $strContents);
}


/**
 * Merges all the CSS files into one file for reducing the number of HTTPRequests. Creates a file named "style.js"
 *
 * At the beginning of the file, mergeStyles() will put the contents of the style_comments.txt file for meta-information about
 * the theme.
 * Since the theme is based on LESS, the actual CSS files that this function mergers into one are just the external
 * libraries (for example Bootstrap, FontAwesome, maybe others) and the output of the LESS compiler, which is
 * the "style_exported.css" file
 *
 * @since 1.0.0
 */
function mergeStyles()
{
    // Grab all the files and put them in an array
    $arrFiles = array(
        '/style_comments.txt',
        '/dm_core/external/libraries/css/bootstrap.min.css',
        '/dm_core/external/libraries/css/font-awesome.min.css',
        '/style_exported.css',
    );

    // Create a string container for the resulting collection of items
    $strContents = '';

    // Iterate through the array of styles and add them to the string container
    foreach ($arrFiles as $filePath) {
        $completePath = get_template_directory() . $filePath;
        $strContents .= file_get_contents($completePath);
    }

    // Now just save the contents of the string container to the output file
    file_put_contents(get_template_directory().'/style.css', $strContents);
}