<?php
$dm_style_options = json_decode($_GET['options']);

$dm_color1 = $dm_color_scheme->color1;
$dm_color2 = $dm_color_scheme->color2;
$dm_color3 = $dm_color_scheme->color3;
$dm_color4 = $dm_color_scheme->color4;

$dm_size_h1 = $dm_style_options->h1-font-size;
$dm_size_h2 = $dm_style_options->h2-font-size;
$dm_size_h3 = $dm_style_options->h3-font-size;
$dm_size_h4 = $dm_style_options->h4-font-size;

$dm_size_p = $dm_style_options->p_font_size;
$dm_size_a = $dm_style_options->a_font_size;

$dm_font1 = "";
$dm_font2 = "";