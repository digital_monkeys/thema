<?php
function dm_register_theme_menus() {
    register_nav_menus(
        array(
            'main-menu' => _('Main Menu')
        )

    );
}
add_action('init', 'dm_register_theme_menus');
