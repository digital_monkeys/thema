<?php
function dm_enqueue_scripts()
{
    wp_enqueue_script('all_scripts', get_template_directory_uri() . '/js/dm_scripts.js', array('jquery'), '', false);

    wp_register_script('masonry_use', get_template_directory_uri() . '/js/masonry_use.js', array(), '', true);
    wp_register_script('masonry_grid', get_template_directory_uri() . '/dm_core/external/plugins_to_run/js/masonry.pkgd.min.js', array(), '', true);
}

add_action('wp_enqueue_scripts', 'dm_enqueue_scripts');
add_action('admin_enqueue_scripts', 'dm_enqueue_scripts');