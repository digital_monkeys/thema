<?php
add_action('wp_enqueue_scripts', 'dm_theme_styles');
//add_action('admin_enqueue_scripts', 'dm_theme_styles');
function dm_theme_styles() {
    /*
    // Libraries
    wp_enqueue_style('bootstrap_css', get_template_directory_uri().'/dm_core/external/libraries/css/bootstrap.min.css');
    wp_enqueue_style('fontawesome_css', get_template_directory_uri().'/dm_core/external/libraries/css/font-awesome.min.css');

    // Plugins
    wp_enqueue_style('hover_css', get_template_directory_uri() .'/dm_core/external/plugins_to_run/css/hover.css');
    wp_enqueue_style('loaders_css', get_template_directory_uri() .'/dm_core/external/plugins_to_run/css/loaders.css');
    */

    // Fonts
    // Our main purpose is to build the link for Google Fonts by reading the selected fonts
    $fontList = '';
    $fonts_missing = 0;

    $font1 = dm_get_option('font-1');
    if($font1 != 'none') {
        $fontList .= str_replace(' ', '+', $font1).':100, 200, 300, 400, 500, 600, 700, 800, 900';
    }
    else
    {
        $fontList .= 'Roboto:100, 200, 300, 400, 500, 600, 700, 800, 900';
        $fonts_missing++;
    }

    $font2 = dm_get_option('font-2');
    if($font2 != 'none') {
        $fontList .= '|'.str_replace(' ', '+', $font2) . ':100, 200, 300, 400, 500, 600, 700, 800, 900';
    }
    else
    {
        $fontList .= '|Lora:100, 200, 300, 400, 500, 600, 700, 800, 900';
        $fonts_missing++;
    }

    $font3 = dm_get_option('font-3');
    if($font3 != 'none') {
        $fontList .= '|'.str_replace(' ', '+', $font3) . ':100, 200, 300, 400, 500, 600, 700, 800, 900';
    }
    else
    {
        $fonts_missing++;
    }

    if($fonts_missing < 3)
    {
        wp_enqueue_style('google_fonts', 'https://fonts.googleapis.com/css?family='.$fontList);
    }


    // All the theme styles, compressed into one single file
    wp_enqueue_style('main_css', get_template_directory_uri() .'/style.css');
}

//add_action('admin_enqueue_scripts', 'dm_admin_styles');

function dm_admin_styles() {
    wp_enqueue_style('admin_css', get_template_directory_uri() .'/admin_style.css');
}

function load_custom_wp_admin_style() {
    wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin-style.css', false );
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );



add_action( 'admin_enqueue_scripts', 'dm_load_admin_scripts' );
function dm_load_admin_scripts()
{
    wp_enqueue_style('hover_css_admin', get_template_directory_uri() .'/less/components/hover.css');
	wp_enqueue_style('loaders_css_admin', get_template_directory_uri() .'/less/components/loaders.css');
	wp_enqueue_style('transitions_css_admin', get_template_directory_uri() .'/less/transitions.css');
}



if (!function_exists('remove_wp_open_sans')) :
    function remove_wp_open_sans() {
        wp_deregister_style( 'open-sans' );
        wp_register_style( 'open-sans', false );
    }
    add_action('wp_enqueue_scripts', 'remove_wp_open_sans');
// Uncomment below to remove from admin
// add_action('admin_enqueue_scripts', 'remove_wp_open_sans');
endif;