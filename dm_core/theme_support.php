<?php
add_theme_support('post-thumbnails', array('client'));
$my_post_formats = array( 'quote', 'chat', 'audio', 'gallery', 'image' );
add_theme_support( 'post-formats', $my_post_formats );
add_theme_support('menus');
add_theme_support( 'custom-logo', array(
    //'height'      => 400,
    //'width'       => 400,
    'flex-height' => true,
    'flex-width' => true,
) );

add_theme_support('jetpack-portfolio');
update_option('jetpack-portfolio', '0');


add_theme_support('jetpack-testimonial');
update_option('jetpack-testimonial', '1');