<?php


add_action('widgets_init', 'dm_create_widget_areas');
function dm_create_widget_areas()
{
    register_sidebar(array(
        'name'=> __('Footer Sidebar', 'dm'),
        'id'=> 'footer-sidebar',
        'description'=>__('Displays on the footer of the site', 'dm'),
        'before_widget'=> '<div class="widget">',
        'after_widget'=>'</div>',
        'before_widget' => '<div class="col-sm-3">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name'=> __('Blog Sidebar', 'dm'),
        'id'=> 'blog-sidebar',
        'description'=>__('Displays on the side of the blog', 'dm'),
        'before_widget'=> '<div class="widget">',
        'after_widget'=>'</div>',
        'before_widget' => '<div class="">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name'=> __('Left Sidebar', 'dm'),
        'id'=> 'left-sidebar',
        'description'=>__('Displays on the left side', 'dm'),
        'before_widget'=> '<div class="widget">',
        'after_widget'=>'</div>',
        'before_widget' => '<div class="">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name'=> __('Right Sidebar', 'dm'),
        'id'=> 'right-sidebar',
        'description'=>__('Displays on the right side', 'dm'),
        'before_widget'=> '<div class="widget">',
        'after_widget'=>'</div>',
        'before_widget' => '<div class="">',
        'after_widget' => '</div>',
    ));

    /*
    register_sidebar(array(
        'name'=> __('Portfolio Left Sidebar', 'dm'),
        'id'=> 'portfolio-left-sidebar',
        'description'=>__('Displays on the left side of the portfolio page', 'dm'),
        'before_widget'=> '<div class="widget">',
        'after_widget'=>'</div>',
        'before_widget' => '<div class="">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name'=> __('Portfolio Right Sidebar', 'dm'),
        'id'=> 'portfolio-right-sidebar',
        'description'=>__('Displays on the right side of the portfolio page', 'dm'),
        'before_widget'=> '<div class="widget">',
        'after_widget'=>'</div>',
        'before_widget' => '<div class="">',
        'after_widget' => '</div>',
    ));
    */
}

function dm_create_widget_area($name, $id, $description ) {
    register_sidebar(array(
        'name'=> _($name),
        'id'=> $id,
        'description'=>_($description),
        'before_widget' => '<div class="col-sm-3 widget">',
        'after_widget' => '</div>',
    ));
}