<?php
add_filter( 'vc_load_default_templates', 'dm_vc_template_section_clients_1' ); // Hook in

function dm_vc_template_section_clients_1( $data ) {
    $template               = array();
    $template['name']       = __('Section Clients 1', '' ); // Assign name for your custom template
//$template['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/custom_template_thumbnail.jpg', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px.
//$template['custom_class'] = 'custom_template_for_vc_custom_template'; // CSS class name
    $template['content']    =
        <<<CONTENT
    [vc_row css=".vc_custom_1460048596714{padding-bottom: 30px !important;background-image: url(http://localhost/wp-content/uploads/2016/04/forest_wide.jpg?id=137) !important;}"][vc_column][vc_row_inner el_class="boxed-layout" css=".vc_custom_1460048615251{margin-top: -20px !important;margin-bottom: 20px !important;}"][vc_column_inner][dm_title color="Gray Dark" text="WE'VE WORKED WITH:"][/vc_column_inner][/vc_row_inner][vc_row_inner el_class="boxed-layout"][vc_column_inner width="1/6"][vc_single_image image="143" img_size="full" alignment="center"][/vc_column_inner][vc_column_inner width="1/6"][vc_single_image image="142" img_size="full" alignment="center"][/vc_column_inner][vc_column_inner width="1/6"][vc_single_image image="141" img_size="full" alignment="center"][/vc_column_inner][vc_column_inner width="1/6"][vc_single_image image="140" img_size="full" alignment="center"][/vc_column_inner][vc_column_inner width="1/6"][vc_single_image image="139" img_size="full" alignment="center"][/vc_column_inner][vc_column_inner width="1/6"][vc_single_image image="138" img_size="full" alignment="center"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]
CONTENT;

    array_unshift( $data, $template );
    return $data;
}