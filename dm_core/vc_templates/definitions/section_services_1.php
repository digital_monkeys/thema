<?php
add_filter( 'vc_load_default_templates', 'dm_vc_template_section_services_1' ); // Hook in

function dm_vc_template_section_services_1( $data ) {
$template               = array();
$template['name']       = __('Section Services 1', 'dm'); // Assign name for your custom template
//$template['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/custom_template_thumbnail.jpg', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px.
//$template['custom_class'] = 'custom_template_for_vc_custom_template'; // CSS class name
$template['content']    =
    <<<CONTENT
[vc_row equal_height="yes" el_class="boxed-layout sectionPersonalizedService_1"][vc_column width="1/2" el_class="bg-color2Lighter" css=".vc_custom_1459948348511{padding-top: 20px !important;}"][vc_row_inner el_class="serviceItem_1" css=".vc_custom_1459948100371{margin-right: 5% !important;}"][vc_column_inner el_class="icon" width="1/6"][vc_icon icon_fontawesome="fa fa-cog" color="custom" align="center" css_animation="left-to-right" css=".vc_custom_1459929697425{margin-top: 0.5em !important;}"][/vc_column_inner][vc_column_inner width="5/6"][vc_column_text css_animation="right-to-left"]
<h3>Aliquam tincidunt mauris eu</h3>
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner el_class="serviceItem_1" css=".vc_custom_1459929577632{margin-right: 5% !important;}"][vc_column_inner el_class="icon" width="1/6"][vc_icon icon_fontawesome="fa fa-cog" color="custom" align="center" css_animation="left-to-right" css=".vc_custom_1459929688246{margin-top: 0.5em !important;}"][/vc_column_inner][vc_column_inner width="5/6"][vc_column_text css_animation="right-to-left"]
<h3>Aliquam tincidunt mauris eu</h3>
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner el_class="serviceItem_1" css=".vc_custom_1459929594012{margin-right: 5% !important;}"][vc_column_inner el_class="icon" width="1/6"][vc_icon icon_fontawesome="fa fa-cog" color="custom" align="center" css_animation="left-to-right" css=".vc_custom_1459929706077{margin-top: 0.5em !important;}"][/vc_column_inner][vc_column_inner width="5/6"][vc_column_text css_animation="right-to-left"]
<h3>Aliquam tincidunt mauris eu</h3>
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner el_class="serviceItem_1" css=".vc_custom_1459929603063{margin-right: 5% !important;}"][vc_column_inner el_class="icon" width="1/6"][vc_icon icon_fontawesome="fa fa-cog" color="custom" align="center" css_animation="left-to-right" css=".vc_custom_1459929714972{margin-top: 0.5em !important;}"][/vc_column_inner][vc_column_inner width="5/6"][vc_column_text css_animation="right-to-left"]
<h3>Aliquam tincidunt mauris eu</h3>
Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/2" css=".vc_custom_1459947725638{margin-left: 40px !important;}" el_class="bg-color2"][vc_column_text css_animation="appear" el_class="title font-colorWhite" css=".vc_custom_1459948737341{margin-top: 50px !important;}"]
<h1 style="text-align: center;">Want</h1>
<h1 style="text-align: center;">Personalized</h1>
<h1 style="text-align: center;">Service ?</h1>
[/vc_column_text][vc_column_text css_animation="bottom-to-top" el_class="description font-colorWhite" css=".vc_custom_1459948744289{margin-top: 40px !important;margin-bottom: 40px !important;}"]
<p style="text-align: justify;">Ut convallis, sem sit amet interdum consectetuer, odio augue aliquam leo, nec dapibus tortor nibh sed augue. Integer eu magna sit amet metus fermentum posuere. Morbi sit amet nulla sed dolor elementum imperdiet. Quisque fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque adipiscing eros ut libero. Ut condimentum mi vel tellus. Suspendisse laoreet. Fusce ut est sed dolor gravida convallis.</p>
[/vc_column_text][btn_1 color="2" label="Get in touch !" icon="fa fa-bolt"][/vc_column][/vc_row]
CONTENT;

array_unshift( $data, $template );
return $data;
}