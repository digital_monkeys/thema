<?php
add_filter( 'vc_load_default_templates', 'dm_vc_template_section_testimonials_1' ); // Hook in

function dm_vc_template_section_testimonials_1( $data ) {
    $template               = array();
    $template['name']       = __('Section Testimonials 1', 'dm'); // Assign name for your custom template
//$template['image_path'] = preg_replace( '/\s/', '%20', plugins_url( 'images/custom_template_thumbnail.jpg', __FILE__ ) ); // Always use preg replace to be sure that "space" will not break logic. Thumbnail should have this dimensions: 114x154px.
//$template['custom_class'] = 'custom_template_for_vc_custom_template'; // CSS class name
    $template['content']    =
        <<<CONTENT
[vc_row css=".vc_custom_1460093649562{padding-bottom: 35px !important;background-image: url(http://localhost/wp-content/uploads/2016/04/fake_brick.png?id=169) !important;}"][vc_column][vc_row_inner css=".vc_custom_1460094356137{margin-top: -30px !important;}"][vc_column_inner][dm_title color="White" text="WHAT OUR CLIENTS THINK"][/vc_column_inner][/vc_row_inner][vc_row_inner el_class="boxed-layout"][vc_column_inner width="1/4"][dm_testimonial color_image_stroke="Gray Dark" font_title="Theme font 2" color_title="White" font_body="Theme font 2" color_body="White" title="Best WordPress theme." body="Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis." image="173" image_id="173"][/vc_column_inner][vc_column_inner width="1/4"][dm_testimonial color_image_stroke="Gray Dark" font_title="Theme font 2" color_title="White" font_body="Theme font 2" color_body="White" title="Couldn't be better." body="Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis." image="173" image_id="175"][/vc_column_inner][vc_column_inner width="1/4"][dm_testimonial color_image_stroke="Gray Dark" font_title="Theme font 2" color_title="White" font_body="Theme font 2" color_body="White" title="These guys delivered." body="Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis." image="173" image_id="176"][/vc_column_inner][vc_column_inner width="1/4"][dm_testimonial color_image_stroke="Gray Dark" font_title="Theme font 2" color_title="White" font_body="Theme font 2" color_body="White" title="It's awesome." body="Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis." image="173" image_id="174"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]
CONTENT;

    array_unshift( $data, $template );
    return $data;
}