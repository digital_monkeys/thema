            <footer class="row" style="background-image: url('<?php echo get_template_directory_uri(); ?>/graphics/patterns/zwartevilt.png')">
                <div class="boxed-layout">
                    <?php
                    if(is_active_sidebar('footer-sidebar')){
                        dynamic_sidebar('footer-sidebar');
                    }

                    ?>
                </div>
            </footer>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>