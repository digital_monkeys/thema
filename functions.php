<?php
/**
 * The functions.php file is used solely for including/requiring files. There are way too many things to code into one
 * file, so we don't put anything here, not even a single function or a constant
 */

require_once 'dm_core/constants.php'; // DOCS ADDED
require_once 'dm_core/script_manager.php'; // DOCS ADDED
require_once 'dm_core/tgm/manager.php'; // DOCS ADDED
require_once 'dm_core/create_required_files.php'; // DOCS ADDED
require_once 'dm_core/walkers/nav_menu.php';
require_once 'dm_core/media_library_manipulator.php'; // DOCS ADDED
include_once 'dm_core/import_export/importer.php'; // DOCS ADDED
include_once 'dm_core/import_export/exporter.php'; // DOCS ADDED
include_once 'dm_core/import_export/deleter.php'; // DOCS ADDED
require_once 'widgets/manager.php'; // DOCS ADDED
include_once 'dm_core/import_export/ajax_import_manager.php';
require_once 'dm_core/color_schemes.php'; // DOCS ADDED
require_once 'dm_core/less_manager/manager.php'; // DOCS ADDED
require_once 'dm_core/customizer/manager.php'; // DOCS NOT NEEDED
require_once 'shortcodes/wordpress/manager.php'; // DOCS ADDED
require_once 'shortcodes/visual_composer/manager.php'; // DOCS ADDED
require_once 'dm_core/vc_templates/manager.php'; // DOCS ADDED
require_once 'dm_core/theme_menus.php'; // DOCS NOT NEEDED
require_once 'dm_core/theme_scripts.php'; // DOCS NOT NEEDED
require_once 'dm_core/theme_support.php'; // DOCS NOT NEEDED
require_once 'dm_core/theme_styles.php'; // DOCS ADDED
require_once 'dm_core/theme_widgets.php'; // DOCS NOT NEEDED
require_once 'dm_core/custom_functions.php'; // DOCS PARTIALLY ADDED - NOT NEEDED MORE


// !!!!!!! For testing purposes only, comment out in production !!!!!!!
add_action('init', 'compile_less');

add_action('customize_save_after', 'compile_less');

