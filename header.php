<!DOCTYPE html>
<html>
<head>
    <title> <?php wp_title(); ?> </title>
    <?php wp_head(); ?>
    <?php
        $dm_headerBgColor = dm_get_option('header-background-color');
    ?>

    <style>
        body {
            font-family: "<?php echo dm_get_option('font-1');?>"
        }
        .font-1 {
            font-family: "<?php echo dm_get_option('font-1');?>"
        }
        .font-2 {
            font-family: "<?php echo dm_get_option('font-2');?>"
        }
        .font-3 {
            font-family: "<?php echo dm_get_option('font-3');?>"
        }
    </style>

    <?php
        if(is_customize_preview())
        {

        }

    ?>

    <noscript>
        <style>
            .main_preloader {
                display: none !important;
            }
        </style>
    </noscript
</head>
<body <?php body_class(); ?> >

    <div class="container-fluid">
        <aside class="main_preloader">
            <div class="loader">
                <div class="loader-inner ball-grid-beat">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </aside>

        <?php
            $dm_headerType = dm_get_header_type();
            $dm_headerIsSticky = dm_get_option('header-is-sticky');
            if(strlen($dm_headerIsSticky) > 0) {
                $dm_headerIsSticky = 'true';
            } else {
                $dm_headerIsSticky = 'false';
            }
            $headerData = "data-is-sticky='$dm_headerIsSticky'";
            $headerData .= " data-background-color='".dm_get_color_index($dm_headerBgColor)."'";
            $headerData .= " data-mobile-breakpoint='".dm_get_option('header-mobile-breakpoint')."'";
            $headerData .= " data-mobile-direction='".dm_get_option('header-mobile-direction')."'";
            $headerData .= " data-mobile-type='".dm_get_option('header-mobile-type')."'";
            $headerData .= " data-mobile-size='".dm_get_option('header-mobile-size')."'";

        ?>

        <header class="<?php echo " header-skin-$dm_headerType"; ?>" <?php echo $headerData; ?>>
            <?php
                get_template_part('partials/header', "skin$dm_headerType");
            ?>
        </header>
        <div id="page-contents">


