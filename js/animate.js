jQuery(document).ready(function($) {

    // First we register the component that handles the DOM elements' transitions
    registerComponent(
        {
            'selector': '.dm-animate',
            'initFunction': animate_init,
            'resetFunction' : animate_reset
        }
    );
    function animate_init(component) {

        // We read the necessary attribute data to use in the transition
        var attrTransitionType = component.attr('data-transition-type');
        var attrTransitionDelay= component.attr('data-transition-delay');
        var attrTransitionDuration = component.attr('data-transition-duration');
        var attrTransitionEasing = component.attr('data-transition-easing');
        var attrTransitionAmplificationMovement = component.attr('data-transition-amplification-movement');
        var attrTransitionAmplificationRotation = component.attr('data-transition-amplification-rotation');

        // We save the element's original CSS position
        var position = component.css('position');
        var amplificationMovement = parseInt(attrTransitionAmplificationMovement.substr(1));
        var amplificationRotation = parseInt(attrTransitionAmplificationRotation.substr(1));

        var initialTop = 0;
        var initialBottom = 0;
        var initialLeft = 0;
        var initialRight = 0;

        // If the element's position is other than 'static', we read it's 'top', 'bottom', 'left' and 'right' values
        if(position != 'static')
        {
            if(!isNaN(component.css('top'))) {
                initialTop = parseInt(component.css('top'));
            }

            if(!isNaN(component.css('bottom'))) {
                initialBottom = parseInt(component.css('bottom'));
            }

            if(!isNaN(component.css('left'))) {
                initialLeft = parseInt(component.css('left'));
            }

            if(!isNaN(component.css('right'))) {
                initialRight = parseInt(component.css('right'));
            }
        }

        // Even though the element's opacity should already be 0, this is for situations
        // when the reset function hasn't been properly executed, or when the element has been added after page load (for example in Visual Composer)
        component.css('opacity', '0');

        // Most transitions require that element's to be other than 'static', so for those elements, we set it to be 'relative' temporarily
        if(position == 'static') {
            component.css('position', 'relative');
        }

        // Based on what transition type has been chosen for the element, the proper transition effect is applied
        switch(attrTransitionType.toLowerCase())
        {
            case 'fade-in':
                // For the 'fade-in' effect, we apply the easing on the opacity transition.
                // For all the others, the opacity is animated without easing. That is why we have a separate tween call for it.
                TweenMax.to(component, attrTransitionDuration, {opacity: 1, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing), onComplete: onTransitionEnd});
                break;
            case 'slide-up':
                TweenMax.to(component, 0, {top: initialTop + 30 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {top: initialTop, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'slide-down':
                TweenMax.to(component, 0, {top: initialTop - 30 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {top: initialTop, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'slide-left':
                TweenMax.to(component, 0, {left: initialLeft + 30 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {left: initialLeft, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'slide-right':
                TweenMax.to(component, 0, {left: initialLeft - 30 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {left: initialLeft, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'scale-in':
                TweenMax.to(component, 0, {scaleX: 0, scaleY: 0});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'scale-slide-up':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, top: initialTop + 130 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, top: initialTop,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'scale-slide-down':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, top: initialTop - 130 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, top: initialTop, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'scale-slide-left':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, left: initialLeft + 130 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, left: initialLeft, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'scale-slide-right':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, left: initialLeft + 130 * amplificationMovement});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, left: initialLeft, opacity: 1, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'rotate-in':
                TweenMax.to(component, 0, {scaleX: 0, scaleY: 0, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, rotation: 0, delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing), onComplete: onTransitionEnd});
                break;
            case 'roll-up':
                TweenMax.to(component, 0, {scaleX:1, scaleY: 1, top: initialTop + 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, top: initialTop, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'roll-down':
                TweenMax.to(component, 0, {scaleX:1, scaleY: 1, top: initialTop - 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, top: initialTop, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'roll-left':
                TweenMax.to(component, 0, {scaleX:1, scaleY: 1, left: initialLeft + 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, left: initialLeft, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'roll-right':
                TweenMax.to(component, 0, {scaleX:1, scaleY: 1, left: initialLeft - 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, left: initialLeft, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;

            case 'roll-scale-up':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, top: initialTop + 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, top: initialTop, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'roll-scale-down':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, top: initialTop - 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, top: initialTop, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'roll-scale-left':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, left: initialLeft + 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, left: initialLeft, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
            case 'roll-scale-right':
                TweenMax.to(component, 0, {scaleX:0, scaleY: 0, left: initialLeft - 130 * amplificationMovement, rotation: 360 * amplificationRotation});
                TweenMax.to(component, attrTransitionDuration, {scaleX: 1, scaleY: 1, left: initialLeft, rotation: 0,  delay: attrTransitionDelay, ease: getEasing(attrTransitionEasing)});
                break;
        }

        if(attrTransitionType.toLowerCase() != 'fade-in') {
            TweenMax.to(component, attrTransitionDuration, {
                opacity: 1,
                delay: attrTransitionDelay,
                onComplete: onTransitionEnd
            });
        }
        // After the transition ends, if the element's original position was 'static', we reset it to 'static'
        function onTransitionEnd()
        {
            if(position == 'static') {
                component.css('position', 'static');
            }
        }



        /*
         'bounceIn',
         'bounceInDown',
         'bounceInLeft',
         'bounceInRight',
         'bounceInUp',
         'fadeIn',
         'fadeInDown',
         'fadeInDownBig',
         'fadeInLeft',
         'fadeInLeftBig',
         'fadeInRight',
         'fadeInRightBig',
         'fadeInUp',
         'fadeInUpBig',
         'flip',
         'flipInX',
         'flipInY',
         'lightSpeedIn',
         'rotateIn',
         'rotateInDownLeft',
         'rotateInDownRight',
         'rotateInUpLeft',
         'rotateInUpRight',
         'slideInUp',
         'slideInDown',
         'slideInLeft',
         'slideInRight',
         'rollIn',
         'zoomIn',
         'zoomInDown',
         'zoomInLeft',
         'zoomInRight',
         'zoomInUp',
         'jello',
         'bounce',
         'flash',
         'pulse',
         'rubberBand',
         'shake',
         'swing',
         'tada',
         'wobble',

         */
    }

    // All the elements that are going to be animated must be hidden at page load.
    // This way, if JavaScript isn't enabled, the page still displays ok, but without the transitions
    function animate_reset(component) {
        component.css('opacity', '0');
    }

    // This function is just a parser for the easing name, to return the correct property
    // (otherwise it would have involved using a Factory pattern, and the use case is too small)
    window.getEasing = function(easingName) {
        switch(easingName.toLowerCase()) {
            case 'power0.easein': return Power0.easeIn; break;
            case 'power0.easeout': return Power0.easeOut; break;
            case 'power0.easeinout': return Power0.easeInOut; break;

            case 'power1.easein': return Power1.easeIn; break;
            case 'power1.easeout': return Power1.easeOut; break;
            case 'power1.easeinout': return Power1.easeInOut; break;

            case 'power2.easein': return Power2.easeIn; break;
            case 'power2.easeout': return Power2.easeOut; break;
            case 'power2.easeinout': return Power2.easeInOut; break;

            case 'power3.easein': return Power3.easeIn; break;
            case 'power3.easeout': return Power3.easeOut; break;
            case 'power3.easeinout': return Power3.easeInOut; break;

            case 'power4.easein': return Power4.easeIn; break;
            case 'power4.easeout': return Power4.easeOut; break;
            case 'power4.easeinout': return Power4.easeInOut; break;

            case 'back.easein': return Back.easeIn; break;
            case 'back.easeout': return Back.easeOut; break;
            case 'back.easeinout': return Back.easeInOut; break;

            case 'elastic.easein': return Elastic.easeIn; break;
            case 'elastic.easeout': return Elastic.easeOut; break;
            case 'elastic.easeinout': return Elastic.easeInOut; break;

            case 'bounce.easein': return Bounce.easeIn; break;
            case 'bounce.easeout': return Bounce.easeOut; break;
            case 'bounce.easeinout': return Bounce.easeInOut; break;

            case 'rough.easein': return Rough.easeIn; break;
            case 'rough.easeout': return Rough.easeOut; break;
            case 'rough.easeinout': return Rough.easeInOut; break;

            case 'slowmo': return SlowMo.ease.config(0.3, 0.7, false); break;
            case 'stepped': return SteppedEase.config(12); break;

            case 'circ.easein': return Circ.easeIn; break;
            case 'circ.easeout': return Circ.easeOut; break;
            case 'circ.easeinout': return Circ.easeInOut; break;

            case 'expo.easein': return Expo.easeIn; break;
            case 'expo.easeout': return Expo.easeOut; break;
            case 'expo.easeinout': return Expo.easeInOut; break;

            case 'sine.easein': return Sine.easeIn; break;
            case 'sine.easeout': return Sine.easeOut; break;
            case 'sine.easeinout': return Sine.easeInOut; break;
        }
    };
});
