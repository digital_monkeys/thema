jQuery(
    function($)
    {
        // Create the container array for all of the components
        window.dm_arrComponents = [];

        // Create the container array for all of the masonry grids
        window.dm_arrMasonryGrids = [];

        // The function that allows components to be enqueued (registered).
        // This way, we have a unified way to add and manage visual components, with various effects and transitions
        window.registerComponent = function(objComponentData) {
            //console.log('registerComponent() objComponentData = ');
            //console.log(objComponentData);
            // Adding the default values for parameters that are optional
            if(objComponentData.initOn == null)
            {
                objComponentData.initOn = 'scroll';
            }

            if(objComponentData.initOn.indexOf('resize') != -1 && objComponentData.initOn.indexOf('load') == -1)
            {
                objComponentData.initOn += ' load';
            }

            if(objComponentData.initOnce == null)
            {
                objComponentData.initOnce = true;
            }

            if(objComponentData.initFunction == null)
            {
                objComponentData.initFunction = function(){};
            }

            if(objComponentData.resetFunction == null)
            {
                objComponentData.resetFunction = function(){};
            }


            // The only parameter that isn't optional is 'selector', so without it, we don't add the component to
            // the collection
            if(objComponentData.selector != null)
            {
                window.dm_arrComponents.push(objComponentData);
            }
        };

        // Wrapper function for the components that should be initialized on the 'scroll' event
        window.updateComponentsOnScroll = function() {
            updateComponents('scroll');
        };

        // Wrapper function for the components that should be initialized on the 'load' event
        window.updateComponentsOnLoad = function() {
            updateComponents('load');
        };

        // Wrapper function for the components that should be initialized on the 'resize' event
        window.updateComponentsOnResize = function() {
            updateComponents('resize');
        };

        // This function is called every time a 'load', 'scroll' or 'resize' event is fired.
        // And in some cases, it is called even more often, based on what's needed
        window.updateComponents = function(event) {
            if(window.pageIsClosing == null || window.pageIsClosing == false) {
                // We go through all the components in the collection and apply the 'init' function if needed
                for (var i = 0; i < window.dm_arrComponents.length; i++) {
                    // We store the current component data from the collection
                    var crtComponent = window.dm_arrComponents[i];

                    // We extract the meaningful part of the selector to use it as an attribute name
                    var attrName = 'data-' + crtComponent.selector.substr(1) + '-initiated';

                    // Here we poll the DOM for each component and init the component if it's required
                    $(crtComponent.selector).each(
                        function () {
                            // Based on the component's name, we check if has already been initialized. If it has, we
                            // leave it alone and move on
                            // If it has been initialized at least once, but it has been set to be instantiated multiple
                            // times, we proceed
                            if (crtComponent.initOnce == false || $(this).attr(attrName) == null || $(this).attr(attrName) == 'false') {
                                // Just a null-check for the 'initOn' property, even though it should always be covered
                                // by the default
                                if (crtComponent.initOn != null) {
                                    if (crtComponent.initOn.indexOf('scroll') != -1) {
                                        // If the event that the component is linked to is 'scroll',
                                        // we need to check if the DOM element is actually in the viewport
                                        if (event == 'scroll') {
                                            if ($(this).visible()) {
                                                crtComponent.initFunction($(this), event);
                                                $(this).attr(attrName, 'true');
                                            }
                                        }
                                    }
                                    if (crtComponent.initOn.indexOf('load') != -1) {
                                        if (event == 'load') {
                                            crtComponent.initFunction($(this), event);
                                            $(this).attr(attrName, 'true');
                                        }
                                    }
                                    if (crtComponent.initOn.indexOf('resize') != -1) {
                                        if (event == 'resize') {
                                            crtComponent.initFunction($(this), event);
                                            $(this).attr(attrName, 'true');
                                        }
                                    }
                                }
                            }
                        }
                    );
                }
            }
        };

        // Apply the 'reset' function for all the components that have been registered
        window.resetComponents = function()
        {
            for(var i = 0; i < window.dm_arrComponents.length; i++)
            {
                var crtComponent = window.dm_arrComponents[i];

                $(crtComponent.selector).each(
                    function(){
                        crtComponent.resetFunction($(this));
                    }
                );
            }
        };

        // On page load, call the 'reset' function for all the visual components, then check if any one of them needs to be initialized
        $(document).ready(resetComponents);
        $(document).ready(updateComponentsOnLoad);

        // Also on page load, update the masonry grids
        $(document).ready(function(){
           // window.updateMasonryGrids();
        });

        $(document).ready(function(){
            if(window.isVisualComposer()) {
                setTimeout(window.updateMasonryGridsVC, 0);
            } else {
                window.updateMasonryGrids();
            }
        });

        window.updateMasonryGridsVC = function()
        {
            $('.grid-item').each(function(){
                $(this).removeClass('vc_element-container');
                $(this).removeClass('ui-sortable');
                var gridItemClasses = $(this).attr('class');

                $(this).attr('class', 'grid-item col-xs-12 vc_element-container ui-sortable');

                $(this).find('.vc_element').each(function(){
                    $(this).addClass(gridItemClasses);
                });
            });

            /*
            $('.masonry .grid-item').masonry({
                // set itemSelector so .grid-sizer is not used in layout
                itemSelector: '.grid-item',
                // use element for option
                columnWidth: '.col-lg-3',
                percentPosition: true
            });
            */
        };

        window.updateMasonryGrids = function()
        {
            $('.masonry').masonry({
                // set itemSelector so .grid-sizer is not used in layout
                itemSelector: '.grid-item',
                // use element for option
                columnWidth: '.grid-item',
                percentPosition: true
            });
        };

        // Return whether the page is loaded in Visual Composer's Front End editor
        window.isVisualComposer = function()
        {
            if (window.location != window.parent.location && window.parent.location.href.indexOf('vc_action') != -1) {
                return true;
            } else {
                return false;
            }
        };

        // On page load, we trigger the preparation of the DOM listener
        $(document).ready(prepareDOMListener);

        function prepareDOMListener()
        {
            // We delay the addition of the DOM listener by 3 seconds, in order to give the browser time to do other things
            // (like external dependencies, plugins etc. )
            setTimeout(addDOMListener, 3000);
        }

        /*
        window.updateMasonryGrids = function()
        {
            for(var i = 0; i < window.dm_arrMasonryGrids.length; i++)
            {
                window.dm_arrMasonryGrids[i].
            }
        };
        */


        function addDOMListener()
        {
            // This function listens for the 'animationstart' event, which is fired whenever
            // a CSS animation (the ones based on keyframes) is finished
            var elementAddedListener = function(event){
                if (event.animationName == "insertElement") {

                    updateComponentsOnScroll();
                    updateMasonryGrids();
                }
            };

            // We add the actual listener for the 'animationstart' event, with the necessary vendor prefixes
            document.addEventListener("animationstart", elementAddedListener, false); // standard + firefox
            document.addEventListener("MSAnimationStart", elementAddedListener, false); // IE
            document.addEventListener("webkitAnimationStart", elementAddedListener, false); // Chrome + Safari
        }
    }
);