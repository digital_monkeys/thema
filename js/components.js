jQuery(document).ready(function($) {


    registerComponent(
        {
            'selector': '.dm-growing-number',
            'initFunction': growing_number_init,
            'resetFunction' : growing_number_reset
        }
    );
    function growing_number_init(component) {
        var element = component.find('.dm-number');
        var textAdded = '';
        var attrEndValue = component.attr('data-end-value');
        if(attrEndValue.indexOf('%') != -1)
        {
            textAdded = '%';
        }

        var endValue = parseInt(attrEndValue);

        var counter = {var: 0};
        TweenMax.to(counter, 3, {
            var: endValue,
            onUpdate: function () {
                element.text(Math.ceil(counter.var) + textAdded);
            },
            ease: Quint.easeOut
        });
    }
    function growing_number_reset(component) {
        var element = component.find('.dm-number');
        element.text('0');
    }


    registerComponent(
        {
            'selector': '.dm-progress-bar',
            'initFunction': progress_bar_init,
            'resetFunction' : progress_bar_reset
        }
    );
    function progress_bar_init(component) {
        var element = component.find('.dm-current-progress');
        var textAdded = '';
        var attrEndValue = component.attr('data-end-value');
        if(attrEndValue.indexOf('%') != -1)
        {
            textAdded = '%';
        }
        var endValue = parseInt(attrEndValue);

        TweenMax.to(element, Math.random() * 2 + 3, {width: (endValue + "%"), ease: Elastic.easeOut})
    }
    function progress_bar_reset(component) {
        component.find('.dm-current-progress').css('width', '0px');
    }



    registerComponent(
        {
            'selector': '.dm-portfolio-item',
            'initFunction': portfolio_item_init,
            'initOn' : 'resize',
            'initOnce' : false
        }
    );
    function portfolio_item_init(component) {
        console.log('portfolio_item_init()');
        var separator = component.find('.dm-separator');
        var image = component.find('.dm-image').first();
        separator.height(image.height());
    }


    registerComponent(
        {
            'selector': '.dm-circle',
            'initFunction': progress_circle_init,
            'resetFunction' : progress_circle_reset
        }
    );
    function progress_circle_init(component)
    {
        var textAdded = '';
        var attrEndValue = component.attr('data-end-value');
        var attrTotalValue = component.attr('data-total-value');
        if(attrEndValue.indexOf('%') != -1)
        {
            textAdded = '%';
        }
        var endValue = Math.min(1, parseInt(attrEndValue) / parseInt(attrTotalValue));

        var updateCount = 0;
        var counterPercent = {crtValue: 0};
        TweenMax.to(counterPercent, endValue * 2, {
            crtValue: endValue,
            onUpdate: function () {
                updateCount++;
                if(updateCount % 3 == 0) {
                    component.circleProgress({
                        value: counterPercent.crtValue,
                        size: 200,
                        thickness: '4',
                        fill: {
                            gradient: ["#f1c40f", "#e74c3c"],
                            color: '#2ecc71'
                        }
                    });
                }
            }
        });

        var counterProgress = {crtValue: 0};
        TweenMax.to(counterProgress, 4 , {
            crtValue: parseInt(attrEndValue),
            onUpdate: function () {
                component.find('.dm_number').text(Math.round(counterProgress.crtValue).toString());
            },
            delay:.8,
            ease: Quint.easeOut
        });
    }
    function progress_circle_reset(component) {
        component.circleProgress({
            value: 0,
            size: 200,
            thickness: '4',
            fill: {
                //gradient: ["#f1c40f", "#e74c3c"]
                color: '#2ecc71'
            }
        });

        component.find('.dm_number').text('0');
    }
});



