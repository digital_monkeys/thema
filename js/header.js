jQuery(function($) {
    // The function that manages everything related to the sticky header
    window.dm_initHeader = function () {
        var pageScroll = $(document).scrollTop();
        var headerIsSticky = false;
        var header = $('header');
        var headerHeight = $('header').height();
        var headerMobile = header.find('.dm-mobile-menu');
        var headerMobileInner = headerMobile.find('.dm-mobile-menu-inner');
        var headerFooterOverlapAmount = 0;
        var headerShouldBeSticky = header.attr('data-is-sticky');
        var pageHeight = $(document).height();
        var footerHeight = $('footer').height();
        var pageContents = $('#page-contents');
        var headerTop = 0;

        var crtURL = window.location.href;

        var displayHeaderPlaceholder = true;

        if(isVisualComposer()) {
            header.css('display', 'none');
        } else {
            // We check if there is a hero component present.
            // If we have one, then we check to see if it has been set to display full-screen and act accordingly
            if($('.dm-hero').length > 0)
            {

                var hero = $('.dm-hero');
                var attrIsFullScreen = hero.attr('data-is-fullscreen');
                var attrHeaderStyle = hero.attr('data-header-style');
                // If the hero is set to be displayed full screen, we hide the header's placeholder and add the necessary classes to the header
                if(attrIsFullScreen == 'true')
                {

                    //$('.header-placeholder').css('display', 'none');
                    displayHeaderPlaceholder = false;
                    header.attr('data-is-transparent', 'true');
                    header.attr('data-transparent-style', attrHeaderStyle);

                    if(attrHeaderStyle == 'light')
                    {
                        header.addClass('header-light-contents');
                    }
                    else if(attrHeaderStyle == 'dark')
                    {
                        header.addClass('header-dark-contents');
                    }
                }
            }


            if(header.attr('data-is-sticky') == 'true') {
                header.css('position', 'fixed');
            } else {
                header.css('position', 'absolute');
            }

            updateStickyHeader();
            $(window).scroll(updateStickyHeader);
            $(window).resize(updateStickyHeader);
        }


        if(displayHeaderPlaceholder)
        {
            $('<div class="header-placeholder"></div>').insertAfter('header');
            $('.header-placeholder').css('background-color', 'rgba(0,0,0,0)');
            $('.header-placeholder').css('opacity', '0');
            $('.header-placeholder').css('height', header.height()+'px');
        }

        function updateStickyHeader() {
            //console.log('checkHeaderOnUpdate()');
            pageScroll = $(document).scrollTop();

            $('.header-placeholder').css('height', header.height()+'px');

            var adminBar = $('#wpadminbar');
            var adminBarHeight = parseFloat(adminBar.height());

            if (pageScroll > 1) {
                footerHeight = $('footer').height();
                pageHeight = $(document).height();
                headerHeight = $('header').height();
                headerFooterOverlapAmount = pageHeight - footerHeight - pageScroll - headerHeight;

                if (adminBar.length > 0 && adminBar.css('position') == 'fixed') {
                    headerFooterOverlapAmount -= adminBarHeight;
                }

                headerFooterOverlapAmount =-(headerFooterOverlapAmount);

                if (headerFooterOverlapAmount > 0) {
                    if (adminBar.length > 0) {
                        if(adminBar.css('position') != 'fixed') {
                            headerTop = -headerFooterOverlapAmount;
                        } else {
                            headerTop = -(headerFooterOverlapAmount - adminBarHeight);
                        }
                    } else {
                        headerTop = -headerFooterOverlapAmount;
                    }
                } else {
                    if (adminBar.length > 0) {
                        if(adminBar.css('position') == 'fixed') {
                            headerTop = adminBarHeight;
                        } else {
                            if(pageScroll < adminBarHeight) {
                                headerTop = adminBarHeight - pageScroll;
                            } else {
                                headerTop = 0;
                            }
                        }

                    } else {
                        headerTop = 0;
                    }
                }

                if(header.attr('data-is-sticky') == 'true') {
                    header.css('top', headerTop + 'px');
                }

                header.attr('data-top', headerTop + 'px');

                //console.log('header top = ' + header.css('top') + ', pageScroll = ' + pageScroll);

                if(headerShouldBeSticky == 'true') {
                    if (headerIsSticky == false) {
                        stickyHeaderOn();
                    }
                }
            } else {

                if (headerShouldBeSticky == 'true') {
                    if (headerIsSticky == true) {
                        stickyHeaderOff();
                    }
                }
            }

            //console.log('updateStickyHeader() headerIsSticky = ' + headerIsSticky);

            if(pageScroll == 0) {
                if(parseFloat(header.css('top')) < adminBarHeight) {
                    header.css('top', adminBarHeight + 'px');
                }
            }

            window.updateMobileHeader();
        }

        function stickyHeaderOn() {
            header.removeClass('header-light-contents');
            header.removeClass('header-dark-contents');

            var classBgColor = 'bg-color' + header.attr('data-background-color');
            header.addClass('header-sticky-mode');
            header.addClass(classBgColor);

            headerIsSticky = true;
        }

        function stickyHeaderOff() {
            if(header.attr('data-transparent-style') == 'light') {
               if(header.attr('data-is-transparent') == 'true')
                {
                    header.addClass('header-light-contents');
                }
            } else if(header.attr('data-transparent-style') == 'dark') {
                if(header.attr('data-is-transparent') == 'true')
                {
                    header.addClass('header-dark-contents');
                }
            }

            var classBgColor = 'bg-color' + header.attr('data-background-color');
            header.removeClass('header-sticky-mode');
            header.removeClass(classBgColor);

            headerIsSticky = false;
        }
    };



    // Init the sticky header
    dm_initHeader();

});
