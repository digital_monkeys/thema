jQuery(function($) {
    var pageScroll = $(document).scrollTop();
    var headerIsSticky = false;
    var header = $('header');
    var headerHeight = $('header').height();
    var mobileMenu = header.find('.dm-mobile-menu');
    var desktopMenu = header.find('.dm-desktop-menu');
    var mobileMenuInner = mobileMenu.find('.dm-mobile-menu-inner');
    var mobileTrigger =  $('.dm-trigger');
    var mobileTriggerContainer =  $('.dm-trigger').parent();
    var headerFooterOverlapAmount = 0;
    var headerShouldBeSticky = header.attr('data-is-sticky');
    var pageHeight = $(document).height();
    var footerHeight = $('footer').height();
    var pageContents = $('#page-contents');
    var headerTop = 0;



    var mobileMenuTransitionTime = 1;
    var mobileMenuTransitionEasing = Power2.easeInOut;

    enableTriggerClickEvents();


    function mobileTriggerClickHandler() {
        var pageIsOffset = pageContents.attr('data-is-offset');
        if(null == pageIsOffset || 'false' == pageIsOffset) {
            window.showMobileMenu();
        } else {
            window.hideMobileMenu();
        }
    }


    window.showMobileMenu = function(doInstantly) {
        //console.log('showMobileMenu()');
        disableTriggerClickEvents();
        setTimeout(enableTriggerClickEvents, mobileMenuTransitionTime * 1000);
        pageContents.attr('data-is-offset', 'true');

        // The direction where the mobile mobile menu will come from
        var attrMobileDirection = header.attr('data-mobile-direction');

        // How the mobile menu affects the page (it can be 'push', 'squeeze' or 'overlay')
        var attrMobileType = header.attr('data-mobile-type');

        var attrMobileSize = header.attr('data-mobile-size');
        if(attrMobileSize == 'default') {
            attrMobileSize = '250px';
        } else if(attrMobileSize == 'full-screen') {
            attrMobileSize = '100%';
        }


        var headerWidth = 0;
        if(attrMobileSize.indexOf('%') != -1) {
            headerWidth = window.innerWidth * parseInt(attrMobileSize) / 100;
        } else if(attrMobileSize.indexOf('px') != -1) {
            headerWidth = parseInt(attrMobileSize);
        }

        mobileMenuInner.css('width', headerWidth + 'px');

        if(attrMobileType == 'squeeze') {
            // If we have less than 400 pixels left for our content, we automatically switch to the 'push' effect
            if(window.innerWidth - headerWidth < 400) {
                attrMobileType = 'push';
            }
        }
        var transitionTime = mobileMenuTransitionTime;
        if(doInstantly === true) {
            transitionTime = 0;
        }

        var triggerContainerWidth = parseFloat(mobileTriggerContainer.outerWidth());

        if(attrMobileDirection == 'left') {
            mobileMenuInner.css('left', -headerWidth);
            if(attrMobileType == 'push') {
                if(headerWidth + triggerContainerWidth > window.innerWidth) {
                    headerWidth = window.innerWidth - triggerContainerWidth;
                    mobileMenuInner.css('width', headerWidth + 'px');
                }

                TweenMax.to(pageContents, transitionTime, {left: headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to(header, transitionTime, {left: headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to($('.dm-mobile-menu-inner'), transitionTime, {left: 0, ease: mobileMenuTransitionEasing});
            } else if(attrMobileType == 'squeeze') {
                TweenMax.to(pageContents, transitionTime, {left: headerWidth, width:window.innerWidth - headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to(header, transitionTime, {left: headerWidth, width:window.innerWidth - headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to($('.dm-mobile-menu-inner'), transitionTime, {left: 0, ease: mobileMenuTransitionEasing});
            } else if(attrMobileType == 'overlay') {
                mobileMenuInner.addClass('bg-color-white');
                mobileTrigger.find('.dm-icon').css('color', '#dadada');

                mobileTriggerContainer.css({'top': '20px', 'left': '20px', 'padding': '0px'});

                TweenMax.to(mobileTriggerContainer, 0.3, {backgroundColor: '#fff', left: 0, top: 0, padding: 20, ease: mobileMenuTransitionEasing});
                TweenMax.to(mobileTriggerContainer, transitionTime, {left: headerWidth, ease: mobileMenuTransitionEasing, delay: 0.3});
                TweenMax.to(mobileMenuInner, transitionTime, {left: 0, ease: mobileMenuTransitionEasing, delay: 0.3});
            }
        } else if(attrMobileDirection == 'right') {
            mobileMenuInner.css('left', 'auto');
            mobileMenuInner.css('right', -headerWidth);
            header.css('right', 0);
            header.css('left', 'auto');
            pageContents.css('right', 0);
            pageContents.css('left', 'auto');



            if(attrMobileType == 'push') {
                if(headerWidth + triggerContainerWidth > window.innerWidth) {
                    headerWidth = window.innerWidth - triggerContainerWidth;
                    mobileMenuInner.css('width', headerWidth + 'px');
                }

                TweenMax.to(pageContents, transitionTime, {right: headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to(header, transitionTime, {right: headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to(mobileMenuInner, transitionTime, {right: 0, ease: mobileMenuTransitionEasing});

            } else if(attrMobileType == 'squeeze') {

                header.css('left', 0);

                TweenMax.to(pageContents, transitionTime, {width: window.innerWidth - headerWidth, ease: mobileMenuTransitionEasing});
                TweenMax.to(header, transitionTime, {width: window.innerWidth - headerWidth + 30, ease: mobileMenuTransitionEasing});
                TweenMax.to(mobileMenuInner, transitionTime, {right: 0, ease: mobileMenuTransitionEasing});

            } else if(attrMobileType == 'overlay') {

                mobileMenuInner.addClass('bg-color-white');
                mobileTrigger.find('.dm-icon').css('color', '#dadada');
                mobileMenuInner.css('left', 'auto');

                if(headerWidth > window.innerWidth) {
                    mobileMenuInner.css('right', -window.innerWidth + 'px');
                } else {
                    mobileMenuInner.css('right', -headerWidth + 'px');
                }
                mobileTriggerContainer.css({'top': '20px', 'right': '20px', 'padding': '0px'});


                var newTriggerRight = headerWidth;
                var overflowAmount = headerWidth + triggerContainerWidth - window.innerWidth;
                if(overflowAmount > 0) {
                    newTriggerRight = window.innerWidth - triggerContainerWidth;
                    //mobileMenuInner.css('padding-left', (window.innerWidth - (headerWidth + triggerContainerWidth)) + 'px');
                    var newMenuPadding = Math.min(triggerContainerWidth - 20, overflowAmount);
                    mobileMenuInner.css('padding-left', newMenuPadding + 'px');
                } else {
                    mobileMenuInner.css('padding-left', '0');
                }

                TweenMax.to(mobileTriggerContainer, 0.3, {backgroundColor: '#fff', right: 0, top: 0, padding: 20, ease: mobileMenuTransitionEasing});
                TweenMax.to(mobileTriggerContainer, transitionTime, {right: newTriggerRight, ease: mobileMenuTransitionEasing, delay: 0.3});
                TweenMax.to(mobileMenuInner, transitionTime, {right: 0, ease: mobileMenuTransitionEasing, delay: 0.3});
            }
        }
    }; // showMobileMenu() END


    function checkMobileTriggerOffScreenUpdate() {
        var distances = mobileTriggerContainer[0].getBoundingClientRect();
        var distanceLeft = distances.left;
        var distanceRight = window.innerWidth - distances.right;

        if(parseInt(distanceLeft) < 0) {
            var crtLeft = parseInt(mobileTriggerContainer.css('left'));
            var crtPadding = parseInt(mobileMenuInner.css('padding-left'));
            //mobileTrigger.css('left',  (-distanceLeft) + 'px');
            //mobileMenuInner.css('padding-left', (-distanceLeft) + 'px')
        } else {
            //mobileTrigger.css('left',  0);
            //mobileMenuInner.css('padding-left', 0);
        }
    }


    window.hideMobileMenu = function(doInstantly) {
        //console.log('hideMobileMenu()');

        disableTriggerClickEvents();
        setTimeout(enableTriggerClickEvents, mobileMenuTransitionTime * 1000);
        pageContents.attr('data-is-offset', 'false');

        // The direction where the mobile mobile menu will come from
        var attrMobileDirection = 'left';

        if(mobileMenuInner.css('right') == 'auto') {
            attrMobileDirection = 'left';
        } else {
            attrMobileDirection = 'right';
        }

        var transitionTime = mobileMenuTransitionTime;
        if(doInstantly === true) {
            transitionTime = 0;
        }

        var headerWidth = parseFloat(mobileMenuInner.css('width'));

        if(attrMobileDirection == 'left') {

            TweenMax.to(pageContents, transitionTime, {
                left: 0,
                width: '100%',
                ease: mobileMenuTransitionEasing
            });

            TweenMax.to(header, transitionTime, {
                left: 0,
                width: window.innerWidth,
                ease: mobileMenuTransitionEasing,
                onComplete: function () {
                    header.css('width', '100%');
                }
            });


            TweenMax.to(mobileMenuInner, transitionTime, {left: -headerWidth, ease: mobileMenuTransitionEasing});

            if(mobileTriggerContainer.css('background-color') != 'rgba(0, 0, 0, 0)') {
                // It means we have to reset the trigger to its original 'look' and position
                mobileMenuInner.addClass('bg-color-white');
                var crtPadding = parseInt(mobileTriggerContainer.css('padding'));
                TweenMax.to(mobileTriggerContainer, 0.3, {backgroundColor: 'rgba(0, 0, 0, 0)', padding: 0, top: crtPadding,  left: crtPadding, ease: mobileMenuTransitionEasing, delay: transitionTime});
                TweenMax.to(mobileTriggerContainer, transitionTime, {left: 0, ease: mobileMenuTransitionEasing});
                TweenMax.to(mobileTriggerContainer, 0, {padding: 20, top: 0, left: 0, delay: transitionTime + 0.3,  ease: mobileMenuTransitionEasing, onComplete: switchTriggerSide});

            }
        } else if(attrMobileDirection == 'right') {

            TweenMax.to(pageContents, transitionTime, {
                right: 0,
                width: '100%',
                ease: mobileMenuTransitionEasing
            });

            if(header.css('right') == 'auto') {
                TweenMax.to(header, transitionTime, {
                    left: 0,
                    width: window.innerWidth,
                    ease: mobileMenuTransitionEasing,
                    onComplete: function () {
                        //header.css('width', '100%');
                    }
                });
            } else {

                TweenMax.to(header, transitionTime, {
                    right: 0,
                    width: window.innerWidth,
                    ease: mobileMenuTransitionEasing,
                    onComplete: function () {
                        //header.css('width', '100%');
                    }
                });

            }


            TweenMax.to(mobileMenuInner, transitionTime, {right: -headerWidth, ease: mobileMenuTransitionEasing});
            if(mobileTriggerContainer.css('background-color') != 'rgba(0, 0, 0, 0)') {
                //mobileMenuInner.addClass('bg-color-white');

                var crtPadding = parseInt(mobileTriggerContainer.css('padding'));
                TweenMax.to(mobileTriggerContainer, 0.3, {backgroundColor: 'rgba(0, 0, 0, 0)', padding: 0, top: crtPadding, right: crtPadding, ease: mobileMenuTransitionEasing, delay: transitionTime});
                TweenMax.to(mobileTriggerContainer, transitionTime, {right: 0, ease: mobileMenuTransitionEasing});
                TweenMax.to(mobileTriggerContainer, 0, {padding: 20, top: 0, right: 0, delay: transitionTime + 0.3, ease: mobileMenuTransitionEasing, onComplete: switchTriggerSide});
            }
        }

    }; // hideMobileMenu() END

    function enableTriggerClickEvents() {
        disableTriggerClickEvents();
        mobileTrigger.on('click', mobileTriggerClickHandler);
    }

    function disableTriggerClickEvents() {
        mobileTrigger.off('click', mobileTriggerClickHandler);
    }

    //$(document).resize(window.updateMobileHeader);
    $(window).resize(
        function() {
            //console.log('onResize() call to hideMobileMenu');
            window.hideMobileMenu(true);
        }
    );

    window.updateMobileHeader = function() {
        var pageIsOffset = pageContents.attr('data-is-offset');

        if(pageIsOffset == 'true') {
            if (parseInt(header.css('width')) != window.innerWidth) {
                TweenMax.to(header, mobileMenuTransitionTime, {
                    width: window.innerWidth,
                    ease: mobileMenuTransitionEasing
                });
            }
        } else {
            header.css('width', window.innerWidth + 'px');
        }


        //console.log('updateMobileHeader()');
        var attrMobileBreakpoint = header.attr('data-mobile-breakpoint');
        if(attrMobileBreakpoint == 'default') {
            attrMobileBreakpoint = 782;
        } else if(attrMobileBreakpoint == 'max') {
            attrMobileBreakpoint = 10000;
        }



        var pageIsMobile = $('body').attr('data-is-mobile');

        if (pageIsOffset == 'true') {
            hideMobileMenu();
        }

        if(parseFloat(window.innerWidth) > parseInt(attrMobileBreakpoint)) {
            // We are on a 'desktop'

            if(pageIsMobile == 'true') {
                $('body').attr('data-is-mobile', 'false');
                desktopMenu.css('display', 'block');

                TweenMax.to(mobileMenu, 0.5, {opacity: 0, delay: 0.5,
                        onComplete: function () {
                            mobileMenu.css('display', 'none');
                        }
                    }
                );

                TweenMax.to(mobileTrigger, 2.5, {opacity: 0, delay: 0.5});
                TweenMax.to(desktopMenu, 0.5, {opacity: 1, delay: 0.2});


            }
        } else {
            // We are on 'mobile'
            if(pageIsOffset == null || pageIsOffset == 'false') {

                switchTriggerSide();
            }

            if(pageIsMobile == null || pageIsMobile == 'false') {
                $('body').attr('data-is-mobile', 'true');
                mobileMenu.css('display', 'block');

                TweenMax.to(desktopMenu, 0.5, {opacity: 0, delay: 0.2,
                        onComplete: function () {
                            desktopMenu.css('display', 'none');
                        }
                    }
                );

                TweenMax.to(mobileMenu, 0.5, {opacity: 1});
                TweenMax.to(mobileTrigger, 0.5, {opacity: 1, delay: 0.5});
            }
        }

        var headerAttrTop = header.attr('data-top');
        mobileMenuInner.css('top', -headerAttrTop);
    };

    function switchTriggerSide() {
        //console.log('switchTriggerSide()');
        // The direction where the mobile mobile menu will come from
        var attrMobileDirection = header.attr('data-mobile-direction');

        if (attrMobileDirection == 'left') {
            if (parseInt(mobileTriggerContainer.css('right')) == 0) {
                TweenMax.to(mobileTriggerContainer, 0.35, {right: -50, ease: Back.easeIn});
                TweenMax.to(mobileTriggerContainer, 0, {left: -50, delay: 0.35});
                TweenMax.to(mobileTriggerContainer, 0.35, {
                    left: 0,
                    right: 'auto',
                    ease: Back.easeOut,
                    delay: 0.4
                });
            } else {
                mobileTriggerContainer.css('left', '0px');
                mobileTriggerContainer.css('right', 'auto');
            }

        } else if (attrMobileDirection == 'right') {
            if (parseInt(mobileTriggerContainer.css('left')) == 0) {
                TweenMax.to(mobileTriggerContainer, 0.35, {left: -50, ease: Back.easeIn});
                TweenMax.to(mobileTriggerContainer, 0, {right: -50, delay: 0.35});
                TweenMax.to(mobileTriggerContainer, 0.35, {
                    right: 0,
                    left: 'auto',
                    ease: Back.easeOut,
                    delay: 0.4
                });
            } else {
                mobileTriggerContainer.css('right', '0px');
                mobileTriggerContainer.css('left', 'auto');
            }
        }
    }


    $(document).ready(function(){
        enableTriggerHoverEvents();

        function mobileTriggerMouseOver() {
            disableTriggerHoverEvents();
            TweenMax.to($(this).find('.dm-icon'), 0.5, {rotationY: 180, delay: 0.5});
            TweenMax.to($(this), 0.5, {rotationY: 180, onComplete: enableTriggerHoverEvents});
        }

        function mobileTriggerMouseOut() {
            disableTriggerHoverEvents();
            TweenMax.to($(this).find('.dm-icon'), 0.5, {rotationY: 0, rotationX: 0, delay: 0.5});
            TweenMax.to($(this), 0.5, {rotationY: 0, rotationX: 0, onComplete: enableTriggerHoverEvents});
        }

        function enableTriggerHoverEvents() {
            mobileTrigger.on('mouseenter', mobileTriggerMouseOver);
            mobileTrigger.on('mouseleave', mobileTriggerMouseOut);
        }

        function disableTriggerHoverEvents() {
            mobileTrigger.off('mouseenter', mobileTriggerMouseOver);
            mobileTrigger.off('mouseleave', mobileTriggerMouseOut);
        }
    });
});