jQuery(
    function($) {
        //var masonryColWidth = $('.masonry').first().attr('data-column-width');
        $(document).ready(function(){
            updateMasonryGrids();
        });

        window.updateMasonryGrids = function() {
            $('.dm-masonry').each(function(){
                var $grid = $(this).isotope({
                    itemSelector: '.dm-grid-item',
                    layoutMode: 'masonry'
                });

                $(this).parent().find('.dm-filter-btn').click(onFilterClick);
                function onFilterClick() {
                    var selectedFilter = $(this).text();
                    $grid.isotope({
                        // filter element with numbers greater than 50
                        filter: function() {
                            // _this_ is the item element. Get text of element's .number
                            var tags = $(this).attr('data-tags');
                            if(tags.indexOf(selectedFilter) != -1) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });
                }
            });
        };

        /*
        $('.dm-masonry').masonry({
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.dm-grid-item',
            // use element for option
            columnWidth: '.dm-grid-item',
            percentPosition: true
        });


        window.updateMasonryGrids = function() {
            console.log('updateMasonryGrids()');
            $('.dm-masonry').masonry({

                // set itemSelector so .grid-sizer is not used in layout
                itemSelector: '.dm-grid-item',
                // use element for option
                columnWidth: '.dm-grid-item',
                percentPosition: true
            });
            console.log('updateMasonryGrids() grid');

            console.log('updateMasonryGrids()');

            $('.dm-masonry').each(function () {
                console.log('updateMasonryGrids() grid');
                // init Masonry
                var grid = $(this).masonry({
                    // set itemSelector so .grid-sizer is not used in layout
                    itemSelector: '.dm-grid-item',
                    // use element for option
                    columnWidth: '.dm-grid-item',
                    percentPosition: true
                });
                // layout Masonry after each image loads
                if (grid.imagesLoaded) {
                    grid.imagesLoaded().progress(function () {
                        grid.masonry('layout');
                    });
                }

                //window.dm_arrMasonryGrids.push(grid);
            })
        }
        */


    }
);
