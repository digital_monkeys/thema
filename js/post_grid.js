jQuery(function($){
    registerComponent(
        {
            'selector': '.dm-post-grid',
            'initFunction': post_grid_init,
            'initOnce': true,
            'initOn': 'load'
        }
    );
    function post_grid_init(component) {
        /*
        component.find('.dm-filter-btn').click(onFilterClick);

        //var innerGrid = component.find('.dm-post-grid-inner');

        $grid.isotope({
            // filter element with numbers greater than 50
            filter: function() {
                // _this_ is the item element. Get text of element's .number
                var number = $(this).find('.number').text();
                // return true to show, false to hide
                return parseInt( number, 10 ) > 50;
            }
        });

        */


        function onFilterClick() {
            var crtFilter = $(this).text();
            innerGrid.find('.dm-grid-item').each(checkGridItem);

            setTimeout(window.updateMasonryGrids, 410);

            function checkGridItem(){
                var postTags = $(this).attr('data-tags');
                if(postTags.indexOf(crtFilter) == -1) {
                    //TweenMax.to($(this), 0.4, {scaleX: 0, scaleY: 0, onComplete: hidePost, onCompleteParams: [$(this)]});
                }
                else {
                    $(this).css('display', 'block');
                    //TweenMax.to($(this), 0.4, {scaleX: 1, scaleY: 1});
                }

                function hidePost(postToHide) {
                    //postToHide.css('display', 'none');
                }
            }

            function rearrangeGrid() {

            }
        }
        /*
        var element = component.find('.dm-number');
        var textAdded = '';
        var attrEndValue = component.attr('data-end-value');
        if(attrEndValue.indexOf('%') != -1)
        {
            textAdded = '%';
        }

        var endValue = parseInt(attrEndValue);

        var counter = {var: 0};
        TweenMax.to(counter, 3, {
            var: endValue,
            onUpdate: function () {
                element.text(Math.ceil(counter.var) + textAdded);
            },
            ease: Quint.easeOut
        });
        */

    }


});