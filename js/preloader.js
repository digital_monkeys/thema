jQuery(document).ready(function($)
{
    $('.main_preloader').each(main_preloader_fadeOut);
    function main_preloader_fadeOut()
    {
        var preloader = $(this);
        setTimeout(fadePreloader, 1000);
        function fadePreloader() {
            preloader.find('.loader-inner').addClass('fade-out');

            setTimeout(fadePreloaderBackground, 450);
            function fadePreloaderBackground()
            {
                preloader.addClass('fade-out');

            }

            setTimeout(removePreloader, 1300);
            function removePreloader() {
                preloader.css('display', 'none');
                updateComponentsOnScroll();
                $(window).bind('scroll', updateComponentsOnScroll);
                $(window).bind('resize', updateComponentsOnResize);
            }
        }
    }

    $( window ).bind('beforeunload', function() {
        $('.main_preloader').addClass('fade-in');
        $('.main_preloader').removeClass('fade-out');
        $('.main_preloader').find('.loader-inner').removeClass('fade-out');
        $('.main_preloader').css('display', 'block');

        /*
        $('.main_preloader').each(main_preloader_fadeIn);
        function main_preloader_fadeIn()
        {
            $(this).addClass('fade-in');
            $(this).removeClass('fade-out');
            $(this).find('.loader-inner').removeClass('fade-out');
            $(this).css('display', 'block');
        }
        */
    });
});
