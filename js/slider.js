jQuery(document).ready(function($) {
    /**
     * Add (register) the slider component to the main component collection
     *
     * The slider component will always be initiated on window resize, because the slider items must have a "fluid"
     * width and we can't use percentage-based width
     *
     * @since 1.0.0
     */

    var dm_sliderErrorMargin = 30;

    registerComponent(
        {
            'selector': '.dm-slider',
            'initFunction': slider_init,
            'resetFunction': slider_reset,
            'initOn' : 'resize',
            'initOnce' : false
        }
    );

    function slider_reset(component) {
        var itemSelector = '.dm-slider-item';
        var itemContainer = component.find('.dm-slider-inner').first();
        var btnLeft = component.find('.dm-nav-button-left');
        var btnRight = component.find('.dm-nav-button-right');
        var btnHeight = parseFloat(btnLeft.height());
        var crtContainerLeft = parseFloat(itemContainer.css('left'));
        var sliderWidth = parseFloat(component.width());

        var transitionDuration  = parseFloat(component.attr('data-transition-duration'));
        var transitionEasing    = component.attr('data-transition-easing');
        var transitionDelay     = parseFloat(component.attr('data-transition-delay'));
        var autoPlay            = component.attr('data-auto-play');
        var autoPause           = component.attr('data-auto-pause');
        var itemContentSize     = component.attr('data-item-content-size');
        var showArrows          = component.attr('data-show-arrows');
        var sliderType          = component.attr('data-slider-type');

        var crtColCount = getCrtColCount(component);

        var itemCount = component.find(itemSelector).length;

        var itemWidth = Math.floor(parseFloat(component.parent().width()) / crtColCount);


        if (sliderType == 'infinite_scroll') {
            if(!window.isVisualComposer()) {
                var arrCrtItems = itemContainer.find(itemSelector).clone();
                var arrCrtItems2 = itemContainer.find(itemSelector).clone();
            }

            arrCrtItems.appendTo(itemContainer);
            arrCrtItems2.appendTo(itemContainer);

            itemCount = component.find(itemSelector).length;
        }


        var maxItemHeight = 0;

        var colCountLG          = component.attr('data-col-lg');
        var colCountMD          = component.attr('data-col-md');
        var colCountSM          = component.attr('data-col-sm');
        var colCountXS          = component.attr('data-col-xs');

        component.find(itemSelector).each(function() {

            // We remove the Bootstrap-related classes
            $(this).removeClass(
                'col-lg-' + colCountLG +
                ' col-md-' + colCountMD +
                ' col-sm-' + colCountSM +
                ' col-xs-' + colCountXS
            );

            // We have to center the content within the slider item, so we try to find the first div inside it and
            // center it horizontally
            $(this).find('div').first().css('margin', '0 auto');


            // If the item has no div inside it and only has inline elements, we provide a fallback for those by setting
            // slider item's 'text-align' property to center
            // We also assign the computed item with to every slider item and set it to be displayed as 'inline-block',
            // in order to have them all in a single line
            $(this).css(
                {
                    "text-align": "center",
                    "width": itemWidth + "px",
                    "display": "inline-block"
                }
            );

            // We test each item to see if its height is bigger than the current maximum height. If it is, then that
            // item's height will be the new maximum
            var crtItemHeight = parseFloat($(this).innerHeight());
            if(crtItemHeight > maxItemHeight)
            {
                maxItemHeight = crtItemHeight;
            }
        });


        // The slider's width is 100%, because it has to be as big as its container
        // The overflow has to be set to 'hidden', in order to hide the invisible items
        // Due to the fact that the overflow is set to 'hidden', we have to make the main slider container as big as
        // the largest item
        component.css(
            {
                "width": "100%",
                "overflow": "hidden",
                "height": maxItemHeight + "px"
            }
        );


        /**
         * The transitions don't always end in the same spot due to approximations and browser resizes, so we leave some
         * room for error
         * @type {number}
         */

        var errorMargin = dm_sliderErrorMargin;
        if(sliderType != 'infinite_scroll')
        {
            errorMargin = 0;
        }


        var objDataForTransitions = {
            "component": component,
            "itemContainer": itemContainer,
            "itemSelector": itemSelector,
            "errorMargin": errorMargin,
            "transitionDuration": parseFloat(component.attr('data-transition-duration')),
            "transitionEasing": component.attr('data-transition-easing'),
            "transitionDelay": parseFloat(component.attr('data-transition-delay')),
            "autoPlay": autoPlay,
            "autoPause": autoPause,
            "sliderType": sliderType,
            "btnLeft": btnLeft,
            "btnRight": btnRight,
            "itemCount": itemCount
        };

        // The navigation buttons have been hidden by CSS as a fallback for non-js browsers, so now we show them
        if(showArrows == 'true') {
            component.find('.dm-nav-button').css('display', 'block');
        }

        // We enable the buttons by default
        enableButtons(objDataForTransitions);

        resetItemContainer(objDataForTransitions);

        if(autoPlay == 'true') {
            setInterval(checkTimeForScroll, 500, objDataForTransitions);
            //setTimeout(checkTimeForScroll, 500, objDataForTransitions);
        }

        component.on('mouseenter', objDataForTransitions, onMouseEnter);
        component.on('mouseleave', objDataForTransitions, onMouseLeave);
    }

    function onItemMouseEnter() {
        console.log('onItemMouseEnter() this = ');
        console.log($(this));
        $(this).css('opacity', '0.1');
    }

    function onMouseEnter(event) {
        event.data.component.attr('data-is-hover', 'true');
    }

    function onMouseLeave(event) {
        event.data.component.attr('data-is-hover', 'false');
    }


    /**
     * The init function for the slider component
     *
     * @param object component The jQuery object containing a specific instance of a slider
     */
    function slider_init(component, event) {
        TweenMax.killAll();
        /**
         * The selector used for the slider items
         * @type {string}
         */
        var itemSelector = '.dm-slider-item';

        /**
         * The jQuery object containing the inner container of the slider.
         *
         * This is the actual container of the slider items, the one that moves around.
         *
         * @type {object}
         */
        var itemContainer = component.find('.dm-slider-inner').first();

        /**
         * The left navigation button (jQuery object)
         *
         * @type {object}
         */
        var btnLeft = component.find('.dm-nav-button-left');

        /**
         * The right navigation button (jQuery object)
         *
         * @type {object}
         */
        var btnRight = component.find('.dm-nav-button-right');

        /**
         * The height of the navigation buttons. Used to center them vertically with the tallest item in the slider
         *
         * @type {Number}
         */
        var btnHeight = parseFloat(btnLeft.height());

        /**
         * The current position of the inner container.
         *
         * This will change when the user clicks the slider navigation buttons or, (if the auto-play functionality is
         * enabled), the slider auto-plays
         * @type {Number}
         */
        var crtContainerLeft = parseFloat(itemContainer.css('left'));

        var sliderWidth = parseFloat(component.width());

        // We create variables for each column count, based on screen size
        var colCountLG          = component.attr('data-col-lg');
        var colCountMD          = component.attr('data-col-md');
        var colCountSM          = component.attr('data-col-sm');
        var colCountXS          = component.attr('data-col-xs');

        // We also need variables for all the attributes that configure the slider's behaviour. These attributes have
        // been set in the shortcode attributes, probably using Visual Composer
        var transitionDuration  = parseFloat(component.attr('data-transition-duration'));
        var transitionEasing    = component.attr('data-transition-easing');
        var transitionDelay     = parseFloat(component.attr('data-transition-delay'));
        var autoPlay            = component.attr('data-auto-play');
        var autoPause           = component.attr('data-auto-pause');
        var itemContentSize     = component.attr('data-item-content-size');
        var showArrows          = component.attr('data-show-arrows');
        var sliderType          = component.attr('data-slider-type');

        /**
         * We create a variable to store the current number of columns based on the screen size.
         * We assume it is 1, and we go from there.
         * @type {number}
         */
        var crtColCount = getCrtColCount(component);


        /**
         * The item's width is equal to the width of the slider's parent divided by the current column count
         *
         * @type {number}
         */
        var itemWidth = Math.floor(parseFloat(component.parent().width()) / crtColCount);

        /**
         * We need to store the number of items in the array
         */
        var itemCount = component.find(itemSelector).length;




        /**
         * We assume the tallest item is 0 pixels tall and we go from there
         * @type {number}
         */
        var maxItemHeight = 0;



        // We use the each() function to iterate through the slider items
        component.find(itemSelector).each(function() {


            // If the item has no div inside it and only has inline elements, we provide a fallback for those by setting
            // slider item's 'text-align' property to center
            // We also assign the computed item with to every slider item and set it to be displayed as 'inline-block',
            // in order to have them all in a single line
            $(this).css('width', itemWidth + 'px');

            // We test each item to see if its height is bigger than the current maximum height. If it is, then that
            // item's height will be the new maximum
            var crtItemHeight = parseFloat($(this).innerHeight());
            if(crtItemHeight > maxItemHeight)
            {
                maxItemHeight = crtItemHeight;
            }
        });

        // Center the navigation buttons vertically
        btnLeft.css('top', (maxItemHeight - btnHeight) / 2 + 'px');
        btnRight.css('top', (maxItemHeight - btnHeight) / 2 + 'px');

        // The slider's width is 100%, because it has to be as big as its container
        // The overflow has to be set to 'hidden', in order to hide the invisible items
        // Due to the fact that the overflow is set to 'hidden', we have to make the main slider container as big as
        // the largest item
        component.css('height', maxItemHeight + 'px');

        // The inner item container has to be positioned either as 'relative' or 'absolute' in order to move it freely
        // using the 'top' and 'left' properties
        // Its width has to be big enough to fit all the items
        itemContainer.css(
            {
            "width": (itemWidth * itemCount + 200) + "px",
            "position": "relative"
            }
        );

        //itemContainer.css('width', (itemWidth * itemCount + 200) + "px");


        /**
         * Used to store the width of the inner container
         * @type {Number}
         */
        var itemContainerWidth = parseFloat(itemContainer.innerWidth());


        var errorMargin = dm_sliderErrorMargin;
        if(sliderType != 'infinite_scroll')
        {
            errorMargin = 0;
        }


        var objDataForTransitions = {
            "component": component,
            "itemContainer": itemContainer,
            "itemSelector": itemSelector,
            "errorMargin": errorMargin,
            "transitionDuration": parseFloat(component.attr('data-transition-duration')),
            "transitionEasing": component.attr('data-transition-easing'),
            "transitionDelay": parseFloat(component.attr('data-transition-delay')),
            "autoPlay": autoPlay,
            "autoPause": autoPause,
            "sliderType": sliderType,
            "btnLeft": btnLeft,
            "btnRight": btnRight,
            "itemCount": itemCount
        };


        // We enable the buttons by default
        enableButtons(objDataForTransitions);

        resetItemContainer(objDataForTransitions);
    }


    function applyScaleEffect(objData) {
        var crtColCount = getCrtColCount(objData.component);
        var crtContainerLeft = parseFloat(objData.itemContainer.css('left'));
        var itemContainerWidth = parseFloat(objData.itemContainer.innerWidth());
        var sliderWidth = parseFloat(objData.component.width());
        var itemWidth = Math.floor(parseFloat(objData.component.parent().width()) / crtColCount);


        objData.itemContainer.find(objData.itemSelector).each(function(){
            if(crtColCount > 2) {
                var itemPos = Math.floor($(this).position().left);
                var relativePos = itemPos + itemWidth / 2 + crtContainerLeft;
                if (relativePos > -itemWidth && relativePos < sliderWidth + itemWidth) {
                    var centerOffset = Math.abs(relativePos - sliderWidth / 2);
                    //console.log(centerOffset);

                    var centerOffsetPercent = (1 + (1 - (centerOffset / sliderWidth)));
                    centerOffsetPercent *= 1.6;
                    centerOffsetPercent -= 2;

                    centerOffsetPercent = Math.min(1, centerOffsetPercent);
                    centerOffsetPercent = Math.max(0.6, centerOffsetPercent);

                    //console.log(centerOffsetPercent);

                    TweenMax.to($(this), 0, {scaleX: centerOffsetPercent, scaleY: centerOffsetPercent})
                }
            } else {
                TweenMax.to($(this), 0, {scaleX: 1, scaleY: 1})
            }
        });
    }


    /**
     * Used to slide the inner item container by one item to the right, so the content will appear to move to
     * the left
     */
    function slideLeft(event) {
        //console.log(event);

        var crtColCount = getCrtColCount(event.data.component);
        var crtContainerLeft = parseFloat(event.data.itemContainer.css('left'));
        var itemContainerWidth = parseFloat(event.data.itemContainer.innerWidth());
        var sliderWidth = parseFloat(event.data.component.width());
        var itemWidth = Math.floor(parseFloat(event.data.component.parent().width()) / crtColCount);

        //console.log('slideLeft() crtContainerLeft = ' + crtContainerLeft);
        //console.log(' ');

        // We only allow the slider to scroll if its left margin is outside the screen to the left
        if(crtContainerLeft < event.data.errorMargin) {
            console.log('slideLeft() condition passed');
            // Immediately after a button click, we disable the buttons until the animation has finished
            disableButtons(event.data);
            event.data.component.attr('data-time-left', parseFloat(event.data.transitionDelay));


            // The actual animation, set to activate the buttons when the transitions is complete
            TweenMax.to(
                event.data.itemContainer,
                event.data.transitionDuration,
                {
                    left: crtContainerLeft + itemWidth,
                    ease: getEasing(event.data.transitionEasing),
                    onComplete: enableButtons,
                    onCompleteParams: [event.data],
                    onUpdate: applyScaleEffect,
                    onUpdateParams: [event.data]
                }
            );
        }
    }

    /**
     * Used to slide the inner item container by one item to the left, so the content will appear to move to
     * the right
     */
    function slideRight(event) {

        var crtContainerLeft = parseFloat(event.data.itemContainer.css('left'));

        var itemContainerWidth = parseFloat(event.data.itemContainer.innerWidth());
        var sliderWidth = parseFloat(event.data.component.width());

        //console.log('slideRight() crtContainerLeft = ' + crtContainerLeft + ", itemContainerWidth = " + itemContainerWidth + ", sliderWidth = " + sliderWidth);
        //console.log(' ');

        // We only allow the slider to scroll if its right margin is outside the screen to the right, meaning if the
        // sum of the left position and the container width is bigger than the slider's width
        if(crtContainerLeft + itemContainerWidth > sliderWidth - event.data.errorMargin) {
            // Immediately after a button click, we disable the buttons until the animation has finished
            disableButtons(event.data);
            console.log("slideRight transitionDelay = " + parseFloat(event.data.transitionDelay));
            event.data.component.attr('data-time-left', parseFloat(event.data.transitionDelay));
            var crtColCount = getCrtColCount(event.data.component);
            var itemWidth = Math.floor(parseFloat(event.data.component.parent().width()) / crtColCount);
            // The actual animation, set to activate the buttons when the transitions is complete
            TweenMax.to(
                event.data.itemContainer,
                event.data.transitionDuration,
                {
                    left: crtContainerLeft - itemWidth,
                    ease: getEasing(event.data.transitionEasing),
                    onComplete: enableButtons,
                    onCompleteParams: [event.data],
                    onUpdate: applyScaleEffect,
                    onUpdateParams: [event.data]
                }
            );
        }

    }

    /**
     * Used to add the event listeners
     */
    function enableButtons(objData) {
        //console.log('enableButtons() objData = ');
        //console.log(objData);
        disableButtons(objData);
        objData.btnLeft.on('click', objData, slideLeft);
        objData.btnRight.on('click', objData, slideRight);

        var crtContainerLeft = parseFloat(objData.itemContainer.css('left'));
        var crtColCount = getCrtColCount(objData.component);
        var itemWidth = Math.floor(parseFloat(objData.component.parent().width()) / crtColCount);

        //console.log('enableButtons() crtContainerLeft = ' + crtContainerLeft + ", reference = " + (itemWidth * objData.itemCount / 3 * 2));
        //console.log(' ');

        if(crtContainerLeft > -objData.errorMargin && crtContainerLeft < objData.errorMargin) {
            resetItemContainer(objData);
        }else if( Math.abs(crtContainerLeft) > itemWidth * objData.itemCount / 3 * 2 - objData.errorMargin &&
            Math.abs(crtContainerLeft) < itemWidth * objData.itemCount / 3 * 2 + objData.errorMargin ) {
            resetItemContainer(objData);
        }
    }

    /**
     * Used to remove the event listeners
     */
    function disableButtons(objData) {
        objData.btnLeft.off('click', slideLeft);
        objData.btnRight.off('click', slideRight);
    }

    /**
     * Decrease the time left by one second and check if it is needed to advance the slider by one item
     */
    function checkTimeForScroll(objData) {

        //console.log('checkTimeForScroll() objData = ');
        //console.log(objData);

        var canProceed = true;

        if(objData.autoPause == 'true') {
            if (objData.component.attr('data-is-hover') == 'true') {
                console.log('isHover');
                canProceed = false;
            }
        }
        if(canProceed == true) {
            var timeLeft = parseFloat(objData.component.attr('data-time-left'));
            if(timeLeft > 0) {
                objData.component.attr('data-time-left', parseFloat(timeLeft) - 0.5);
            } else {
                //slideRight(objData);
                objData.btnRight.trigger('click');
                objData.component.attr('data-time-left', objData.transitionDelay + objData.transitionDuration);
            }
        }

    }

    /**
     * Reset the container to the '0' position. This can be just an apparent 0 when infinite scroll is enabled
     */
    function resetItemContainer(objData) {

        if(!window.isVisualComposer()) {
            var resetPosition = 0;

            var crtColCount = getCrtColCount(objData.component);
            var itemWidth = Math.floor(objData.component.parent().width() / crtColCount);

            if (objData.sliderType == 'infinite_scroll') {
                resetPosition = -(objData.itemCount / 3) * itemWidth;
            }

            // We reset the 'left' value of the item container to 0
            objData.itemContainer.css('left', resetPosition + 'px');
        }

        applyScaleEffect(objData);
    }

    function getCrtColCount(component) {
        var colCountLG          = component.attr('data-col-lg');
        var colCountMD          = component.attr('data-col-md');
        var colCountSM          = component.attr('data-col-sm');
        var colCountXS          = component.attr('data-col-xs');

        if(window.innerWidth > 1200) {
            // If the width of the viewport is bigger than 1200 px, we set the current column count by dividing the
            // maximum number of columns in a row (12) by the number of columns in a large screen
            return 12 / colCountLG;
        } else if(window.innerWidth > 992 && window.innerWidth <= 1200) {
            // Else if the width of the viewport is bigger than 992 px and less 1200px, we set
            // the current column count by dividing the maximum number of columns in a row by the number of columns
            // in a medium screen
            return 12 / colCountMD;
        } else if(window.innerWidth > 768 && window.innerWidth <= 992) {
            // Else if the width of the viewport is between 768 px and 992px, we set the current column count by
            // dividing the maximum number of columns in a row by the number of columns
            // in a small screen
            return 12 / colCountSM;
        } else {
            // Otherwise, it means that we are on an extra small screen, so we set the current column count by
            // dividing the maximum number of columns in a row by the number of columns
            // in a extra small screen
            return 12 / colCountXS;
        }
    }
});