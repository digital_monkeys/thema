<?php
    if ( function_exists( 'the_custom_logo' ) ) {
        dm_custom_logo();
    }
?>
<?php
    $defaults = array (
        'container' => '',
        'theme_location' => 'main-menu',
        'menu_class' => 'menu dm-desktop-menu'
        //'menu_class' => 'headerType1'
    );

    wp_nav_menu( $defaults );
?>

<div class="dm-mobile-menu">
    <div class="dm-trigger-container">
        <div class="dm-trigger">
            <i class="dm-icon fa fa-navicon font-color-gray-light"></i>
        </div>
    </div>
    <div class="dm-header-bottom"></div>
    <div class="dm-mobile-menu-inner">
        <?php
        $defaults = array (
            'container' => '',
            'theme_location' => 'main-menu',
            'menu_class' => 'menu dm-mobile-menu-inner-content'
            //'menu_class' => 'headerType1'
        );

        wp_nav_menu( $defaults );
        ?>
    </div>
</div>


