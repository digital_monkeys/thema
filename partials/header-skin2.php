<?php
    $defaults = array (
        'container' => false,
        'theme_location' => 'main-menu',
        //'menu_class' => 'headerType1'
    );

    wp_nav_menu( $defaults );

    if ( function_exists( 'the_custom_logo' ) ) {
        dm_custom_logo();
    }
?>
