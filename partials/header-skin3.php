<div class="col-sm-3">
    <?php
        if ( function_exists( 'the_custom_logo' ) ) {
            dm_custom_logo();
        }
    ?>
</div>
<div class="col-sm-9">
    <?php
        $defaults = array (
            'container' => false,
            'theme_location' => 'main-menu',
            //'menu_class' => 'headerType1'
        );

        wp_nav_menu( $defaults );
    ?>
</div>

