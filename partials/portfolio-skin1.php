<?php
$dm_post_image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$meta = get_post_meta($post->ID);
//print_r($meta);

$return_string =
    "<div class='row dm_portfolio_item'>
        <div class='col-sm-4'>
            <img class='dm_image' src='$dm_post_image_url' alt='service image'>
        </div>
        <div class='col-sm-8'>
            <div class='dm_separator'></div>
            <div class='dm_content_container'>

                <a href='".get_the_permalink()."'>
                    <h1 class='dm_title font-colorGrayDark font1'> ".get_the_title()." </h1>
                </a>
                <p>
                    <span class='dm_highlight font-color-1 font1'>
                        Client
                    </span>
                    <span class='dm_normal_text'> ".dm_get_meta($post->ID, 'dm_client')." </span>
                </p>
                <p>
                    <span class='dm_highlight font-color-1 font1'>
                        Date:
                    </span>
                    <span class='dm_normal_text'> ".dm_get_meta($post->ID, 'dm_date')." </span>
                </p>
                <p>
                    <span class='dm_highlight font-color-1 font1'>
                        Duration:
                    </span>
                    <span class='dm_normal_text'> ".dm_get_meta($post->ID, 'dm_duration')." </span>
                </p>
                <p>
                    <span class='dm_highlight font-color-1 font1'>
                        Budget:
                    </span>
                    <span class='dm_normal_text'>  </span>
                </p>
                    <span class='dm_highlight font-color-1 font1'>
                        Description:
                    </span>
                    <span class='dm_normal_text'>".get_the_content()." </span>
                </p>
                <p class='dm_btn_action font-color-1 font1'>
                     Read More
                </p>
            </div>
        </div>
    </div>
";
echo $return_string;