<?php
$dm_imgUrl = get_template_directory_uri()."/graphics/svg/color1/separator_1_horizontal.svg";
?>
<?php
$single = '';
//if(is_single())
//{
    $single = 'post-single';
//}


/*
$arrCategories= wp_get_post_categories($postID);
$strCategories = '';
foreach($arrCategories as $category) {
    //$strCategories .= $category->slug.' ';

}
*/
//print_r($arrCategories);

?>
<div class='col-xs-12 dm-post-1 <?php echo $single; ?>'>
    <?php
        // Check if the post is displayed on its own
        if(is_single())
        {
            // Check if the post has a Post Thumbnail assigned to it.
            if ( has_post_thumbnail() ) {
                $dm_post_image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                ?>
                <img class="dm-featured-image" src="<?php echo $dm_post_image_url; ?>" longdesc="URL_2" alt="Text_2" />
            <?php
            }
        }
    ?>
    <h4 class='date'>
        <a href="<?php echo get_day_link(get_post_time('Y'), get_post_time('m'), get_post_time('j')); ?>">
            <?php the_time('l, F j, Y'); ?>
        </a>
    </h4>
    <h1 class='title'>
        <a href="<?php the_permalink(); ?>">
            <?php the_title(); ?>
        </a>
    </h1>
    <img class='separator img-responsive' src='<?php echo $dm_imgUrl; ?>'>
    <h4 class='content'>
        <?php
            if(is_single())
            {
                the_content();
            }
            else
            {
                echo get_the_excerpt();
            }
        ?>
    </h4>

    <div class="row dm-info">
        <div class="col-sm-4">
            <h2 class="dm-social">
                <i class="fa fa-facebook-square font-color-gray-light"></i>
                <i class="fa fa-twitter-square font-color-gray-light"></i>
                <i class="fa fa-google-plus-square font-color-gray-light"></i>
            </h2>
        </div>
        <div class="col-sm-8">
            <h4 class="dm-author">
                Posted by
                <span class="dm-highlight">
                    <?php the_author_posts_link(); ?>
                </span>

            </h4>
        </div>
    </div>
</div>