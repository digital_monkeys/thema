<?php
    $dm_imgUrl = get_template_directory_uri()."/graphics/svg_color1/separator_1_horizontal.svg";
?>

<div class='blogPost_1'>
    <p class='date'> <?php echo the_date(); ?> </p>
    <h4 class='title'> <?php echo the_title(); ?> </h4>
    <img class='separator img-responsive' src='<?php echo $dm_imgUrl; ?>'>
    <p class='content'>
        <?php echo the_excerpt(); ?>
    </p>
    <div class='separator2'></div>
</div>