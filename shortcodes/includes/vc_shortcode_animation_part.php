<?php
$dm_vc_transition_params = array(
    array(
        "param_name" => "transition_type",
        "type" => "dropdown",
        "heading" => __(" Transition type", ""),
        "value" => dm_get_transition_options(),
        "description" => __("If you select any option other than 'none', this element will appear with
                    an animation when it enters the user's viewport"),
        "group" => __("Animation", ""),
    ),
    array(
        "param_name" => "amplification_movement",
        "type" => "dropdown",
        "heading" => __(" Amplify movement", ""),
        "value" => array('x1', 'x1.25', 'x1.5', 'x1.75', 'x2', 'x2.25', 'x2.5', 'x2.75', 'x3', 'x3.25', 'x3.5', 'x3.75', 'x4', 'x4.25', 'x4.5', 'x4.75', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'x12', 'x14', 'x16', 'x18', 'x20', 'x25', 'x30', 'x35', 'x40', 'x45', 'x50', 'x55', 'x60', 'x65', 'x70', 'x75', 'x80', 'x90', 'x95', 'x100'),
        "description" => __("For the animations that involve movement, you can amplify that movement by a factor "),
        "group" => __("Animation", ""),
    ),
    array(
        "param_name" => "amplification_rotation",
        "type" => "dropdown",
        "heading" => __(" Amplify rotation", ""),
        "value" => array('x1', 'x1.25', 'x1.5', 'x1.75', 'x2', 'x2.25', 'x2.5', 'x2.75', 'x3', 'x3.25', 'x3.5', 'x3.75', 'x4', 'x4.25', 'x4.5', 'x4.75', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'x12', 'x14', 'x16', 'x18', 'x20', 'x25', 'x30', 'x35', 'x40', 'x45', 'x50', 'x55', 'x60', 'x65', 'x70', 'x75', 'x80', 'x90', 'x95', 'x100'),
        "description" => __("For the animations that involve rotation, you can amplify that movement by a factor "),
        "group" => __("Animation", ""),
    ),
    array(
        "param_name" => "transition_duration",
        "type" => "dropdown",
        "heading" => __("Transition duration", ""),
        "value" => dm_get_transition_duration_options(),
        "description" => __("Choose how long the transition should be (in seconds)"),
        "group" => __("Animation", ""),
    ),
    array(
        "param_name" => "transition_easing",
        "type" => "dropdown",
        "heading" => __("Transition easing (timing)", ""),
        "value" => dm_get_transition_easing_options(),
        "description" => __("Choose how the transition should progress (try out and see how this setting affects the end result)", ""),
        "group" => __("Animation", ""),
    ),
    /*
    array(
        "param_name" => "animate_on",
        "type" => "dropdown",
        "heading" => __("Animate on", ""),
        "value" => dm_get_animate_on_options(),
        "description" => __("Choose when to display the animation (the \"scroll\" option means that the element will
                    be animated when it enters the user's viewport))", ""),
        "group" => __("Animation", ""),
    ),
    */
    array(
        "param_name" => "transition_delay",
        "type" => "dropdown",
        "heading" => __("Transition delay", ""),
        "value" => dm_get_transition_delay_options(),
        "description" => __("Choose the delay of the transition (in seconds)"),
        "group" => __("Animation", ""),
    ),
    /*
    array(
        "param_name" => "transition_repeat",
        "type" => "dropdown",
        "heading" => __("Transition repeat count", ""),
        "value" => dm_get_transition_repeat_options(),
        "description" => __("Choose how many times you want the transition to be repeated"),
        "group" => __("Animation", ""),
    ),
    */
);