<?php
$dm_vc_css_params = array(
    array(
        'type' => 'css_editor',
        'heading' => __( 'CSS', '' ),
        'param_name' => 'design_options',
        'group' => __( 'Design options', '' ),
    ),
    array(
        'type' => 'textfield',
        'heading' => __( 'Custom CSS class (one or more)', '' ),
        'param_name' => 'custom_css_class',
        'description' => __("You can add one or more classes to your content element, as long as you put a space between them.
            The CSS (or LESS) code to style these classes should be added in the admin part, under the section 'Custom CSS (or LESS)'. ", ""),
        'group' => __( 'Design options', '' ),
    ),
    array(
        'type' => 'textfield',
        'heading' => __( 'Custom CSS ID (one or more)', '' ),
        'param_name' => 'custom_css_id',
        'description' => __("You can add one or more ids to your content element, as long as you put a space between them.
            The CSS (or LESS) code to style these ids should be added in the admin part, under the section 'Custom CSS (or LESS)'. ", ""),
        'group' => __( 'Design options', '' ),
    ),
);