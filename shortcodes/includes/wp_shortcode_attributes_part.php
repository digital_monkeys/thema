<?php
extract(shortcode_atts(array(
    'transition_type' => 'none',
    'transition_duration' => '0.3',
    'transition_easing' => 'Power0.easeIn',
    //'animate_on' => 'scroll',
    'transition_delay' => '0',
    'transition_repeat' => '1',
    'design_options' => '',
    'custom_css_class' => '',
    'custom_css_id' => '',
    'amplification_movement' => 'x1',
    'amplification_rotation' => 'x1',
), $atts));

//$easing = dm_get_easing($transition_easing);

if($transition_type != 'none')
{
    $dynamicClass = 'dm-animate';
    $animationData =
        "
        data-transition-type='$transition_type'
        data-transition-delay='$transition_delay'
        data-transition-duration='$transition_duration'
        data-transition-easing='$transition_easing'
        data-transition-amplification-movement='$amplification_movement'
        data-transition-amplification-rotation='$amplification_rotation'
        ";
}
else
{
    $dynamicClass = '';
    $animationData = '';
}

$design_options_class = esc_attr(apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $design_options, ' ' ), $tag, $atts ));
$dynamicClass .= ' '.$design_options_class;
$dynamicClass .= ' '.$custom_css_class;
$dynamicId = $custom_css_id;