<?php
add_action( 'vc_before_init', 'vc_dm_button' );
function vc_dm_button()
{
    $arrIndividualParams = array(
        array(
            "param_name"    => "label",
            "type"          => "textfield",
            "heading"       => __('Button Text', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            "param_name"    => "icon",
            "type"          => "textfield",
            "heading"       => __('Icon class', 'dm'),
            "group"         => __('Content', 'dm'),
        ),

        array(
            "param_name"    => "color_icon",
            "type"          => "dropdown",
            "heading"       => __('Icon color', 'dm'),
            "value"         => dm_get_color_options(),
            "group"         => __('Colors', 'dm'),
        ),
        array(
            "param_name"    => "color_icon_background",
            "type"          => "dropdown",
            "heading"       => __('Icon background color', 'dm'),
            "value"         => dm_get_color_options(),
            "group"         => __('Colors', 'dm'),
        ),
        array(
            "param_name"    => "color_label",
            "type"          => "dropdown",
            "heading"       => __('Label color', 'dm'),
            "value"         => dm_get_color_options(),
            "group"         => __('Colors', 'dm'),
        ),
        array(
            "param_name"    => "color_label_background",
            "type"          => "dropdown",
            "heading"       => __('Label background color', 'dm'),
            "value"         => dm_get_color_options(),
            "group"         => __('Colors', 'dm'),
        ),
        array(
            "param_name"    => "font_label",
            "type"          => "dropdown",
            "heading"       => __('Title font', 'dm'),
            "value"         => array('Theme font 1', 'Theme font 2'),
            "group"         => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"      => __('Button', 'dm'),
        "base"      => "dm_button",
        "icon"      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"  => __('Thema', 'dm'),
        "class"     => "dm_shortcode",
        "params"    => $arrAllParams
    ));
}