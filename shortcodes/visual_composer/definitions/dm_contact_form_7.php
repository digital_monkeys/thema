<?php
add_action( 'vc_before_init', 'vc_dm_contact_form_7' );
function vc_dm_contact_form_7()
{
    $arrIndividualParams = array(
        array(
            "param_name"    => "form",
            "type"          => "dropdown",
            "heading"       => __('Select your form', 'dm'),
            "description"   => __('Select one of the previously created "Contact Form 7" forms'),
            "value"         => dm_get_contact_forms(),
        ),

        array(
            "param_name"    => "skin",
            "type"          => "dropdown",
            "heading"       => __('Skin', 'dm'),
            "description"   => __('Select a skin for the form'),
            "value"         => array('1', '2', '3'),
        ),

        array(
            "param_name"    => "color",
            "type"          => "dropdown",
            "heading"       => __('Color', ''),
            "description"   => __('Select a color for the form'),
            "value"         => dm_get_color_options(),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"      => __('Contact Form 7 (Thema) ', ''),
        "base"      => "dm_contact_form_7",
        "icon"      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "class"     => "",
        "category"  => __('Thema', ''),
        "params"    => $arrAllParams,
    ));
}