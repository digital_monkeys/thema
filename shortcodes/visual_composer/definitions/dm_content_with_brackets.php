<?php
add_action( 'vc_before_init', 'vc_dm_content_with_brackets' );
function vc_dm_content_with_brackets()
{
    $arrIndividualParams = array(
        array(
            "param_name"    => "line_1_content",
            "type"          => "textfield",
            "heading"       => __('Line 1 content', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            "param_name"    => "line_2_content",
            "type"          => "textfield",
            "heading"       => __('Line 2 content', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            "param_name"    => "line_3_content",
            "type"          => "textfield",
            "heading"       => __('Line 3 content', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            "param_name"    => "line_4_content",
            "type"          => "textfield",
            "heading"       => __('Line 4 content', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            "param_name"    => "middle_content",
            "type"          => "textarea",
            "heading"       => __('Middle content', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            "param_name"    => "right_content",
            "type"          => "textarea",
            "heading"       => __('Right content', 'dm'),
            "group"         => __('Content', 'dm'),
        ),
        array(
            'type'          => 'css_editor',
            'heading'       => __('Css', 'dm'),
            'param_name'    => 'css',
            'group'         => __('Design options', 'dm'),
        ),

    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);



    vc_map(array(
        "name" => __('Content with brackets', 'dm'),
        "base" => "dm_content_with_brackets",
        "icon" => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category" => __('Thema', 'dm'),
        "group" => __('My Content', 'dm'),
        "params" => $arrAllParams,
    ));
}