<?php
add_action( 'vc_before_init', 'vc_dm_feature_item' );
function vc_dm_feature_item()
{
    $arrIndividualParams = array(
        array(
            "param_name"    => "title",
            "type"          => "textfield",
            "heading"       => __('Feature Name', 'dm'),
            "value"         => '',
        ),
        array(
            "param_name"    => "description",
            "type"          => "textarea",
            "heading"       => __('Feature Description', 'dm'),
            "value"         => '',
        ),
        array(
            "param_name"    => "icon",
            "type"          => "textfield",
            "heading"       => __('Feature Icon (name of FontAwesome class or URL to icon file)', 'dm'),
            "value"         => '',
        ),
        array(
            "param_name"    => "color",
            "type"          => "dropdown",
            "heading"       => __('Color', 'dm'),
            "value"         => dm_get_color_options(),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"          => __('Feature Item', ''),
        "base"          => "dm_feature_item",
        "icon"          => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "description"   => __('Has an icon, along with a title and description', 'dm'),
        "class"         => "",
        "category"      => __('Thema', 'dm'),
        "params"        => $arrAllParams,
    ));
}