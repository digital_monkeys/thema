<?php
add_action( 'vc_before_init', 'vc_dm_hero' );
function vc_dm_hero()
{
    $arrIndividualParams = array(
        array(
            "param_name"       => "title",
            "type"             => "textfield",
            "heading"          => __('Title message', 'dm'),
            "group"            => __('Content', 'dm'),
        ),
        array(
            "param_name"       => "subtitle",
            "type"             => "textfield",
            "heading"          => __('Subtitle message (optional)', 'dm'),
            "value"            => array('1', '2'),
            "group"            => __('Content', 'dm'),
        ),
        array(
            "param_name"       => "image_id",
            "type"             => "attach_image",
            "heading"          => __('Background image', 'dm'),
            "group"            => __('Content', 'dm'),
        ),
        array(
            "param_name"       => "skin",
            "type"             => "dropdown",
            "heading"          => __('Skin', 'dm'),
            "value"            => array('1','2'),
            "group"            => __('Skin', 'dm'),
        ),
        array(
            "param_name"       => "is_full_screen",
            "type"             => "checkbox",
            "heading"          => __('Display full screen', ''),
            "description"      => __('In "full screen" mode, the navigation has a transparent background and is displayed on top of the hero image', 'dm'),
            "group"            => __('General', 'dm'),
        ),
        array(
            "param_name"       => "header_style",
            "type"             => "dropdown",
            "heading"          => __('Header color (default, light or dark)', 'dm'),
            "value"            => array('Default', 'Light', 'Dark'),
            "description"      => __('If this hero image is the first element on a page,
                it can make the menu on top of the page either light or dark, in order to make it more readable. You can choose which one it is.', 'dm'),
            "group"            => __('General', 'dm'),
            "dependency"       =>
                array(
                    "element"   => "is_full_screen",
                    "not_empty" => true,
                )
        ),
        array(
            "param_name"       => "title_color",
            "type"             => "dropdown",
            "heading"          => __('Title color (theme-defined colors)', 'dm'),
            "value"            => dm_get_color_options(),
            "group"            => __('Colors', 'dm'),
        ),
        array(
            "param_name"       => "title_font",
            "type"             => "dropdown",
            "heading"          => __('Title font (theme-defined fonts)', 'dm'),
            "value"            => array('Theme font 1', 'Theme font 2'),
            "group"            => __('Fonts', 'dm'),
        ),
        array(
            "param_name"       => "subtitle_color",
            "type"             => "dropdown",
            "heading"          => __('Subtitle color (theme-defined colors)', 'dm'),
            "value"            => dm_get_color_options(),
            "group"            => __('Colors', 'dm'),
        ),
        array(
            "param_name"       => "subtitle_font",
            "type"             => "dropdown",
            "heading"          => __('Subtitle font (theme-defined font)', 'dm'),
            "value"            => array('Theme font 1', 'Theme font 2'),
            "group"            => __('Fonts', 'dm'),
        ),
        array(
            "param_name"       => "title_specific_color",
            "type"             => "colorpicker",
            "heading"          => __('(Optional) Choose specific title color', 'dm'),
            "description"      => __('If you select a color using this color picker, it will override the dropdown used to select a theme color', 'dm'),
            "group"            => __('Colors', 'dm'),
        ),
        array(
            "param_name"       => "subtitle_specific_color",
            "type"             => "colorpicker",
            "heading"          => __('(Optional) Choose specific subtitle color', 'dm'),
            "description"      => __('If you select a color using this color picker, it will override the dropdown used to select a theme color', 'dm'),
            "group"            => __('Colors', 'dm'),
        ),
        array(
            "param_name"       => "title_background_color",
            "type"             => "dropdown",
            "heading"          => __('Title background color (theme-defined colors)', 'dm'),
            "value"            => dm_get_color_options(),
            "group"            => __('Colors', 'dm'),
        ),
        array(
            "param_name"       => "subtitle_background_color",
            "type"             => "dropdown",
            "heading"          => __('Show title background', 'dm'),
            "value"            => dm_get_color_options(),
            "group"            => __('Colors', 'dm'),
        ),
        array(
            "param_name"       => "show_title_background",
            "type"             => "checkbox",
            "heading"          => __('Show title background', 'dm'),
            "value"            => '',
            "group"            => __('Other', 'dm'),
        ),
        array(
            "param_name"       => "show_subtitle_background",
            "type"             => "checkbox",
            "heading"          => __('Show subtitle background', 'dm'),
            "value"            => '',
            "group"            => __('Other', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);




    vc_map(array(
        "name"                 => __('Hero', 'dm'),
        "base"                 => "dm_hero",
        "icon"                 => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"             => __('Thema', 'dm'),
        "params"               => $arrAllParams,
    ));
}