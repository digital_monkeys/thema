<?php

add_action( 'vc_before_init', 'vc_dm_job_item' );
function vc_dm_job_item() {

    $arrIndividualParams = array(
        array(
            "param_name"        => "title",
            "type"              => "textfield",
            "heading"           => __('Job title', 'dm'),
            "value"             => "",
        ),
        array(
            "param_name"        => "experience",
            "type"              => "textfield",
            "heading"           => __('Required Experience', 'dm'),
            "value"             => "",
        ),
        array(
            "param_name"        => "skills",
            "type"              => "textarea",
            "heading"           => __('Required Skills', 'dm' ),
            "value"             => "",
        ),
        array(
            "param_name"        => "job_description",
            "type"              => "textarea",
            "heading"           => __('Job Description', 'dm' ),
            "value"             => "",
        ),
        array(
            "param_name"        => "perks",
            "type"              => "textarea",
            "heading"           => __('What your company offers', 'dm' ),
            "value"             => "",
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map( array(
        "name"                  => __('Job Item', 'dm' ),
        "base"                  => "dm_job_item",
        "description"           => __('You can choose the content to be displayed.', 'dm'),
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ) );
}