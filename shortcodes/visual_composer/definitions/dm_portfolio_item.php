<?php
add_action( 'vc_before_init', 'vc_dm_portfolio_item' );
function vc_dm_portfolio_item()
{
    $arrIndividualParams = array(
        array(
            "param_name"        => "title",
            "type"              => "textarea",
            "heading"           => __('Title', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "type"              => "attach_image",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Portfolio Image', 'dm'),
            "param_name"        => "image_id",
            "value"             => __( "", "" ),
            "description"       => __('Choose an image that shows your portfolio item', 'dm' ),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "client",
            "type"              => "textfield",
            "heading"           => __('Client', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "date",
            "type"              => "textfield",
            "heading"           => __('Date', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "duration",
            "type"              => "textfield",
            "heading"           => __('Duration', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "budget",
            "type"              => "textfield",
            "heading"           => __('Budget', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "description",
            "type"              => "textarea",
            "heading"           => __('Description', 'dm'),
            "group"             => __('Content', 'dm'),
        ),


        array(
            "param_name"        => "color_title",
            "type"              => "dropdown",
            "heading"           => __('Title color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_highlight",
            "type"              => "dropdown",
            "heading"           => __('Highlight color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_normal_text",
            "type"              => "dropdown",
            "heading"           => __('Normal text color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_description",
            "type"              => "dropdown",
            "heading"           => __('Description color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),

        array(
            "param_name"        => "font_title",
            "type"              => "dropdown",
            "heading"           => __('Title font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
        array(
            "param_name"        => "font_description",
            "type"              => "dropdown",
            "heading"           => __("Description font", ""),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"                  => __('Portfolio Item', 'dm'),
        "base"                  => "dm_portfolio_item",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ));
}