<?php
add_action( 'vc_before_init', 'vc_dm_post_grid' );
function vc_dm_post_grid()
{
    $arrIndividualParams = array(
        array(
            "param_name"        => "post_type",
            "type"              => "dropdown",
            "heading"           => __('Post Type', 'dm'),
            "value"             => dm_get_post_types(),
            "group"             => __('Content', 'dm'),
        ),

        array(
            "param_name"        => "post_template",
            "type"              => "dropdown",
            "heading"           => __('Post Template', 'dm'),
            "value"             => dm_get_partial_list(),
            "description"       => __('Pick a template for each specific post item'),
            "group"             => __('Content', 'dm'),
        ),

        array(
            "param_name"        => "post_count",
            "type"              => "dropdown",
            "heading"           => __('Post Count', 'dm'),
            "value"             => array('all', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ,13, 14, 15, 16, 17, 18, 19, 20),
            "description"       => __('How many posts you want to be displayed', 'dm'),
            "group"             => __('Content', 'dm'),
        ),



        array(
            "param_name"        => "display_filters",
            "type"              => "checkbox",
            "heading"           => __('Display Filters', 'dm'),
            "group"             => __('Filters', 'dm'),
        ),

        array(
            "param_name"        => "filter_by",
            "type"              => "dropdown",
            "heading"           => __('Filter by', 'dm'),
            "value"             => array('tags', 'categories'),
            "description"       => __('', 'dm'),
            "group"             => __('Filters', 'dm'),
            "dependency"        =>
                array(
                    "element"   => "display_filters",
                    "not_empty" => true,
                )
        ),

        array(
            "param_name"        => "filter_skin",
            "type"              => "dropdown",
            "heading"           => __('Filter button skin', 'dm'),
            "value"             => array('1', '2', '3'),
            "description"       => __('', 'dm'),
            "group"             => __('Filters', 'dm'),
            "dependency"        =>
                array(
                    "element"   => "display_filters",
                    "not_empty" => true,
                )
        ),





        array(
            "param_name"        => "grid_type",
            "type"              => "dropdown",
            "heading"           => __('Grid Type', 'dm'),
            "value"             => array('normal', 'masonry'),
            "group"             => __('Layout', 'dm'),
        ),









        array(
            "param_name"        => "column_count_large",
            "type"              => "dropdown",
            "heading"           => __('Columns on a large screen', 'dm'),
            "value"             => array(1, 2, 3, 4, 6, 12),
            "group"             => __('Layout', 'dm'),
        ),

        array(
            "param_name"        => "column_count_medium",
            "type"              => "dropdown",
            "heading"           => __('Columns on a medium screen', 'dm'),
            "value"             => array(1, 2, 3, 4, 6, 12),
            "group"             => __('Layout', 'dm'),
        ),

        array(
            "param_name"        => "column_count_small",
            "type"              => "dropdown",
            "heading"           => __('Columns on a small screen', 'dm'),
            "value"             => array(1, 2, 3, 4, 6, 12),
            "group"             => __('Layout', 'dm'),
        ),

        array(
            "param_name"        => "column_count_extra_small",
            "type"              => "dropdown",
            "heading"           => __('Columns on an extra small screen', 'dm'),
            "value"             => array(1, 2, 3, 4, 6, 12),
            "group"             => __('Layout', 'dm'),
        ),

        array(
            "param_name"        => "column_width",
            "type"              => "textfield",
            "heading"           => __('Column Width - in pixels', 'dm'),
            "value"             => '',
            "description"       => __('(Optional) Column width for masonry grid (This option overrides everything else.
                If you specify fixed width for the columns, they are not going to be fluid anymore, and
                each item is going to have that exact width.)', 'dm'),
            "group"             => __('Layout', 'dm'),
            "dependency"        =>
                array(
                    "element"   => "grid_type",
                    "value"     => "masonry",
                )
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);


    vc_map(array(
        "name"                  => __('Post Grid', 'dm'),
        "base"                  => "dm_post_grid",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ));
}