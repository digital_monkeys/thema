<?php

add_action( 'vc_before_init', 'vc_dm_product_display' );
function vc_dm_product_display() {

    $arrIndividualParams = array(
        array(
            "type"              => "textfield",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Product name', 'dm'),
            "param_name"        => "title",
            "value"             => __('', 'dm'),
            "description"       => __('This is where you add your product\'s name', 'dm')
        ),
        array(
            "type"              => "textfield",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Tagline', 'dm'),
            "param_name"        => "tagline",
            "value"             => __('', 'dm'),
            "description"       => __('This is the subtitle that appears below the product\'s name', 'dm')
        ),
        array(
            "type"              => "attach_image",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Product Image', 'dm'),
            "param_name"        => "image_id",
            "value"             => __('', 'dm'),
            "description"       => __('Choose an image that shows your product', 'dm')
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);



    vc_map( array(
        "name"                  => __('Product Display', 'dm'),
        "base"                  => "dm_product_display",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ) );
}