<?php

add_action( 'vc_before_init', 'vc_dm_progress_bar' );
function vc_dm_progress_bar() {

    $arrIndividualParams = array(
        array(
            "param_name"        => "label",
            "type"              => "textfield",
            "heading"           => __('Label', 'dm'),
            "value"             => "",
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "current_value",
            "type"              => "textfield",
            "heading"           => __('Type the current value', 'dm'),
            "value"             => 0,
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "total_value",
            "type"              => "textfield",
            "heading"           => __('Type the total value', 'dm'),
            "value"             => 100,
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "color_label",
            "type"              => "dropdown",
            "heading"           => __('Label color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_current_value",
            "type"              => "dropdown",
            "heading"           => __('Current color value', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_total_value",
            "type"              => "dropdown",
            "heading"           => __('Color total  value', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "font_label",
            "type"              => "dropdown",
            "heading"           => __('Label font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
        array(
            "param_name"        => "skin",
            "type"              => "dropdown",
            "heading"           => __('Progress bar skin', 'dm'),
            "value"             => array('1', '2', '3'),
            "group"             => __('Skin', 'dm'),
        ),

    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map( array(
        "name"                  => __('Progress bar', 'dm'),
        "base"                  => "dm_progress_bar",
        "description"           => __('You can choose the content to be displayed.', 'dm'),
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ) );
}