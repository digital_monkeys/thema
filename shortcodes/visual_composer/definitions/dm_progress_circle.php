<?php
add_action( 'vc_before_init', 'vc_dm_progress_circle' );
function vc_dm_progress_circle()
{
    $arrIndividualParams = array(
        array(
            "param_name"        => "progress",
            "type"              => "textfield",
            "heading"           => __('Progress', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "total",
            "type"              => "textfield",
            "heading"           => __('Total', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "label",
            "type"              => "textfield",
            "heading"           => __('Label', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "color_number",
            "type"              => "dropdown",
            "heading"           => __('Number color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_label",
            "type"              => "dropdown",
            "heading"           => __('Label color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "font_number",
            "type"              => "dropdown",
            "heading"           => __('Number font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
        array(
            "param_name"        => "font_label",
            "type"              => "dropdown",
            "heading"           => __('Label font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"                  => __('Progress Circle', 'dm'),
        "base"                  => "dm_progress_circle",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ));
}