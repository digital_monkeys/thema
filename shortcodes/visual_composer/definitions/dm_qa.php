<?php
add_action( 'vc_before_init', 'vc_dm_qa' );
function vc_dm_qa()
{
    $arrIndividualParams = array(
        array(
            "param_name"        => "icon",
            "type"              => "textfield",
            "heading"           => __('Icon (name of FontAwesome class or URL to icon file)', 'dm'),
            "value"             => '',
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "question",
            "type"              => "textfield",
            "heading"           => __('Question here', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "answer",
            "type"              => "textarea",
            "heading"           => __('Answer here', 'dm'),
            "group"             => __('Content', 'dm'),
        ),
        array(
            "param_name"        => "color_icon",
            "type"              => "dropdown",
            "heading"           => __('Icon color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_question",
            "type"              => "dropdown",
            "heading"           => __('Question color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "color_answer",
            "type"              => "dropdown",
            "heading"           => __('Answer color', 'dm'),
            "value"             => dm_get_color_options(),
            "group"             => __('Colors', 'dm'),
        ),
        array(
            "param_name"        => "font_question",
            "type"              => "dropdown",
            "heading"           => __('Question font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
        array(
            "param_name"        => "font_answer",
            "type"              => "dropdown",
            "heading"           => __('Answer font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2'),
            "group"             => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"                  => __('FAQ', 'dm'),
        "base"                  => "dm_qa",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "description"           => __('Has an icon, along with a title and description', 'dm'),
        "class"                 => "",
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ));
}