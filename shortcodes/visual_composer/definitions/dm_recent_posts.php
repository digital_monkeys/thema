<?php
add_action( 'vc_before_init', 'vc_dm_recent_posts' );
function vc_dm_recent_posts()
{
    $arrIndividualParams = array(
        array(
            "param_name"            => "type",
            "type"                  => "dropdown",
            "heading"               => __('Recent Posts Type', 'dm'),
            "value"                 => array(1, 2, 3),
        ),
        array(
            "param_name"            => "columns",
            "type"                  => "dropdown",
            "heading"               => __('Columns', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "description"           => __('How many posts per row to be displayed', 'dm')
        ),
        array(
            "param_name"            => "count",
            "type"                  => "dropdown",
            "heading"               => __('Post Count', 'dm'),
            "value"                 => array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20),
            "description"           => __('How many posts you want to be displayed', 'dm')
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);


    vc_map(array(
        "name"                      => __('Recent Posts', 'dm'),
        "base"                      => "dm_recent_posts",
        "icon"                      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"                  => __('Thema', 'dm'),
        "params"                    => $arrAllParams,
    ));
}