<?php
add_action( 'vc_before_init', 'vc_dm_service_item' );
function vc_dm_service_item()
{
    $arrIndividualParams = array(
        array(
            "param_name"            => "title",
            "type"                  => "textfield",
            "heading"               => __('Title', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),
        array(
            "param_name"            => "number",
            "type"                  => "input",
            "heading"               => __('Number', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),
        array(
            "param_name"            => "icon",
            "type"                  => "textfield",
            "heading"               => __('Icon (URL or class)', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),
        array(
            "param_name"            => "color_icon",
            "type"                  => "dropdown",
            "heading"               => __('Icon color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors', 'dm'),
        ),
        array(
            "param_name"            => "color_icon_background",
            "type"                  => "dropdown",
            "heading"               => __('Icon background color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors', 'dm'),
        ),
        array(
            "param_name"            => "color_separator",
            "type"                  => "dropdown",
            "heading"               => __('Separator color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors', 'dm'),
        ),
        array(
            "param_name"            => "color_title",
            "type"                  => "dropdown",
            "heading"               => __('Title color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors', 'dm'),
        ),
        array(
            "param_name"            => "color_description",
            "type"                  => "dropdown",
            "heading"               => __('Description color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors', 'dm'),
        ),

        array(
            "param_name"            => "font_title",
            "type"                  => "dropdown",
            "heading"               => __('Title font', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2'),
            "group"                 => __('Fonts', 'dm'),
        ),
        array(
            "param_name"            => "font_description",
            "type"                  => "dropdown",
            "heading"               => __('Description font', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2'),
            "group"                 => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"                      => __('Service Item', 'dm'),
        "base"                      => "dm_service_item",
        "icon"                      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"                  => __('Thema', 'dm'),
        "params"                    => $arrAllParams,
    ));
}