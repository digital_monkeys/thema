<?php
add_action( 'vc_before_init', 'vc_dm_service_item_2' );
function vc_dm_service_item_2()
{
    $arrIndividualParams = array(
        array(
            "param_name"            => "title",
            "type"                  => "textarea",
            "heading"               => __('Title', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),
        array(
            "param_name"            => "description",
            "type"                  => "textarea",
            "heading"               => __('Description', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),

        array(
            "type"                  => "attach_image",
            "holder"                => "div",
            "class"                 => "",
            "heading"               => __('Service Image', 'dm'),
            "param_name"            => "image_id",
            "value"                 => __( "", "" ),
            "description"           => __('Choose an image that shows your service item', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),
        array(
            "param_name"            => "color_title",
            "type"                  => "dropdown",
            "heading"               => __('Title color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors', 'dm'),
        ),
        array(
            "param_name"            => "color_description",
            "type"                  => "dropdown",
            "heading"               => __('Description color', 'dm'),
            "value"                 => dm_get_color_options(),
            "group"                 => __('Colors','dm'),
        ),

        array(
            "param_name"            => "font_title",
            "type"                  => "dropdown",
            "heading"               => __('Title font', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2'),
            "group"                 => __('Fonts', 'dm'),
        ),
        array(
            "param_name"            => "font_description",
            "type"                  => "dropdown",
            "heading"               => __('Description font', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2'),
            "group"                 => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"                      => __('Service Item 2', 'dm'),
        "base"                      => "dm_service_item_2",
        "icon"                      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"                  => __('Thema', 'dm'),
        "params"                    => $arrAllParams,
    ));
}