<?php
add_action( 'vc_before_init', 'vc_dm_static_carousel' );
function vc_dm_static_carousel()
{
    $arrIndividualParams = array(
        array(
            "param_name"            => "auto_play",
            "type"                  => "checkbox",
            "heading"               => __('Auto-play', 'dm'),
            "description"           => __('If auto-play is selected, the slider will advance even without the user\'s
                                           input', 'dm'),
            "group"                 => __('Behaviour', 'dm'),
        ),

        array(
            "param_name"            => "auto_pause",
            "type"                  => "checkbox",
            "heading"               => __('Auto-pause on hover', 'dm'),
            "description"           => __('If auto-pause is selected, the slider will pause every time the user\'s
                                           cursor is hovering over the slider' , 'dm'),
            "group"                 => __('Behaviour', 'dm'),
        ),

        array(
            "param_name"            => "show_arrows",
            "type"                  => "checkbox",
            "heading"               => __('Show arrows', 'dm'),
            "description"           => __('If this option is checked, the slider will have an arrow on the left and
                                           right side for navigation' , 'dm'),
            "group"                 => __('Behaviour', 'dm'),
        ),

        array(
            "param_name"            => "slider_type",
            "type"                  => "dropdown",
            "heading"               => __('When you reach the end:', 'dm'),
            "value"                 => array(
                                            __('Infinite scroll', 'dm') => 'infinite_scroll',
                                            __('Classic', 'dm')         => 'classic',
                                        ),
            "description"           => __('Choose how the carousel should behave when it gets to the end of the element
                                           set.
                                           <br> - "Infinite scroll" means that the carousel will cycle through all the items,
                                            without an end.
                                           <br> - "Classic" means that once the end is reached, the only option is to go the
                                            other way. <b>(This option is not well suited to be used with auto-play,
                                            because the slider will stop when it reaches the end)</b>', 'dm'),
            "group"                 => __('Behaviour', 'dm'),
        ),




        array(
            "param_name"            => "slider_transition_duration",
            "type"                  => "dropdown",
            "heading"               => __('Transition Time (in seconds)', 'dm'),
            "value"                 => array('default', '0.25', '0.5', '0.75', '1', '1.25', '1.5', '1.75', '2', '2.25', '2.5', '2.75', '3',  '3.25', '3.5', '3.75', '4', '4.25', '4.5', '4.75', '5'),
            "description"           => __('How long each transition should last', 'dm'),
            "group"                 => __('Slider Transitions', 'dm'),
        ),

        array(
            "param_name"            => "slider_transition_delay",
            "type"                  => "dropdown",
            "heading"               => __('Transition Interval', 'dm'),
            "value"                 => array('default', '0', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '5.5', '6', '6.5', '7', '7.5', '8', '8.5', '9', '9.5', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'),
            "description"           => __('How much time should pass between two successive item transitions', 'dm'),
            "group"                 => __('Slider Transitions', 'dm'),
        ),

        array(
            "param_name"            => "slider_transition_easing",
            "type"                  => "dropdown",
            "heading"               => __("Transition easing (timing)", ""),
            "value"                 => dm_get_transition_easing_options(),
            "description"           => __("Choose how the transition should look", ""),
            "group"                 => __('Slider Transitions', 'dm'),
        ),

        array(
            "param_name"            => "item_content_size",
            "type"                  => "dropdown",
            "heading"               => __('Item content size', 'dm'),
            "value"                 => array('center', 'stretch'),
            "description"           => __('"Stretch" means that the item content will be stretched to fill its designated space (useful for photo galleries, for example).
                "Center" means the item will not be over-stretched, but it will be centered horizontally (useful for logo sliders, for example).' , 'dm'),
            "group"                 => __('Layout', 'dm'),
        ),


        array(
            "param_name"            => "column_count_large",
            "type"                  => "dropdown",
            "heading"               => __('Columns on a large screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_medium",
            "type"                  => "dropdown",
            "heading"               => __('Columns on a medium screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_small",
            "type"                  => "dropdown",
            "heading"               => __('Columns on a small screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_extra_small",
            "type"                  => "dropdown",
            "heading"               => __('Columns on an extra small screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map( array(
        'name'                      => __( 'Static Carousel' , 'dm' ),
        'base'                      => 'dm_static_carousel',
        'icon'                      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        'description'               => __( 'A content slider that you manually add items to.', 'dm' ),
        'as_parent'                 => array('except' => ''), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
        'content_element'           => true,
        'show_settings_on_create'   => true,
        "params" => $arrAllParams,
        "category" => __("Thema", ""),
        "js_view" => 'VcRowView'
    ) );
}