<?php
add_action( 'vc_before_init', 'vc_dm_static_grid' );
function vc_dm_static_grid()
{
    $arrIndividualParams = array(
        array(
            "param_name"            => "grid_type",
            "type"                  => "dropdown",
            "heading"               => __('Grid Type', 'dm'),
            "value"                 => array('normal', 'masonry'),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_large",
            "type"                  => "dropdown",
            "heading"               => __('Columns on a large screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_medium",
            "type"                  => "dropdown",
            "heading"               => __('Columns on a medium screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_small",
            "type"                  => "dropdown",
            "heading"               => __('Columns on a small screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_count_extra_small",
            "type"                  => "dropdown",
            "heading"               => __('Columns on an extra small screen', 'dm'),
            "value"                 => array(1, 2, 3, 4, 6, 12),
            "group"                 => __('Layout', 'dm'),
        ),

        array(
            "param_name"            => "column_width",
            "type"                  => "textfield",
            "heading"               => __('Column Width - in pixels', 'dm'),
            "value"                 => '',
            "description"           => __('(Optional) Column width for masonry grid (This option overrides everything else.
                If you specify fixed width for the columns, they are not going to be fluid anymore, and
                each item is going to have that exact width.)', 'dm'),
            "group"                 => __('Layout', 'dm'),
            "dependency"            =>
                array(
                    "element" => "grid_type",
                    "value" => "masonry",
                )
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map( array(
        'name'                      => __( 'Static Grid' , 'dm' ),
        'base'                      => 'dm_static_grid',
        'icon'                      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        'description'               => __( 'Here you can put any item that you want managed with a grid', 'dm' ),
        'as_parent'                 => array('except' => ''), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
        'content_element'           => true,
        'show_settings_on_create'   => true,
        "params" => $arrAllParams,
        "category" => __("Thema", ""),
        "js_view" => 'VcRowView'
    ) );
}