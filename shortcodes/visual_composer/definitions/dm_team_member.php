<?php

add_action( 'vc_before_init', 'vc_dm_team_member' );
function vc_dm_team_member()
{

    $arrIndividualParams = array(
        array(
            "type"              => "attach_image",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Image', 'dm'),
            "param_name"        => "image_id",
            "value"             => __("", ""),
            "description"       => __('(Optional) This is where you add your own image.', 'dm')
        ),

        array(
            "type"              => "textfield",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Title text', 'dm'),
            "param_name"        => "title",
            "value"             => __("", ""),
            "description"       => __('This is where you add your own title.', 'dm')
        ),

        array(
            "type"              => "textfield",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Subtitle text', 'dm'),
            "param_name"        => "subtitle",
            "value"             => __("", ""),
            "description"       => __('This is where you add your own subtitle.', 'dm')
        ),

        array(
            "param_name"        => "color_title",
            "type"              => "dropdown",
            "heading"           => __('Title color', 'dm'),
            "value"             => dm_get_color_options(),
        ),

        array(
            "param_name"        => "color_subtitle",
            "type"              => "dropdown",
            "heading"           => __('Subtitle color', 'dm'),
            "value"             => dm_get_color_options(),
        ),

        array(
            "param_name"        => "font_title",
            "type"              => "dropdown",
            "heading"           => __('Title font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2')
        ),

        array(
            "param_name"        => "font_subtitle",
            "type"              => "dropdown",
            "heading"           => __('Title font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2')
        ),
    );


    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);


    vc_map( array(
        "name"                  => __('Title team member', 'dm'),
        "base"                  => "dm_team_member",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ) );
}


