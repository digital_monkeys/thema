<?php
add_action( 'vc_before_init', 'vc_dm_testimonial' );
function vc_dm_testimonial()
{

    $arrIndividualParams = array(
        array(
            "param_name"            => "title",
            "type"                  => "textfield",
            "heading"               => __('Quote title', 'dm'),
        ),
        array(
            "param_name"            => "body",
            "type"                  => "textarea",
            "heading"               => __('Quote body', 'dm'),
        ),
        array(
            "param_name"            => "image_id",
            "type"                  => "attach_image",
            "heading"               => __('Client image', 'dm'),
        ),
        array(
            "param_name"            => "color_image_stroke",
            "type"                  => "dropdown",
            "heading"               => __('Image stroke color', 'dm'),
            "value"                 => dm_get_color_options(),
        ),
        array(
            "param_name"            => "font_title",
            "type"                  => "dropdown",
            "heading"               => __('Quote title font', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2')
        ),
        array(
            "param_name"            => "color_title",
            "type"                  => "dropdown",
            "heading"               => __('Quote title color', 'dm'),
            "value"                 => dm_get_color_options(),
        ),
        array(
            "param_name"            => "font_body",
            "type"                  => "dropdown",
            "heading"               => __('Quote body font', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2')
        ),
        array(
            "param_name"            => "color_body",
            "type"                  => "dropdown",
            "heading"               => __('Quote body color', 'dm'),
            "value"                 => dm_get_color_options(),
        ),
        array(
            "param_name"            => "color_quote_marks",
            "type"                  => "dropdown",
            "heading"               => __('Quote marks color', 'dm'),
            "value"                 => dm_get_color_options(),
        ),

    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);



    vc_map(array(
        "name"                     => __('Testimonial', 'dm'),
        "base"                     => "dm_testimonial",
        "icon"                     => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"                 => __('Thema', 'dm'),
        "params"                   => $arrAllParams
    ));
}