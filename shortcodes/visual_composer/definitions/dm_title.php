<?php
add_action( 'vc_before_init', 'vc_dm_title' );
function vc_dm_title()
{
    $arrIndividualParams = array(
        array(
            "param_name"            => "text",
            "type"                  => "textfield",
            "heading"               => __('Title content', 'dm'),
            "group"                 => __('Content', 'dm'),
        ),
        array(
            "param_name"            => "color",
            "type"                  => "dropdown",
            "heading"               => __('Color', 'dm'),
            "value"                 => array('Theme color 1', 'Theme color 2', 'Gray Light', 'Gray Dark', 'White', 'Black'),
            "group"                 => __('Colors', 'dm'),
        ),
        array(
            "param_name"            => "font",
            "type"                  => "dropdown",
            "heading"               => __('Color', 'dm'),
            "value"                 => array('Theme font 1', 'Theme font 2'),
            "group"                 => __('Fonts', 'dm'),
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name"                      => __("Title", ""),
        "base"                      => "dm_title",
        "icon"                      => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"                  => __('Thema', 'dm'),
        "params"                    => $arrAllParams,
    ));
}