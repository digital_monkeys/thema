<?php
add_action( 'vc_before_init', 'vc_dm_title_band' );
function vc_dm_title_band()
{
    $arrIndividualParams = array(
        array(
            "param_name" => "title",
            "type" => "textfield",
            "heading" => __("Title", ""),

        ),
        array(
            "param_name" => "subtitle",
            "type" => "textfield",
            "heading" => __("Subtitle", ""),

        ),
        array(
            "param_name" => "icon",
            "type" => "textfield",
            "heading" => __("Icon (class or URL)", ""),
        ),
        array(
            "param_name" => "background_color",
            "type" => "dropdown",
            "heading" => __("Background color", ""),
            "value" => dm_get_color_options(),
        ),
        array(
            "param_name" => "font_title",
            "type" => "dropdown",
            "heading" => __("Title font", ""),
            "value" => array('Theme font 1', 'Theme font 2')
        ),
        array(
            "param_name" => "font_subtitle",
            "type" => "dropdown",
            "heading" => __("Subtitle font", ""),
            "value" => array('Theme font 1', 'Theme font 2')
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);

    vc_map(array(
        "name" => __("Title Band", ""),
        "base" => "dm_title_band",
        "icon" => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category" => __("Thema", ""),
        "params" => $arrAllParams,
    ));
}