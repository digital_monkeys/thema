<?php

add_action( 'vc_before_init', 'vc_dm_title_band_big_text' );
function vc_dm_title_band_big_text() {

    $arrIndividualParams = array(
        array(
            "type"              => "attach_image",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Image', 'dm'),
            "param_name"        => "image_id",
            "value"             => __( "", "" ),
            "description"       => __('(Optional) This is where you add your own image.', 'dm')
        ),

        array(
            "type"              => "textfield",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Title text', 'dm'),
            "param_name"        => "title",
            "value"             => __( "", "" ),
            "description"       => __('This is where you add your own title.', 'dm')
        ),

        array(
            "type"              => "textfield",
            "holder"            => "div",
            "class"             => "",
            "heading"           => __('Subtitle text', 'dm'),
            "param_name"        => "subtitle",
            "value"             => __( "", "" ),
            "description"       => __('(Optional) This is where you add your own subtitle.', 'dm')
        ),
        array(
            "param_name"        => "background_color",
            "type"              => "dropdown",
            "heading"           => __('Background color', 'dm'),
            "value"             => dm_get_color_options(),
        ),
        array(
            "param_name"        => "title_color",
            "type"              => "dropdown",
            "heading"           => __('Title color', 'dm'),
            "value"             => dm_get_color_options(),
        ),
        array(
            "param_name"        => "subtitle_color",
            "type"              => "dropdown",
            "heading"           => __('Subtitle color', 'dm'),
            "value"             => dm_get_color_options(),
        ),
        array(
            "param_name"        => "font_title",
            "type"              => "dropdown",
            "heading"           => __('Title font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2')
        ),
        array(
            "param_name"        => "font_subtitle",
            "type"              => "dropdown",
            "heading"           => __('Subtitle font', 'dm'),
            "value"             => array('Theme font 1', 'Theme font 2')
        ),
    );

    $arrAllParams = dm_get_vc_full_params($arrIndividualParams);


    vc_map( array(
        "name"                  => __('Title band big text', 'dm'),
        "base"                  => "dm_title_band_big_text",
        "icon"                  => get_template_directory_uri().'/dm_core/graphics/vc_shortcode_icon.png',
        "category"              => __('Thema', 'dm'),
        "params"                => $arrAllParams,
    ) );
}