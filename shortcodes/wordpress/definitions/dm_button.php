<?php
function dm_button($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'label' => 'Choose label',
        'icon' => 'fa fa-bolt',

        'color_icon'                => 'Theme color 1',
        'color_icon_background'     => 'Theme color 1',
        'color_label'               => 'Theme color 1',
        'color_label_background'    => 'Theme color 1',

        'font_label'                => 'Theme font 1',
        'rounded_corners'           => '',
    ), $atts));

    $color_index_icon               = dm_get_color_index($color_icon);
    $color_index_icon_background    = dm_get_color_index($color_icon_background);
    $color_index_label              = dm_get_color_index($color_label);
    $color_index_label_background   = dm_get_color_index($color_label_background);

    $font_index_label               = dm_get_font_index($font_label);

    $return_string = '';
    $return_string .=
        "
         <div class='btn-1 $dynamicClass' $animationData id='$dynamicId'>
            <span class='$icon dm-icon font-color$color_index_icon bg-color$color_index_icon_background'></span>
            <h4 class='dm-label font-color$color_index_label bg-color$color_index_label_background font$font_index_label'> $label </h4>
            <div class='arrow-right border-left-color$color_index_icon_background'></div>
        </div>
        ";

    return $return_string;
}