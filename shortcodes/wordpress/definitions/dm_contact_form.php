<?php
function dm_contact_form($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'type' => '1',
    ), $atts));

    $return_string = '';
    $return_string .=

        "
         <form class='contact-form $dynamicClass' $animationData id='$dynamicId'>
            <div class='col-sm-5'>
                <p> Your name: </p>
                <input type='text' name='name' placeholder='Type your name here...'>
                <p> Your e-mail: </p>
                <input  type='email'name='email' placeholder='Type your email here...'>
                <p> Subject: </p>
                <input  type='text' name='subject' placeholder='Type the subject of your message...'>
            </div>
            <div class='col-sm-7'>
                <p> Message: </p>
                <textarea>

                </textarea>
                <div class='btn-action'>

                    <span> Submit!</span>

                    <div class='iconContainer'>
                        <i class='fa fa-envelope'></i>
                        <div class='btn-arrow-right'></div>
                    </div>
                </div>
            </div>
        </form>
        ";

    return $return_string;
}