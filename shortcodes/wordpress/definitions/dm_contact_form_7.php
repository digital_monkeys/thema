<?php
function dm_contact_form_7($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'form' => '',
        'skin' => '1',
        'color' => 'Theme color 1',
    ), $atts));

    $arrFormAtts = explode('-', $form);

    $formID = '';
    $formName = '';
    if(strlen($form) > 0) {
        $formID = $arrFormAtts[0];
        $formTitle = $arrFormAtts[1];
    }

    $return_string =
        "
            <div class='contact-form-skin-$skin'>
            ".do_shortcode("[contact-form-7 id='$formID' title='$formTitle']")."
            </div>
        ";

    return $return_string;
}