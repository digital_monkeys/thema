<?php

function dm_content_with_brackets($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'line_1_content' => '',
        'line_2_content' => '',
        'line_3_content' => '',
        'line_4_content' => '',
        'middle_content' => '',
        'right_content' => '',
        'css' => '',

    ), $atts));



    $return_string =
        "
        <section class='row dm-content-with-brackets-1 $dynamicClass' $animationData id='$dynamicId'>
            <div class='boxed-layout'>
               <div class='vc_col-sm-5'>
                    <div class='dm-column-1'>
                        <h3 class='dm-content''>
                            <span> $line_1_content </span>
                        </h3>
                       <h3 class='dm-content'>
                            <span> $line_2_content </span>

                        </h3>
                       <h3 class='dm-content'>
                            <span> $line_3_content </span>

                        </h3>
                       <h3 class='dm-content'>
                            <span> $line_4_content </span>

                        </h3>
                       <span class='dm-bracket'> } </span>
                   </div>
                </div>
                <div class='vc_col-sm-2'>
                    <div class='dm-column-2'>
                        <h2 class='dm-content'> $middle_content </h2>
                    </div>
                </div>
                <div class='vc_col-sm-5'>
                    <div class='dm-column-3'>
                        <span class='dm-bracket'> { </span>
                        <h4 class='dm-content'> $right_content </h4>
                    </div>
                </div>
            </div>
        </section>
        ";

    return $return_string;
}