<?php
function dm_feature_item($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title' => 'The Title',
        'description' => '',
        'icon' => '',
        'color' => '1',
    ), $atts));

    $color_index = dm_get_color_index($color);

    $return_string = '';
    /*
    $return_string .=
        "
        <div class='feature_item_1 $dynamicClass' $animationData>
            <div class='icon_container_hexagon' style='background-image: url(".dm_get_svg_url($color_index, 'hexagon').");'>
                <i class='$icon'></i>
            </div>
            <h2 class='title font-color".$color_index."Dark'> $title</h2>
            <p class='description font-color$color_index'> $description </p>
        </div>
        ";
    */
    $return_string .=
        "
        <div class='feature-item-1 $dynamicClass' $animationData>
            <div class='icon-container-hexagon'>
                <object data=".dm_get_svg_url($color_index, 'hexagon')." type='image/svg+xml'></object>
                <i class='$icon'></i>
            </div>
            <h2 class='title font-color".$color_index."Dark'> $title</h2>
            <p class='description font-color$color_index'> $description </p>
        </div>
        ";

    return $return_string;
}