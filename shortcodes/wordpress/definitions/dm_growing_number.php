<?php
function dm_growing_number($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'icon'                          => 'fa fa-gear',
        'number'                        => '0',
        'title'                         => '',

        'color_icon'                    => 'Theme color 1',
        'color_title'                   => 'Theme color 1',
        'font_title'                    => 'Theme font 1',

    ), $atts));

    $color_index_icon                   = dm_get_color_index($color_icon);
    $color_index_title                  = dm_get_color_index($color_title);
    $font_index_title                   = dm_get_font_index($font_title);

    $animationData .= " data-end-value = '$number'";

    $return_string = "
        <div class='dm-growing-number $dynamicClass' $animationData id='$dynamicId'>

            <i class='$icon font-color$color_index_icon'></i>
            <h1 class='dm-number font-color$color_index_title font$font_index_title'>$number</h1>
            <p class='dm-title font-color$color_index_title font$font_index_title'> $title </p>
        </div>
    ";



    return $return_string;
}