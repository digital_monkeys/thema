<?php
function dm_hero($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title' => 'The Title',
        'subtitle' => 'The Subtitle',
        'title_color' => '',
        'subtitle_color' => '',
        'title_font' => '',
        'subtitle_font' => '',
        'title_specific_color' => '',
        'subtitle_specific_color' => '',
        'title_background_color' => '1',
        'subtitle_background_color' => '1',
        'show_title_background' => '',
        'show_subtitle_background' => '',
        'image_id' => '',
        'skin' => '1',
        'is_full_screen' => 'false',
        'header_style' => "Default",
    ), $atts));

    if('true' == $is_full_screen)
    {
        $animationData .= " data-is-fullscreen='true'";
    }
    else
    {
        $animationData .= " data-is-fullscreen='false'";
    }

    $animationData .= " data-header-style='".strtolower($header_style)."'";

    $imagePath = '';
    if(0 != strlen($image_id))
    {
        $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];
    }

    $title_color_style = '';
    $class_color_title = '';
    if(strlen((string)$title_specific_color) > 0)
    {
        $title_color_style = 'color: '.$title_specific_color.'!important';
    }
    else
    {
        $class_color_title = 'font-color'.dm_get_color_index($title_color);
    }

    $subtitle_color_style = '';
    $class_color_subtitle = '';
    if(strlen((string)$subtitle_specific_color) > 0)
    {
        $subtitle_color_style = 'color: '.$subtitle_specific_color.'!important';
    }
    else
    {
        $class_color_subtitle = 'font-color'.dm_get_color_index($subtitle_color);
    }


    $title_background_color_class = '';
    if(strlen((string)$show_title_background) > 0)
    {
        $title_background_color_class = 'bg-color'.dm_get_color_index($title_background_color).'Fade !important';
    }

    $subtitle_background_color_class = '';
    if(strlen((string)$show_subtitle_background) > 0)
    {
        $subtitle_background_color_class = 'bg-color'.dm_get_color_index($subtitle_background_color).'Fade !important';
    }

    $font_index_title = dm_get_font_index($title_font);
    $font_index_subtitle = dm_get_font_index($subtitle_font);

    $return_string = '';
    $return_string .=
        "
         <div class='row'>
            <div class='dm-hero dm-hero-$skin $dynamicClass' $animationData style='background-image: url(\"$imagePath\"); background-size: cover;' id='$dynamicId'>
                <div class='boxed-layout'>";
    //if($skin == 1)
    //{
        $return_string .= "<h1 style='$title_color_style' class='dm-title font$font_index_title $class_color_title $title_background_color_class'> $title </h1>";
        $return_string .= "<h1 style='$title_color_style' class='dm-title font$font_index_title $class_color_title $title_background_color_class'> $title </h1>";

    //if(strlen($subtitle) != 0)
        //{
            //$return_string .= "<br> <h4 class='dm_subtitle $class_color_subtitle font$font_index_subtitle $subtitle_background_color_class' style='$subtitle_color_style'> $subtitle </h4>";
        //}
    //}
    /*elseif($skin == 2)
    {

    }*/


    $return_string .= "
                </div>
            </div>
        </div>
        ";

    return $return_string;

}
