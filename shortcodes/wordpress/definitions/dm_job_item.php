<?php
function dm_job_item($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title' => 'The Title',
        'experience' => '',
        'skills' => '',
        'job_description' => '',
        'perks' => '',
    ), $atts));

    $return_string = '';
    $return_string .=
        "
         <div class='job-unit-1 $dynamicClass' $animationData id='$dynamicId'>
            <div class='content'>
                <h2 class='heading'> $title </h2>

                <h4 class='subheading'> Experience: </h4>
                <p> $experience </p>

                <h4 class='subheading'> What you need to know: </h4>
                <p> $skills </p>

                <h4 class='subheading'> What you'll do: </h4>
                <p> $job_description </p>

                <h4 class='subheading'> What we offer: </h4>
                <p> $perks </p>
            </div>
            <div class='btn-action'>
                <i class='fa fa-flash'></i>
                <h4> Apply Now! </h4>
            </div>
        </div>
        ";

    return $return_string;
}