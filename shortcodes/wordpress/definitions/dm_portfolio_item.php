<?php
function dm_portfolio_item($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title'                     => 'Choose title',
        'client'                    => '',
        'date'                      => '',
        'duration'                  => '',
        'budget'                    => '',
        'description'               => '',
        'image_id'                  => '',
        'color_separator'           => 'Theme color 1',
        'color_title'               => 'Theme color 1',
        'color_highlight'           => 'Theme color 1',
        'color_normal_text'         => 'Theme color 1',
        'color_description'         => 'Theme color 1',
        'font_title'                => 'Theme font 1',
        'font_description'          => 'Theme font 1',
    ), $atts));

    $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];

    $color_index_separator          = dm_get_color_index($color_separator);
    $color_index_title              = dm_get_color_index($color_title);
    $color_index_description        = dm_get_color_index($color_description);
    $color_index_highlight          = dm_get_color_index($color_highlight);
    $color_index_normal_text          = dm_get_color_index($color_normal_text);

    $font_index_title               = dm_get_font_index($font_title);
    $font_index_description         = dm_get_font_index($font_description);


    $return_string = "
        <div class='row dm-portfolio-item $dynamicClass' $animationData id='$dynamicId'>
            <div class='col-sm-4'>
                <img class='dm-image' src='$imagePath' alt='service image'>
            </div>
            <div class='col-sm-8'>
                <div class='dm-separator'></div>
                <div class='dm-content-container'>

                    <h2 class='dm-title font-color$color_index_title font$font_index_title'> $title </h2>
                    <p>
                        <span class='dm-highlight font-color$color_index_highlight font$font_index_description'>
                            Client
                        </span>
                        <span class='dm-normal-text'>$client</span>
                    </p>
                    <p>
                        <span class='dm-highlight font-color$color_index_highlight font$font_index_description'>
                            Date:
                        </span>
                        $date
                    </p>
                    <p>
                        <span class='dm-highlight font-color$color_index_highlight font$font_index_description'>
                            Duration:
                        </span>
                        <span class='dm-normal-text'>$duration</span>
                    </p>
                    <p>
                        <span class='dm-highlight font-color$color_index_highlight font$font_index_description'>
                            Budget:
                        </span>
                        <span class='dm-normal-text'>$budget</span>
                    </p>
                        <span class='dm-highlight font-color$color_index_highlight font$font_index_description'>
                            Description:
                        </span>
                        <span class='dm-normal-text'>$description</span>
                    </p>
                    <div class='dm-btn-action font-color$color_index_highlight font$font_index_description'>
                        Read More
                    </div>
                </div>
            </div>
        </div>
    ";

    return $return_string;
}