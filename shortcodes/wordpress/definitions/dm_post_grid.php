<?php
function dm_post_grid($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'post_type' => 'post',
        'column_count_large' => '1',
        'column_count_medium' => '1',
        'column_count_small' => '1',
        'column_count_extra_small' => '1',
        'column_width' => '',
        'grid_type' => 'normal',
        'post_count' => 'all',
        'post_template' => 'post-skin1',
        'display_filters' => '',
        'filter_by' => 'tags',
        'filter_skin' => '1'
    ), $atts));


    $columnSizeLarge = 12 / $column_count_large;
    $columnSizeMedium = 12 / $column_count_medium;
    $columnSizeSmall = 12 / $column_count_small;
    $columnSizeExtraSmall = 12 / $column_count_extra_small;

    $gridItemStyle = '';

    $gridItemClass = "class='dm-grid-item col-lg-$columnSizeLarge col-md-$columnSizeMedium col-sm-$columnSizeSmall col-xs-$columnSizeExtraSmall''";
    $masonryClass = '';
    if($grid_type == 'normal')
    {
        $dynamicClass .= "";
    }
    elseif($grid_type == 'masonry')
    {
        if(strlen($column_width) > 0 && (int)$column_width > 0)
        {
            $gridItemStyle = "style='max-width:100%; width: $column_width"."px'";
            $gridItemClass = "class='dm-grid-item'";
        }
        wp_enqueue_script( 'masonry_grid' );
        wp_enqueue_script( 'masonry_use' );
        $masonryClass .= " dm-masonry";
        $animationData .= "data-column-width='$column_width";
    }


    ob_start();

    $arrPostTemplate = explode('-', $post_template);
    $template_slug =  'partials/'.$arrPostTemplate[0];
    $template_name =  $arrPostTemplate[1];

    echo "<div class='row $dynamicClass dm-post-grid' $animationData id='$dynamicId' >";
    // WP_Query arguments
    $args = array(
        'post_type' => "$post_type",
        //'posts_per_page' => '3',
    );
    // Filters BEGIN

    if(strlen($display_filters) > 0) {
        if($filter_by == 'tags') {
            $filter_by = 'post_tag';
        } else if($filter_by == 'categories') {
            $filter_by = 'category';
        }

        $arrFilter = array();

        // The Query for filters
        $dm_query = new WP_Query($args);

        if ($dm_query->have_posts()) {
            while ($dm_query->have_posts()) {
                $dm_query->the_post();

                $postID = get_the_ID();

                //$terms = wp_get_post_terms($postID, 'post_tag');
                $terms = wp_get_post_terms($postID, $filter_by);
                foreach ($terms as $term) {
                    if (!array_key_exists($term->slug, $arrFilter)) {
                        $arrFilter[$term->slug] = $term->slug;
                    }
                }
            }
        }


        echo "<ul class='dm-grid-filters dm-skin-$filter_skin'>";
        echo '<li class="dm-filter-btn">all</li>';
        foreach ($arrFilter as $filter) {
            echo '<li class="dm-filter-btn">' . $filter . '</li>';
        }

        echo '</ul>';
    }
    // Filters END




    echo "<div class='dm-post-grid-inner $masonryClass'>";
    // The Query
    $dm_query = new WP_Query($args);

    if($dm_query->have_posts())
    {
        while($dm_query->have_posts()) {
            $dm_query->the_post();

            $postID = get_the_ID();

            /*
            $arrTags = wp_get_post_tags($postID);
            $strTags = 'all ';
            foreach($arrTags as $tag) {
                $strTags .= $tag->slug.' ';
            }
            */

            $strTags = 'all ';
            $terms = wp_get_post_terms($postID, $filter_by);
            foreach ($terms as $term) {
                $strTags .= $term->slug;
            }

            echo "<div $gridItemClass $gridItemStyle data-tags='$strTags'>";

            get_template_part($template_slug, $template_name);

            echo "</div>";
        }
    }
    else
    {
        echo "<h3> Sorry, no posts to display. </h3>";
    }

    echo "</div>";
    echo "</div>";

    //wp_reset_query();

    return ob_get_clean();
}