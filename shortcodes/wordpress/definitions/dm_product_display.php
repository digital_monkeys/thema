<?php
function dm_product_display($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title' => 'The Title',
        'tagline' => '',
        'image_id' => '',
        'image_url' => ''
    ), $atts));

    if(strlen($image_id) != 0)
    {
        $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];
    }
    else
    {
        $imagePath = $image_url;
    }


    $return_string = '';
    $return_string .=
        "
         <div class='row $dynamicClass' $animationData id='$dynamicId'>
            <section class='product-display'>
                <div class='boxed-layout'>
                    <div class='col-sm-6'>
                        <h1> $title </h1>
                        ";

    if(strlen($tagline) > 0)
    {
        $return_string .= "<h3> $tagline </h3>";
    }

    $return_string .=
                    "</div>
                    <div class='col-sm-6'>
                        <img class='img-responsive' src='$imagePath'>
                    </div>
                </div>
            </section>
        </div>
        ";

    return $return_string;
}