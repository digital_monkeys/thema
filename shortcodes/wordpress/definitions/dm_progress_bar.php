<?php
function dm_progress_bar($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'skin'                      => '1',
        'label'                     => '',
        'current_value'             => '0',
        'total_value'               => '100',
        'color_label'               => 'Theme color 1',
        'color_current_value'       => 'Theme color 1',
        'color_total_value'         => 'Theme color 1',
        'font_label'                => 'Theme font 1',
        //'font_description'          => 'Theme font 1',
    ), $atts));

    //$imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];


    $color_index_label             = dm_get_color_index($color_label);
    //$color_index_description        = dm_get_color_index($color_description);

    $font_index_label               = dm_get_font_index($font_label);
    //$font_index_description         = dm_get_font_index($font_description);

    $crtProgress = min(($current_value / $total_value * 100), 100);

    $animationData .= " data-end-value = '$crtProgress'";

    if($skin == '1') {
        $return_string = "
            <div class='dm-progress-bar $dynamicClass' $animationData id='$dynamicId'>

                <h3 class='dm-title font-color$color_index_label font$font_index_label'> $label </h3>
                <div class='dm-progress bg-color-1-light'>
                    <div data-current-value='$crtProgress' class='dm-current-progress bg-color-1-dark' style='width: $crtProgress%;'></div>

                </div>
            </div>
        ";
    }elseif($skin == 2) {
        $return_string = "
            <div class='row dm-progress-bar $dynamicClass' $animationData id='$dynamicId'>
                <div class='col-sm-4'>
                    <h4 class='dm-title font-color$color_index_label font$font_index_label'> $label </h4>
                </div>
                <div class='col-sm-8'>
                    <div class='dm-progress bg-color-1-Light'>
                        <div class='dm-current-progress bg-color-1-dark' style='width: $crtProgress%;'></div>
                    </div>
                </div>
            </div>


            <div class='dm-circle'> <h1 class='dm-test-number test-number'> 99 </h1></div>
        ";
    }

    return $return_string;
}