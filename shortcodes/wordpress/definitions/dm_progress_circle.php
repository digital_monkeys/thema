<?php
function dm_progress_circle($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'progress'                      => '0',
        'total'                         => '1',
        'label'                         => '',

        'color_label'                   => 'Theme color 1',
        'font_label'                    => 'Theme font 1',

        'color_number'                   => 'Theme color 1',
        'font_number'                    => 'Theme font 1',

    ), $atts));

    $color_index_label                  = dm_get_color_index($color_label);
    $font_index_label                   = dm_get_font_index($font_label);

    $color_index_number                 = dm_get_color_index($color_number);
    $font_index_number                  = dm_get_font_index($font_number) ;

    $animationData .= " data-end-value = '$progress' data-total-value='$total";

    $return_string = "
        <div class='dm_circle $dynamicClass' $animationData id='$dynamicId' data-end-value='70' data-total-value='150' style='display: inline-block'>
            <h2 class='dm_number font-color$color_index_number  font$font_index_number '> $progress </h2>
            <h4 class='dm_label font-color$color_index_label font$font_index_label'> $label </h4>
        </div>

    ";

    return $return_string;
}