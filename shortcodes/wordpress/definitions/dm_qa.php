<?php
function dm_qa($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'icon'                      => 'fa fa-question',
        'question'                  => 'Question here',
        'answer'                    => 'Answer here',


        'color_icon'                => 'Theme color 1',
        'color_question'            => 'Theme color 1',
        'color_answer'              => 'Theme color 1',

        'font_icon'                 => 'Theme font 1',
        'font_question'             => 'Theme font 1',
        'font_answer'               => 'Theme font 1',
    ), $atts));



    $color_index_icon               = dm_get_color_index($color_icon);
    $color_index_question           = dm_get_color_index($color_question);
    $color_index_answer             = dm_get_color_index($color_answer);



    $font_index_question            = dm_get_font_index($font_question);
    $font_index_answer            = dm_get_font_index($font_answer);



    $return_string = "
        <div class='dm-qa $dynamicClass' $animationData id='$dynamicId'>
           <i class='$icon font-color$color_index_icon'></i>
           <h4 class='dm-question font-color$color_index_question font$font_index_question'> $question</h4>
           <h4 class='dm-answer font-color$color_index_answer font$font_index_answer'> $answer</h4>
        </div>
    ";

    return $return_string;
}