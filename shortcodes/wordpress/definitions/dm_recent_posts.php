<?php
function dm_recent_posts($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'type' => '1',
        'columns' => '3',
        'count' => '3',
    ), $atts));


    $columnSize = 12 / $columns;


    $return_string = '';
    $return_string .=
        ' <div class="row">';

        // WP_Query arguments
        $args = array (
            'post_type' => 'post',
            'posts_per_page' => $count,

        );

        // The Query
        $dm_query = new WP_Query($args);

    if($dm_query->have_posts())
    {
        while($dm_query->have_posts())
        {
            $dm_query-> the_post();
            $return_string .=
                "
                <div class='col-md-$columnSize dm-post-1 $dynamicClass' $animationData id='$dynamicId'>
                    <p class='date'> ".get_the_date()." </p>
                    <h4 class='title'> ".get_the_title()." </h4>
                    <img class='separator img-responsive' src=".dm_get_svg_url(1, 'separator_1_horizontal').">
                    <p class='content'>
                        ".get_the_excerpt()."
                    </p>
                    <div class='separator2'></div>
                </div>
                ";

        }
    }
    else
    {
        $return_string .= "<h3> Sorry, no posts to display. </h3>";
    }


    $return_string .= "</div>";
    wp_reset_query();

    return $return_string;

}