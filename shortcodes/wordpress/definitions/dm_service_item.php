<?php
function dm_service_item($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title'                     => 'Choose title',
        'description'               => '',
        'icon'                      => 'fa fa-gear',

        'color_icon'                => 'Theme color 1',
        'color_icon_background'     => 'Theme color 1',
        'color_separator'           => 'Theme color 1',
        'color_title'               => 'Theme color 1',
        'color_description'         => 'Theme color 1',

        'font_title'                => 'Theme font 1',
        'font_description'          => 'Theme font 1',
    ), $atts));

    $color_index_icon               = dm_get_color_index($color_icon);
    $color_index_icon_background    = dm_get_color_index($color_icon_background);
    $color_index_separator          = dm_get_color_index($color_separator);
    $color_index_title              = dm_get_color_index($color_title);
    $color_index_description        = dm_get_color_index($color_description);

    $font_index_title               = dm_get_font_index($font_title);
    $font_index_description         = dm_get_font_index($font_description);


    $return_string = "
        <div class='row dm-service-item-1 $dynamicClass' $animationData id='$dynamicId'>
            <div class='col-lg-5 col-md-6 dm-icon-container'>
                <img class='dm-icon-background' src=".dm_get_svg_url($color_index_icon_background, 'hexagon')." alt='icon background'>
                <i class='dm-icon $icon font-color$color_index_icon'></i>

                <img class='dm-separator' src=".dm_get_svg_url($color_index_separator, 'separator_1')." alt='separator'>

            </div>

            <div class='col-lg-9 col-md-8 dm-content-container'>
                <h3 class='dm-title font-color$color_index_title font$font_index_title'> $title </h3>
                <p class='dm-description font-color$color_index_description font$font_index_description'> $description </p>
            </div>
        </div>
    ";

    return $return_string;
}