<?php
function dm_service_item_2($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title'                     => 'Choose title',
        'description'               => '',
        'image_id'                  => '',
        'color_title'               => 'Theme color 1',
        'color_description'         => 'Theme color 1',
        'font_title'                => 'Theme font 1',
        'font_description'          => 'Theme font 1',
    ), $atts));

    $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];


    $color_index_title              = dm_get_color_index($color_title);
    $color_index_description        = dm_get_color_index($color_description);

    $font_index_title               = dm_get_font_index($font_title);
    $font_index_description         = dm_get_font_index($font_description);


    $return_string = "
        <div class='dm-service-item-2 $dynamicClass' $animationData id='$dynamicId'>

                <img class='dm-image' src='$imagePath' alt='service image'>
                <div class='dm-content-container'>
                    <h2 class='dm-title font-color$color_index_title font$font_index_title'> $title </h2>
                    <p class='dm-description font-color$color_index_description font$font_index_description'> $description </p>
                </div>
        </div>
    ";

    return $return_string;
}