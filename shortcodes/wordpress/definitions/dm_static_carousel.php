<?php
function dm_static_carousel($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'slider_transition_duration'=> '0.7',
        'slider_transition_delay'   => '4',
        'slider_transition_easing'  => 'Power0.easeIn',
        'auto_play'                 => 'false',
        'auto_pause'                => 'false',
        'item_content_size'         => 'center',
        'show_arrows'               => 'false',
        'slider_type'               => 'infinite_scroll',

        'column_count_large'        => '1',
        'column_count_medium'       => '1',
        'column_count_small'        => '1',
        'column_count_extra_small'  => '1',
        'column_width'              => '',
        'grid_type'                 => 'normal',
    ), $atts));

    $strShortcodeList = str_replace('][/', ']->[/', $content);
    $strShortcodeList = str_replace('][', ']-|-[', $strShortcodeList);
    $strShortcodeList = str_replace(']->[/', '][/', $strShortcodeList);
    $arrShortcodeList = explode('-|-', $strShortcodeList);

    $columnSizeLarge = 12 / $column_count_large;
    $columnSizeMedium = 12 / $column_count_medium;
    $columnSizeSmall = 12 / $column_count_small;
    $columnSizeExtraSmall = 12 / $column_count_extra_small;

    $gridItemStyle = '';

    $sliderItemClass = "
        class=
        '
            dm-slider-item
            col-lg-$columnSizeLarge
            col-md-$columnSizeMedium
            col-sm-$columnSizeSmall
            col-xs-$columnSizeExtraSmall
        '
    ";


    $sliderItemData = "
        data-col-lg='$columnSizeLarge'
        data-col-md='$columnSizeMedium'
        data-col-sm='$columnSizeSmall'
        data-col-xs='$columnSizeExtraSmall'
        data-transition-duration='$slider_transition_duration'
        data-transition-delay='$slider_transition_delay'
        data-time-left='$slider_transition_delay'
        data-auto-play='$auto_play'
        data-auto-pause='$auto_pause'
        data-item-content-size='$item_content_size'
        data-show-arrows='$show_arrows'
        data-transition-easing='$slider_transition_easing'
        data-slider-type='$slider_type'
        data-is-hover='false'
    ";

    ob_start();

    echo "<div class='dm-container row dm-slider $dynamicClass ' $animationData $sliderItemData id='$dynamicId'>";
    echo "    <i class='fa fa-chevron-left dm-nav-button dm-nav-button-left'></i>";
    echo "    <div class='dm-slider-inner'>";
    foreach($arrShortcodeList as $shortcode)
    {
        echo "<div $sliderItemClass>";
        echo do_shortcode($shortcode);
        echo "</div>";
    }
    echo "    </div>";
    echo "    <i class='fa fa-chevron-right dm-nav-button dm-nav-button-right'></i>";

    echo "</div> ";

    $return_string = ob_get_clean();

    return $return_string;


}