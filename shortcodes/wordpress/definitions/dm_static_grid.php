<?php
function dm_static_grid($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'column_count_large' => '1',
        'column_count_medium' => '1',
        'column_count_small' => '1',
        'column_count_extra_small' => '1',
        'column_width' => '',
        'grid_type' => 'normal',
    ), $atts));

    $strShortcodeList = str_replace('][/', ']->[/', $content);
    $strShortcodeList = str_replace('][', ']-|-[', $strShortcodeList);
    $strShortcodeList = str_replace(']->[/', '][/', $strShortcodeList);
    $arrShortcodeList = explode('-|-', $strShortcodeList);

    $columnSizeLarge = 12 / $column_count_large;
    $columnSizeMedium = 12 / $column_count_medium;
    $columnSizeSmall = 12 / $column_count_small;
    $columnSizeExtraSmall = 12 / $column_count_extra_small;

    $gridItemStyle = '';

    $gridItemClass = "class='dm-grid-item col-lg-$columnSizeLarge col-md-$columnSizeMedium col-sm-$columnSizeSmall col-xs-$columnSizeExtraSmall''";

    if($grid_type == 'normal')
    {
        $dynamicClass .= "";
    }
    elseif($grid_type == 'masonry')
    {
        if(strlen($column_width) > 0 && (int)$column_width > 0)
        {
            $gridItemStyle = "style='max-width:100%; width: $column_width"."px'";
            $gridItemClass = "class='dm-grid-item'";
        }
        wp_enqueue_script( 'masonry_grid' );
        wp_enqueue_script( 'masonry_use' );
        $dynamicClass .= " dm-masonry";
        $animationData .= "data-column-width='$column_width";
    }

    ob_start();

    //echo "<div class='dm-container row $dynamicClass ' $animationData id=\"$dynamicId\">";
    echo "<div class='dm-container row $dynamicClass' $animationData id='$dynamicId'>";
    foreach($arrShortcodeList as $shortcode)
    {
        echo "<div $gridItemClass>";
        echo do_shortcode($shortcode);
        echo "</div>";
    }

    echo "</div> ";

    return ob_get_clean();


}