<?php
function dm_team_member($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title'                       => 'THE TITLE',
        'subtitle'                    => '',
        'image_id'                    => '',

        'color_title'                => 'Theme color 1',
        'color_subtitle'             => 'Theme color 1',

        'font_title'                 => 'Theme font 1',
        'font_subtitle'              => 'Theme font 1',

    ), $atts));

    $color_index_title = dm_get_color_index($color_title);
    $font_index_title = dm_get_font_index($font_title);
    $color_index_subtitle = dm_get_color_index($color_subtitle);
    $font_index_subtitle = dm_get_font_index($font_subtitle);


    $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];

    $return_string = "
        <section class='dm-team-member $dynamicClass' $animationData id='$dynamicId'>
            <img src='$imagePath' class='dm-image' alt='featured image'>
            <h2 class='dm-title font-color$color_index_title font$font_index_title'> $title</h2>
            <h3 class='dm-subtitle font-color$color_index_subtitle font$font_index_subtitle'> $subtitle </h3>
        </section>
    ";

    return $return_string;
}