<?php
function dm_testimonial($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'image_id' => '',
        'title' => 'Your title here',
        'body' => 'Your description here',
        'color_image_stroke' => '1',
        'color_title' => '1',
        'color_body' => '1',
        'color_quote_marks' => '1',
        'font_title' => '1',
        'font_body' => '1',
    ), $atts));

    $imagePath = '';
    if(strlen($image_id) != 0)
    {
        $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];
    }


    $color_index_image_stroke = dm_get_theme_color(dm_get_color_index($color_image_stroke));
    $color_index_title = dm_get_color_index($color_title);
    $font_index_title = dm_get_font_index($font_title);
    $color_index_body = dm_get_color_index($color_body);
    $font_index_body= dm_get_font_index($font_body);
    $color_index_quote_marks = dm_get_color_index($color_quote_marks);

    $return_string = '';
    $return_string .=

        "
        <div class='dm-testimonial-1 $dynamicClass' $animationData id='$dynamicId'>
            <img class='dm-image img-responsive' style='border: 3px solid $color_index_image_stroke' src='$imagePath'>
            <div class='dm-outer-container'>
                <i class='dm-icon-quote-left fa fa-quote-left font-color$color_index_quote_marks'></i>
                <div class='dm-inner-container'>
                    <h4 class='dm-title font-color$color_index_title'> $title </h4>
                    <p class='dm-body font-color$color_index_body'> $body </p>
                </div>
                <i class='dm-icon-quote-right fa fa-quote-right font-color$color_index_quote_marks'></i>
            </div>
        </div>
        ";

    return $return_string;
}