<?php
function dm_title($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'text' => 'Choose title',
        'color' => 'Theme color 1',
        'font' => 'Theme font 1',
    ), $atts));

    $color_index = dm_get_color_index($color);
    $font_index = dm_get_font_index($font);

    $return_string = "
    <div class='section-title-1 $dynamicClass' $animationData id='$dynamicId'>
        <h1 class='title font-color$color_index font$font_index'> $text </h1>
        <img class='separator' src=".dm_get_svg_url($color_index, 'separator_1_horizontal').">
    </div>
    ";

    return $return_string;
}