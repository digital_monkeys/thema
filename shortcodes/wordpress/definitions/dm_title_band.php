<?php

function dm_title_band($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';


    extract(shortcode_atts(array(
        'icon' => 'fa fa-star',
        'title' => 'The Title',
        'subtitle' => '',
        'font_title' => '1',
        'font_subtitle' => '1',
        'background_color' => '1',
    ), $atts));

    $color_index_background = dm_get_color_index($background_color);
    $font_index_title = dm_get_font_index($font_title);
    $font_index_subtitle = dm_get_font_index($font_subtitle);

    $return_string =
        "
        <div class='row dm-title-band-1 bg-color$color_index_background $dynamicClass' $animationData  id='$dynamicId'>
            <div class='boxed-layout'>
                <h1 class='dm-icon'>
                    <span class='$icon font-color-white'></span>
                </h1>
                <div class='dm-separator'> </div>
                <div class='dm-content'>
                    <h2 class='dm-title font$font_index_title'> $title </h2>
                    <p class='dm-subtitle font$font_index_subtitle'> $subtitle </p>
                </div>
            </div>
        </div>";

    return $return_string;
}