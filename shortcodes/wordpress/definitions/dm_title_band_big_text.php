<?php
function dm_title_band_big_text($atts, $content, $tag){
    require get_template_directory().'/shortcodes/includes/wp_shortcode_attributes_part.php';

    extract(shortcode_atts(array(
        'title' => 'THE TITLE',
        'subtitle' => '',
        'font_title' => '1',
        'font_subtitle' => '1',
        'background_color' => '1',
        'title_color' => '1',
        'subtitle_color' => '1',
        'image_id' => '',

    ), $atts));

    $color_index_background = dm_get_color_index($background_color);
    $color_index_title = dm_get_color_index($title_color);
    $font_index_title = dm_get_font_index($font_title);
    $color_index_subtitle = dm_get_color_index($subtitle_color);
    $font_index_subtitle = dm_get_font_index($font_subtitle);

    $imagePath = '';
    if(strlen($image_id) != 0)
    {
        $imagePath =  wp_get_attachment_image_src( $image_id, 'full' )[0];
    }

    $return_string =
        "
        <section class='row dm-title-band-big-text-1 bg-color$color_index_background center-align $dynamicClass' $animationData id='$dynamicId'>
            <div class='boxed-layout'>
        ";
    if(isset($imagePath) && strlen($imagePath) > 0)
    {
        $return_string .=   "<img src='$imagePath' class='dm-image' alt='featured image'>";
    }


    $return_string .=       "<h1 class='dm-title font-color$color_index_title font$font_index_title'> $title </h1>";

    if(isset($subtitle) && strlen($subtitle) > 0)
    {
        $return_string .=   "<h3 class='dm-subtitle font-color$color_index_subtitle font$font_index_subtitle'> $subtitle </h3>";
    }
    $return_string .= "
            </div>
        </section>
        ";
    return $return_string;
}