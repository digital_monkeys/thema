<?php
// include all the files in the definitions folder
$dm_wp_shortcode_directory = scandir(get_template_directory().'/shortcodes/wordpress/definitions');
foreach ($dm_wp_shortcode_directory as $filename)
{
    if(strlen($filename) > 2) {
        include_once 'definitions/'.$filename;
    }
}

add_action( 'init', 'register_shortcodes');
function register_shortcodes() {
    // Based on the definitions of the shortcodes, dynamically add the shortcodes

    global $dm_wp_shortcode_directory;
    foreach ($dm_wp_shortcode_directory as $filename)
    {
        $arrFileName = explode('.', $filename);

        $functionName = $arrFileName[0];
        $shortcodeName = $arrFileName[0];
        if(strlen($filename) > 2) {
            add_shortcode($shortcodeName, $functionName);
        }
    }
}




