<?php get_header(); ?>

<div class="boxed-layout">
    <?php
    if(have_posts())
    {
        while(have_posts())
        {
            the_post();
            get_template_part('partials/post', 'skin1');
        }
    }
    else
    {
        ?>
        <h3> Sorry, no posts to display. </h3>
        <?php
    }
    ?>
</div>


<?php get_footer(); ?>