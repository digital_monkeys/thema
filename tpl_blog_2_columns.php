<?php
/*
  * Template Name: Blog Template 2 Columns
  */
?>
<?php get_header(); ?>

<?php echo do_shortcode('
	[vc_row]
		[vc_column]
			[dm_title_band
				font_title="Theme font 2"
				transition_type="fadeInUp"
				transition_duration="0.75"
				transition_easing="easeIn"
				title="Blog"
				subtitle="What we\'ve been up to. News, articles and tips. Enjoy!"
				icon="fa fa-pencil"
			]
		[/vc_column]
	[/vc_row]
	');
?>



	<div class="boxed-layout">
		<div class="row">
			<?php
			$args = array(
				'post_type' => 'post'
			);

			$query = new WP_Query($args);

			if($query->have_posts())
			{
				while($query->have_posts())
				{
					$query->the_post();
					echo "<div class='col-sm-6'>";
					get_template_part('partials/post', 'skin1');
					echo "</div>";
				}
			}
			else
			{
				?>
				<h3> Sorry, no posts to display. </h3>
				<?php
			}
			?>
		</div>
	</div>


<?php get_footer(); ?>