<?php
/*
  * Template Name: Blog Template Sidebar Left and Right
  */
?>
<?php get_header(); ?>

<?php echo do_shortcode('
	[vc_row]
		[vc_column]
			[dm_title_band
				font_title="Theme font 2"
				transition_type="fadeInUp"
				transition_duration="0.75"
				transition_easing="easeIn"
				title="Blog"
				subtitle="What we\'ve been up to. News, articles and tips. Enjoy!"
				icon="fa fa-pencil"
			]
		[/vc_column]
	[/vc_row]
	');
?>



	<div class="boxed-layout">
		<div class="row">
			<div class="col-lg-2 col-sm-3 sidebar">
				<?php
				if(is_active_sidebar('blog-left-sidebar')){
					dynamic_sidebar('blog-left-sidebar');
				}

				?>
			</div>
			<div class="col-lg-8 col-sm-6">
			<?php
			$args = array(
				'post_type' => 'post'
			);

			$query = new WP_Query($args);

			if($query->have_posts())
			{
				while($query->have_posts())
				{
					$query->the_post();

					get_template_part('partials/post', 'skin1');

				}
			}
			else
			{
				?>
				<h3> Sorry, no posts to display. </h3>
				<?php
			}
			?>
			</div>
			<div class="col-lg-2 col-sm-3 sidebar">
				<?php
				if(is_active_sidebar('blog-right-sidebar')){
					dynamic_sidebar('blog-right-sidebar');
				}

				?>
			</div>
		</div>
	</div>


<?php get_footer(); ?>