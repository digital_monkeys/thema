<?php
/*
  * Template Name: Portfolio Template 1
  */
?>
<?php get_header(); ?>

<?php echo do_shortcode('
	[vc_row]
		[vc_column]
			[dm_title_band
				font_title="Theme font 2"
				transition_type="fadeInUp"
				transition_duration="0.75"
				transition_easing="easeIn"
				title="Portfolio"
				subtitle="What we\'ve done so far. And we\'re gonna do it again!"
				icon="fa fa-folder"
			]
		[/vc_column]
	[/vc_row]
	');
?>



	<div class="boxed-layout margin-top margin-bottom">
		<?php
		$args = array(
			'post_type' => 'dm_portfolio'
		);

		$query = new WP_Query($args);

		if($query->have_posts())
		{
			while($query->have_posts())
			{
				$query->the_post();
				get_template_part('partials/portfolio', 'skin1');

			}
		}
		else
		{
			?>
			<h3> Sorry, no posts to display. </h3>
			<?php
		}
		?>
	</div>


<?php get_footer(); ?>