<?php
class My_Widget extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_name = 'my_widget';
        $widget_ops = array(
            'classname' => $widget_name,
            'description' => 'My Widget is awesome',
        );
        parent::__construct( $widget_name, 'My Widget', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        echo $args['before_widget'];
            $strReturn ="
                <div class='footer-section-contact-us font-color-white'>
                    <h2 class='dm-title font-1 font-color-white'> CONTACT  US </h2>
                    <p class='dm-content'>
                         <span class='contact-highlight font-color-1'>

                         Call:
                         </span>
                         ".dm_get_option('contact-phone-number')."
                    </p>
                    <p class='dm-content'>
                         <span class='contact-highlight font-color-1'>

                         Write:
                         </span>
                         ".dm_get_option('contact-email')."
                    </p>
                    <p class='dm-content'>
                         <span class='contact-highlight font-color-1'>

                         Skype:
                         </span>
                         john.doe
                    </p>
                 </div>






";
        echo $strReturn;
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
    }
}