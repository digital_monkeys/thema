<?php
class Follow extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_name = 'follow';
        $widget_ops = array(
            'classname' => $widget_name,
            'description' => 'My Widget is awesome',
        );
        parent::__construct( $widget_name, 'Follow Us', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        echo $args['before_widget'];
        $strReturn ="
            <div class='footer-section-follow-us'>
                 <h2 class='dm-title font-1 font-color-white'> FOLLOW  US </h2>
                 <div class='row'>
                    <div class='col-xs-4 dm-social-icon-container'>
                        <a href='".dm_get_option('facebook_url')."' class='font-color-1'>
                            <i class='fa fa-facebook social-icon'></i>
                        </a>
                    </div>
                    <div class='col-xs-4 dm-social-icon-container'>

                            <a href='".dm_get_option('twitter_url')."' class='font-color-1'>
                                <i class='fa fa-twitter social-icon'></i>
                            </a>

                    </div>
                    <div class='col-xs-4 dm-social-icon-container'>

                            <a href='".dm_get_option('google_plus_url')."'class='font-color-1'>
                                <i class='fa fa-google-plus social-icon'></i>
                            </a>

                    </div>
                </div>
                <div class='row'>
                    <div class='col-xs-4 dm-social-icon-container'>

                            <a href='".dm_get_option('instagram_url')."' class='font-color-1'>
                                <i class='fa fa-instagram social-icon'></i>
                            </a>

                    </div>
                    <div class='col-xs-4 dm-social-icon-container'>

                            <a href='".dm_get_option('linkedin_url')."' class='font-color-1'>
                                <i class='fa fa-linkedin social-icon'></i>
                            </a>

                    </div>
                    <div class='col-xs-4 dm-social-icon-container'>

                            <a href='".dm_get_option('stumbleupon_url')."' class='font-color-1'>
                                <i class='fa fa-stumbleupon social-icon'></i>
                            </a>

                    </div>
                </div>
            </div>


";
        echo $strReturn;
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
    }
}