<?php
class Other_info extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_name = 'other_info';
        $widget_ops = array(
            'classname' => $widget_name,
            'description' => 'My Widget is awesome',
        );
        parent::__construct( $widget_name, 'Other Info', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        // outputs the content of the widget
        echo $args['before_widget'];
        $strReturn ="
            <div class='footer-section-info'>
                <h2 class='dm-title font-1 font-color-white'> OTHER  INFO </h2>
                <p class='dm-content font-color-white'>
                    Copyrights 2016 Digital Monkeys
                    <br>
                    All rights reserved.
                </p>
                <p class='dm-content font-color-white'>
                    Terms & conditions
                </p>
                <p class='dm-content font-color-white'>
                    Privacy Policy
                </p>
            </div>

";
        echo $strReturn;
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
    }
}