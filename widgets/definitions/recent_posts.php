<?php
class Recent_posts extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_name = 'recent_posts';
        $widget_ops = array(
            'classname' => $widget_name,
            'description' => 'My Widget is awesome',
        );
        parent::__construct( $widget_name, 'Recent Posts', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
       echo $args['before_widget'];

        $args = array(
           'post_type' => 'post',
           'post_per_page' => 5,
       );


        $query = new WP_Query($args);
        if($query->have_posts())
        {
            while($query->have_posts())
            {
                $query->the_post();
                echo '<h2>'.get_the_title().'</h2>';
                echo '<p>'.get_the_content().'</p>';
            }
        }
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        // outputs the options form on admin
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
    }
}