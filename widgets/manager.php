<?php
// include all the files in the definitions folder
$dm_wp_widget_directory = scandir(get_template_directory().'/widgets/definitions');
foreach ($dm_wp_widget_directory as $filename)
{
    if(strlen($filename) > 2) {
        include_once 'definitions/'.$filename;
    }
}

// Based on the files containing the definitions of the widgets, dynamically register all the widgets
add_action( 'widgets_init', function(){
    $directory = scandir(get_template_directory().'/widgets/definitions');
    foreach ($directory as $filename)
    {
        if(strlen($filename) > 2) {
            $arrFileName = explode('.', $filename);
            $widgetName = $arrFileName[0];
            register_widget($widgetName);
        }
    }


});